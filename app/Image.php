<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    protected $fillable = ['id', 'application_id', 'src'];

    public function application(){
    	return $this->belongsTo('App\Application');
    }
}
