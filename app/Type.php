<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'types';
    protected $fillable = ['title_geo', 'title_eng', 'title_rus'];

    public function applications(){
    	return $this->hasMany('App\Application');
    }
}
