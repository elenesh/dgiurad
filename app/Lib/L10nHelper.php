<?php

class L10nHelper {

    protected static $lang = null;

    public static final function get($obj, $key = 'title') {


        if (self::$lang === null) {
            throw new Exception("Language not set in L10nHelper pacman emoticon");
        }

        if (isset($obj[$key . '_' . self::$lang]) && $obj[$key . '_' . self::$lang] != '')
            return $obj[$key . '_' . self::$lang];
        else
            return $obj[$key . '_geo'];
    }



    public static final function setLocale($l) {
        if($l == 'ka'){
            $loc = 'geo';
        }else if($l == 'en'){
            $loc = 'eng';
        }else{
            $loc = 'rus';
        }
        self::$lang = $loc;
        setcookie("lang", $loc, time()+ (10 * 365 * 24 * 60 * 60));
    }

    public static final function getLocale() {
        return self::$lang;
    }
}