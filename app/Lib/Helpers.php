<?php

class Helpers {

	public static function getPrice($app, $i = 0){
        if($app->currency == 'USD'){
            $curClass = 'usd';
        }else{
            $curClass = 'gel';
        }
        if($i == 0) {
            if ($app->app_type == 'hourly') {
                return (int)$app->hourly_price . '<i class="' . $curClass . '-sign" style="font-size:14px;"></i> / ' . trans('main.in-hour');
            } else if ($app->app_type == 'daily') {
                return (int)$app->daily_price . '<i class="' . $curClass . '-sign" style="font-size:14px;"></i> / ' . trans('main.in-day');
            } else if ($app->app_type == 'daily-hourly') {
                return (int)$app->hourly_price . '<i class="' . $curClass . '-sign" style="font-size:14px;"></i> / ' . trans('main.in-hour') . ' <br> ' . (int)$app->daily_price . '<i class="' . $curClass . '-sign" style="font-size:14px;"></i> / ' . trans('main.in-day');
            }
        }elseif($i == 1){
            return (int)$app->daily_price . ' <i class="' . $curClass . '-sign" style="font-size:14px;"></i> / ' . trans('main.in-day');
        }elseif($i == 2){
            return (int)$app->hourly_price . ' <i class="' . $curClass . '-sign" style="font-size:14px;"></i> / ' . trans('main.in-hour');
        }
	}

	public static function getCurrencyFromNBG(){
        $client = new SoapClient('http://nbg.gov.ge/currency.wsdl');
        return $client->GetCurrency('USD');
	}

    public static function getLocaleChangerURL($locale){
        $uri = $_SERVER['REQUEST_URI'];
        $uri = explode('/', $uri);
        unset($uri[0]);
        unset($uri[1]);
        $url = url($locale);
        foreach($uri as $u){
            $url .= '/'.$u;
        }
        return $url;
    }

    public static function getDateForView($date){
        $time = strtotime($date);
        return date('d-m-Y', $time);
    }

    public static function getDate($date){
        $time = strtotime($date);
        return date('Y-m-d', $time);
    }

}