<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attributes';
    protected $fillable = ['title_geo', 'title_eng', 'title_rus'];

    public function applications(){
    	return $this->belongsToMany('App\Application', 'application_attribute');
    }
}
