<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $table = 'conditions';
    protected $fillable = ['title_geo', 'title_eng', 'title_rus'];

    public function applications(){
    	return $this->hasMany('App\Application');
    }
}
