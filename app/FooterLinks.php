<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FooterLinks extends Model
{
    protected $table = 'footer_links';
    protected $fillable = ['title_geo', 'title_eng', 'title_rus', 'slug', 'city_id'];

    public $timestamps = false;

}