<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $fillable = ['title_geo', 'title_eng', 'title_rus', 'latitude', 'longitude'];

    public function districts(){
    	return $this->hasMany('App\District');
    }

    public function applications(){
    	return $this->hasMany('App\Application');
    }
}
