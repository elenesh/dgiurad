<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';
    protected $fillable = ['title_geo', 'title_eng', 'title_rus', 'city_id'];

    public function city(){
    	return $this->belongsTo('App\City');
    }

    public function applications(){
    	return $this->hasMany('App\Application');
    }
}
