<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeletedLinks extends Model
{
    protected $table = 'deleted_links';
    protected $fillable = ['unique_id', 'city_id'];
}
