<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reset extends Model
{
    protected $table = 'password_reset';
    protected $fillable = ['user_id', 'uri'];
    
}
