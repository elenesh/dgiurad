<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrePayment extends Model
{
    protected $table = 'pre_payments';
    protected $fillable = ['user_id', 'amount', 'trans_id'];
}
