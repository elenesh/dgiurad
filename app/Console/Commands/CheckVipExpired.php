<?php

namespace App\Console\Commands;

use App\Application;
use App\PrePayment;
use Illuminate\Console\Command;

class CheckVipExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:vip-expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Expired VIP Applications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = \Carbon\Carbon::now();
        $apps = Application::where('vip_end_date', '<', $today)
            ->update([
                'rank' => 0,
                'vip_start_date' => NULL,
                'vip_end_date' => NULL,
            ]);
        $count = count($apps);
        $this->info($count . ' entries has been UPDATED.');
    }
}
