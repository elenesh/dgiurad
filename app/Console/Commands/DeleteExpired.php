<?php

namespace App\Console\Commands;

use App\Application;
use App\PrePayment;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use File;

class DeleteExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Expired Applications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = \Carbon\Carbon::now();
        $afterOneMonth = \Carbon\Carbon::now()->subMonth();

        $oldApps = Application::where('end_time', '<', $today)->update(['active' => 0]);
        $oldInactiveApps = Application::where('end_time', '>', $afterOneMonth)->get();

        foreach($oldInactiveApps as $o){
            File::deleteDirectory(public_path() . "/uploads/applications/".$o->id);
        }
        Application::where('end_time', '<', $afterOneMonth)->delete();

        $this->info(count($oldApps) . ' entries has been made as INACTIVE.');
        $this->info(count($oldInactiveApps) . ' entries has been DELETED.');
    }
}
