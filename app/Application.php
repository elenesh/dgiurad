<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table = 'applications';
    protected $fillable = [
        'unique_id',
        'end_time',
        'type_id',
        'title_geo',
        'title_eng',
        'title_rus',
        'city_id',
        'district_id',
        'street_addr_geo',
        'street_addr_eng',
        'street_addr_rus',
        'm2',
        'room_quantity',
        'bedroom_quantity',
        'bathroom_quantity',
        'floor',
        'description_geo',
        'description_eng',
        'description_rus',
        'daily_price',
        'hourly_price',
        'condition_id',
        'latitude',
        'longitude',
        'rank',
        'vip_start_date',
        'vip_end_date',
        'user_id',
        'phone',
        'slug',
        'comment',
        'default_image_id'
    ];

    public function type(){
    	return $this->belongsTo('App\Type');
    }

    public function city(){
    	return $this->belongsTo('App\City');
    }

    public function district(){
    	return $this->belongsTo('App\District');
    }	

    public function user(){
    	return $this->belongsTo('App\SiteUsers');
    }

    public function favourites(){
    	return $this->belongsToMany('App\SiteUsers', 'favourites');
    }

    public function attributes(){
    	return $this->belongsToMany('App\Attribute', 'application_attribute');
    }

    public function images(){
        return $this->hasMany('App\Image');
    }

    public function defaultImage(){
        return $this->hasOne('App\Image', 'id', 'default_image_id');
    }

    public function condition(){
        return $this->hasOne('App\Condition', 'id', 'condition_id');
    }	
}
