<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class SiteUsers extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'users';
    protected $fillable = ['unique_id', 'email', 'password', 'phone', 'first_name', 'last_name', 'birth_date', 'type', 'incorporated_id', 'incorporated_name', 'balance'];
    protected $hidden = ['password', 'remember_token'];
    
    public function payments(){
    	return $this->hasMany('App\Payment', 'user_id');
    }

    public function applications(){
    	return $this->hasMany('App\Application', 'user_id');
    }

    public function favourites(){
    	return $this->belongsToMany('App\Application', 'favourites', 'user_id');
    }
}
