<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SiteUsers;
use Redirect;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = SiteUsers::where('type', 0)->get();
        return view('admin.users', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new SiteUsers();

        $user->unique_id = $this->getUniqueID();
        $email = $request->input('email');
        $phone = $request->input('phone');
        if($this->checkEmail($email) == 1){
            return Redirect::route('admin.users.add', ['status' => 1]);
        }else if($this->checkPhone($phone) == 1){
            return Redirect::route('admin.users.add', ['status' => 2]);
        }

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->password = Hash::make($request->input('password'));
        $user->phone = $phone;
        $user->email = $email;
        $user->birth_date = $request->input('birth_date');

        $user->save();
        return Redirect::route('admin.users.show', ['status' => 4, 'name' => $user->email]);
    }

    public function checkPhone($phone, $id = 0){
        if($id == 0){
            $user = SiteUsers::where('phone', $phone)->get();
        }else{
            $user = SiteUsers::where('phone', $phone)->where('id', '!=', $id)->get();
        }
        if(isset($user[0])){
            return 1;
        }else{
            return 0;
        }
    }

    public function checkEmail($email, $id = 0){
        if($id == 0){
            $user = SiteUsers::where('email', $email)->get();
        }else{
            $user = SiteUsers::where('email', $email)->where('id', '!=', $id)->get();
        }
        if(isset($user[0])){
            return 1;
        }else{
            return 0;
        }
    }

    public function getUniqueID(){
        $uniqueID = rand(1111111111,9999999999);
        while($this->checkUniqueID($uniqueID) == 1){
            $uniqueID = rand(1111111111,9999999999);
        }
        return $uniqueID;
    }

    public function checkUniqueID($uniqueID){
        $user = SiteUsers::where('unique_id', $uniqueID)->get();
        if(isset($user[0])){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showDetailed($id)
    {
        $data['user'] = SiteUsers::where('unique_id', $id)->with('payments')->with('applications')->firstOrFail();
        return view('admin.user-detailed', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = SiteUsers::findOrFail($id);
        return view('admin.users-add')->with('obj', $obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = SiteUsers::findOrFail($id);

        $email = $request->input('email');
        $phone = $request->input('phone');
        if($this->checkEmail($email, $id) == 1){
            return Redirect::route('admin.users.edit', [$id,'status' => 1]);
        }else if($this->checkPhone($phone, $id) == 1){
            return Redirect::route('admin.users.edit', [$id,'status' => 2]);
        }

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->phone = $phone;
        $user->email = $email;
        $user->birth_date = $request->input('birth_date');
        

        $user->save();
        return Redirect::route('admin.users.edit',[$id,'status' => 6]);
    }

    public function block($id){
        $user = SiteUsers::findOrFail($id);

        $user->blocked = 1;
        $user->save();

        return Redirect::route('admin.users.show', ['status' => 7, 'name' => $user->email]);
    }

    public function unblock($id){
        $user = SiteUsers::findOrFail($id);

        $user->blocked = 0;
        $user->save();

        return Redirect::route('admin.users.show', ['status' => 8, 'name' => $user->email]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = SiteUsers::findOrFail($id);
        try{
            $user->destroy($id);
        } catch(Exception $e){
            abort(404, "Can't Delete");
        }
        return Redirect::route('admin.users.show', ['status' => 5, 'name' => $user->email]);
    }
}
