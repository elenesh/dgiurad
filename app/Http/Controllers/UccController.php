<?php
namespace App\Http\Controllers;

use App\SiteUsers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Hash;
use App\Payment;

ob_start();

class UccController extends Controller
{
    private $secret_key = "unaqtHL7OE0ooIqIfjp4";

    public function debt(Request $request)
    {
        $unique_id = $request->input('abonentCode');
        $UCChash = $request->input('hash');
        $action = $request->input('action');
        $user = $request->input('user');
        $myUser = null;
        $hash = md5($action . $unique_id . $user . $this->secret_key);
        if ($UCChash == $hash) {
            try {
                $myUser = SiteUsers::where('unique_id', $unique_id)->first();
                if (isset($myUser['attributes'])) {
                    $myUser = $myUser['attributes'];
                    $status = 0;
                } else {
                    $status = 1;
                }
            } catch (Exception $e) {
                $status = 2;
            }
        } else {
            $status = 5;
        }

        return $this->makeXML($action, $status, $myUser);
    }

    public function pay(Request $request)
    {
        $action = $request->input('action');
        $unique_id = $request->input('abonentCode');
        $paymentId = $request->input('paymentId');
        $amount = $request->input('amount');
        $user = $request->input('user');
        $UCChash = $request->input('hash');

        $hash = md5($action . $unique_id . $paymentId . $user . $this->secret_key);

        if ($UCChash == $hash) {
            try {
                $user = SiteUsers::where('unique_id', $unique_id)->first();
                if (isset($user['attributes'])) {
                    $userPayment = Payment::where('payment_id', $paymentId)->get();
                    if (!isset($userPayment[0])) {
                        $status = $this->savePayment($user, $amount, $paymentId);
                    } else {
                        $status = 4;
                    }
                } else {
                    $status = 1;
                }
            } catch (Exception $e) {
                $status = 2;
            }
        } else {
            $status = 5;
        }

        return $this->makeXML($action, $status);
    }

    public function savePayment($user, $amount, $paymentId)
    {
        $balance = $user->balance;
        $user->balance = ($balance + $amount);

        $payment = new Payment();

        $payment->user_id = $user->id;
        $payment->amount = $amount;
        $payment->payment_id = $paymentId;
        $payment->type = 'ბალანსის შევსება UCC';

        try {
            $payment->save();
            $user->save();
            return 0;
        } catch (Exception $e) {
            return 2;
        }
    }

    public function makeXML($action, $status, $user = null)
    {
        if ($action == 'debt') {

            if ($status == 0) {
                header('Content-type: text/xml');
                $data = '<?xml version="1.0" encoding="UTF-8"?>
                        <debt-response>
                            <status>' . $status . '</status>
                            <debt>-' . $user['balance'] . '</debt>
                            <name>' . $user['first_name'] . ' ' . $user['last_name'] . '</name>
                        </debt-response>';
                return $data;
            } else {
                header('Content-type: text/xml');
                $data = '<?xml version="1.0" encoding="UTF-8"?>
                        <debt-response>
                            <status>' . $status . '</status>
                        </debt-response>';
                return $data;
            }
        } else if ($action == 'pay') {
            header('Content-type: text/xml');
            $data = '<?xml version="1.0" encoding="UTF-8"?>
                    <pay-response>
                        <status>' . $status . '</status>
                    </pay-response>';
            return $data;
        }
    }
}