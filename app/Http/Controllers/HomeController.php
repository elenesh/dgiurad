<?php
namespace App\Http\Controllers;

use App\Application;
use App\FooterLinks;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Hash;
use Auth;
use Redirect;
use App\TransportRoutes;
use App\City;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $applications = City::all();
        return view('admin.index')->with('applications', $applications);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function showLogin()
    {
        if (Auth::check()){
            return Redirect::route('admin.index');
        }else{
            return view('admin.login');
        }
    }

    public function checkLogin(Request $request)
    {
        if (Auth::check()){
            return Redirect::route('admin.index');  
        }

        $userData = array(
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            );

        if(Auth::attempt($userData, true)){
            return Redirect::route('admin.index');
        } else {
            return Redirect::route('admin.login');
        }
    }


    public function adminLogout(Request $request)
    {
        Auth::logout();
        return Redirect::route('admin.login');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sitemap()
    {
        $apps = Application::get(['slug','unique_id']);
        $footerLinks = FooterLinks::all();
        return \Response::view('site.sitemap', compact('apps', 'footerLinks'))
            ->header('Content-Type', 'application/xml');
    }

}
