<?php

namespace App\Http\Controllers;

use App\City;
use App\FooterLinks;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class FooterLinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['footerLinks'] = FooterLinks::all();
        return view('admin.footer-links', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['cities'] = City::lists('title_geo', 'id');
        return view('admin.footer-links-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $footerLink = new FooterLinks();

        $footerLink->title_geo = $request->input('title_geo');
        $footerLink->title_eng = $request->input('title_eng');
        $footerLink->title_rus = $request->input('title_rus');
        $footerLink->description_geo = $request->input('description_geo');
        $footerLink->description_eng = $request->input('description_eng');
        $footerLink->description_rus = $request->input('description_rus');
        $footerLink->slug = $request->input('slug');
        $footerLink->city_id = $request->input('city_id');

        $footerLink->save();

        return Redirect::route('admin.footer-links.show', ['status' => 4, 'name' => $footerLink->title_geo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = FooterLinks::findOrFail($id);
        $data['cities'] = City::lists('title_geo', 'id');
        return view('admin.footer-links-add', $data)->with('obj', $obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $footerLink = FooterLinks::findOrFail($id);

        $footerLink->title_geo = $request->input('title_geo');
        $footerLink->title_eng = $request->input('title_eng');
        $footerLink->title_rus = $request->input('title_rus');
        $footerLink->description_geo = $request->input('description_geo');
        $footerLink->description_eng = $request->input('description_eng');
        $footerLink->description_rus = $request->input('description_rus');
        $footerLink->slug = $request->input('slug');
        $footerLink->city_id = $request->input('city_id');

        $footerLink->save();
        return Redirect::route('admin.footer-links.edit',[$id, 'status' => 6]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $footerLink = FooterLinks::findOrFail($id);
        try{
            $footerLink->destroy($id);
        } catch(Exception $e){
            abort(404, "Can't Delete");
        }
        return Redirect::route('admin.footer-links.show', ['status' => 5, 'name' => $footerLink->title_geo]);
    }
}
