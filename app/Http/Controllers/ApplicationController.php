<?php

namespace App\Http\Controllers;

use App\ApplicationAttributes;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Application;
use App\SiteUsers;
use App\City;
use App\District;
use App\Type;
use App\Condition;
use App\Image;
use File;
use Intervention\Image\ImageManagerStatic;
use Validator;
use App\Attribute;
use Redirect;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['applications'] = Application::with('defaultImage')->with('user')->get();
        return view('admin.applications', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users'] = SiteUsers::lists('email', 'id');
        $data['cities'] = City::lists('title_geo', 'id');
        $data['districts'] = District::lists('title_geo', 'id');
        $data['types'] = Type::lists('title_geo', 'id');
        $data['conditions'] = Condition::lists('title_geo', 'id');
        $data['attributes'] = Attribute::all();
        return view('admin.applications-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'description' => 'required',
            'street_addr' => 'required',
        ]);

        if ($validator->fails()) {
            return ['success' => false];
        }

        $application = new Application();
        $application->unique_id = $this->getUniqueID();
        $application->end_time = $this->getEndTime($request->input('num_of_days'));
        $application->type_id = $request->input('type_id');
        $application->title = $request->input('title');
        $application->city_id = $request->input('city_id');
        $application->district_id = $request->input('district_id');
        $application->street_addr = $request->input('street_addr');
        $application->m2 = $request->input('m2');
        $application->room_quantity = $request->input('room_quantity');
        $application->floor = $request->input('floor');
        $application->description = $request->input('description');
        $application->daily_price = $request->input('daily_price');
        $application->hourly_price = $request->input('hourly_price');
        $application->currency = $request->input('currency');
        $application->condition_id = $request->input('condition_id');
        $application->latitude = $request->input('latitude');
        $application->longitude = $request->input('longitude');
        $application->user_id = $request->input('user_id');
        $application->phone = $request->input('phone');
        $application->comment = $request->input('comment');
        $application->slug = $this->makeSlug($request->input('title'));


        try{
            $application->save();
        }catch(Exceprion $e){
            return ['success' => false];
        }
        $appl = Application::where('unique_id', $application->unique_id)->firstOrFail();
        $appl->attributes()->attach($request->input('attributes'));
        return ['success' => true, 'applicationId' => $appl->id];
    }

    public function getEndTime($days){
        date_default_timezone_set('Asia/Tbilisi');
        return date('Y-m-d H:i:s', strtotime('+ '.$days.' days'));
    }

    public function getUniqueID(){
        $uniqueID = rand(100000000000,999999999999);
        while($this->checkUniqueID($uniqueID) == 1){
            $uniqueID = rand(100000000000,999999999999);
        }
        return $uniqueID;
    }

    public function checkUniqueID($uniqueID){
        $application = Application::where('unique_id', $uniqueID)->get();
        if(isset($application[0])){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = Application::with('images')->with('attributes')->findOrFail($id);
        $data['users'] = SiteUsers::lists('email', 'id');
        $data['cities'] = City::lists('title_geo', 'id');
        $data['districts'] = District::lists('title_geo', 'id');
        $data['types'] = Type::lists('title_geo', 'id');
        $data['conditions'] = Condition::lists('title_geo', 'id');
        $data['attributes'] = Attribute::all();
        return view('admin.applications-add', $data)->with('obj', $obj);
    }

    private function makeSlug($title){
        $geo = [' ', 'ა', 'ბ', 'გ', 'დ', 'ე', 'ვ', 'ზ', 'თ', 'ი', 'კ', 'ლ', 'მ', 'ნ', 'ო', 'პ', 'ჟ', 'რ', 'ს', 'ტ', 'უ', 'ფ', 'ქ', 'ღ', 'ყ', 'შ', 'ჩ', 'ც', 'ძ', 'წ', 'ჭ', 'ხ', 'ჯ', 'ჰ'];
        $geoEng = ['-', 'a', 'b', 'g', 'd', 'e', 'v', 'z', 't', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'zh', 'r', 's', 't', 'u', 'f', 'q', 'gh', 'y', 'sh', 'ch', 'c', 'dz', 'w', 'tch', 'x', 'j', 'h'];

        return str_slug(str_replace($geo, $geoEng, $title), '-');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $application = Application::with('attributes')->findOrFail($id);
        $application->type_id = $request->input('type_id');
        $application->title = $request->input('title');
        $application->city_id = $request->input('city_id');
        $application->district_id = $request->input('district_id');
        $application->street_addr = $request->input('street_addr');
        $application->m2 = $request->input('m2');
        $application->room_quantity = $request->input('room_quantity');
        $application->floor = $request->input('floor');
        $application->description = $request->input('description');
        $application->daily_price = $request->input('daily_price');
        $application->hourly_price = $request->input('hourly_price');
        $application->currency = $request->input('currency');
        $application->condition_id = $request->input('condition_id');
        $application->latitude = $request->input('latitude');
        $application->longitude = $request->input('longitude');
        $application->user_id = $request->input('user_id');
        $application->phone = $request->input('phone');
        $application->comment = $request->input('comment');
        $application->slug = $this->makeSlug($request->input('title'));
        $attributes = [];
        foreach($application->attributes as $a){
            $attributes[] = $a->id;
        }
        $application->attributes()->detach($attributes);
        $application->attributes()->attach($request->input('attributes'));
        try{
            $application->save();
        }catch(Exceprion $e){
            return ['success' => false];
        }


        return ['success' => true, 'applicationId' => $id];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $application = Application::with('images')->with('attributes')->findOrFail($id);
        try{
            foreach($application->images as $i){
                Image::destroy($i->id);
            }
            File::deleteDirectory(public_path() . "/uploads/applications/".$i->application_id);
            $application->destroy($id);
        }catch (Exceprion $e){
            App::abort(404);
        }
        return Redirect::route('admin.applications.show');

    }

    public function uploadImage(Request $request){
        $img = new Image();
        $img->application_id = $request->input('id');
        $image = $request->file('image');
        if ($image->isValid()){
            $path = public_path().'/uploads/applications/'.$request->input('id')."/";
            $fileName = str_random(32).'.'.$image->getClientOriginalExtension();
            $image->move($path, $fileName);
            $this->resizeImage($path, $fileName);
        }
        $key = $fileName;
        $deleteUrl = route('admin.applications.image.delete');
        $img->src = $fileName;
        $img->save();

        $imageId = Image::where('src', $fileName)->firstOrFail();
        $imageId = $imageId->id;
        $data = json_encode([
            'initialPreview' => [
                "<img  style='height:160px' src='/uploads/applications/".$request->input('id')."/2_".$fileName."' class='file-preview-image'>
                <a data-id=".$imageId." class='btn btn-primary btn-sm set-profile'>".trans('main.main-image-add')."</a>
                <input type='hidden' name='images[]' value='".$fileName."'>",
            ],
            'initialPreviewConfig' => [
                ['caption' => "...", 'width' => '120px', 'url' => $deleteUrl, 'key' => $key],
            ],
            'append' => true
        ]);

        return $data;
    }

    private function resizeImage($path, $imgName){
        $sizes = getimagesize($path.$imgName);
        if($sizes[0] > $sizes[1]){
            ImageManagerStatic::make($path.$imgName)->fit(920,474)->insert(public_path() . "/uploads/applications/watermark.png",'bottom-right', 30, 30)->save($path."1_".$imgName);
        }else{
            ImageManagerStatic::make($path.$imgName)->heighten(474)->insert(public_path() . "/uploads/applications/watermark.png",'bottom-right', 30, 30)->save($path."1_".$imgName);
        }
        ImageManagerStatic::make($path.$imgName)->fit(440,226)->save($path."2_".$imgName);
        File::delete($path.$imgName);
    }

    public function deleteImage(Request $request){
        $imageName = $request->input('key');
        $image = Image::where('src', $imageName)->firstOrFail();
        try{
            Image::destroy($image->id);
            File::delete(public_path() . "/uploads/applications/".$image->application_id."/1_" . $imageName);
            File::delete(public_path() . "/uploads/applications/".$image->application_id."/2_" . $imageName);
        }catch (Exceprion $e){
            App::abort(404);
        }
        return 0;
    }

    public function addDefaultImage(Request $request){
        $id = $request->input('id');
        $application = Application::findOrFail($request->input('appl_id'));
        $application->default_image_id = $id;
        $application->save();
        return ['success' => true];
    }

    public function deleteImageFromEdit($id){
        die();
        $image = Image::findOrFail($id);
        try{
            Image::destroy($id);
            File::delete(public_path() . "/uploads/applications/".$image->application_id."/1_" . $image->src);
            File::delete(public_path() . "/uploads/applications/".$image->application_id."/2_" . $image->src);
        }catch (Exceprion $e){
            App::abort(404);
        }
        return Redirect::route('admin.applications.edit', $id);
    }
}
