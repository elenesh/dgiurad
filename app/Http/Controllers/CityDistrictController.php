<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\City;
use App\District;
use Redirect;

class CityDistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['cityDistrict'] = City::with('districts')->get();
        return view('admin.city-district', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $data['cities'] = City::lists('title_geo', 'id');
        return view('admin.city-district-add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = new City();

        $city->title_geo = $request->input('title_geo');
        $city->title_eng = $request->input('title_eng');
        $city->title_rus = $request->input('title_rus');
        $city->latitude = $request->input('latitude');
        $city->longitude = $request->input('longitude');

        $city->save();

        return Redirect::route('admin.city-district.show', ['status' => 4, 'name' => $city->title_geo]);
    }

    public function storeDistrict(Request $request)
    {
        $district = new District();

        $district->title_geo = $request->input('title_geo');
        $district->title_eng = $request->input('title_eng');
        $district->title_rus = $request->input('title_rus');
        $district->city_id = $request->input('city_id');

        $district->save();

        return Redirect::route('admin.city-district.show', ['status' => 4, 'name' => $district->title_geo]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $obj = City::findOrFail($id);
        $data['cities'] = City::lists('title_geo', 'id');
        return view('admin.city-district-add', $data)->with('obj', $obj);
    }

    public function editDistrict($id)
    {
        $obj = District::findOrFail($id);
        $data['cities'] = City::lists('title_geo', 'id');
        return view('admin.city-district-add', $data)->with('obj', $obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::findOrFail($id);

        $city->title_geo = $request->input('title_geo');
        $city->title_eng = $request->input('title_eng');
        $city->title_rus = $request->input('title_rus');
        $city->latitude = $request->input('latitude');
        $city->longitude = $request->input('longitude');

        $city->save();
        return Redirect::route('admin.city-district.city.edit', [$id, 'status' => 6]);
    }

    public function updateDistrict(Request $request, $id)
    {
        $district = District::findOrFail($id);

        $district->title_geo = $request->input('title_geo');
        $district->title_eng = $request->input('title_eng');
        $district->title_rus = $request->input('title_rus');
        $district->city_id = $request->input('city_id');

        $district->save();
        return Redirect::route('admin.city-district.district.edit', [$id, 'isDistrict' => 1, 'status' => 6]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        try{
            $city->destroy($id);
        } catch(Exception $e){
            abort(404, "Can't Delete");
        }
        return Redirect::route('admin.city-district.show', ['status' => 5, 'name' => $city->title_geo]);
    }

    public function destroyDistrict($id)
    {
        $district = District::findOrFail($id);
        try{
            $district->destroy($id);
        } catch(Exception $e){
            abort(404, "Can't Delete");
        }
        return Redirect::route('admin.city-district.show', ['status' => 5, 'name' => $district->title_geo]);
    }
}
