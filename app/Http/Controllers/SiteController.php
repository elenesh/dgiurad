<?php

namespace App\Http\Controllers;

use App\DeletedLinks;
use App\FooterLinks;
use App\Payment;
use App\PrePayment;
use App\Reset;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\City;
use App\Application;
use App\Type;
use App\Condition;
use App\District;
use App\Attribute;
use App\Image;
use Illuminate\Support\Facades\App;
use Intervention\Image\ImageManagerStatic;
use App\SiteUsers;
use File;
use DB;
use Mockery\CountValidator\Exception;
use Validator;
use Redirect;
use Hash;
use Cache;
use Cookie;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->with('districts')->orderBy('title_geo', 'asc')->get();
        $districts = $this->getCityDistricts($districts);
        $districts = json_encode($districts);
        $data['districts'] = $districts;
        $data['types'] = Type::all();
        $data['conditions'] = Condition::all();
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();
        $data['superVIP'] = $this->setAppType(Application::with('city')->with('district')->with('defaultImage')->with('type')->where('rank', 2)->where('active', 1)->orderBy(DB::raw('RAND()'))->take(6)->get());
        $data['VIP'] = $this->setAppType(Application::with('city')->with('district')->with('defaultImage')->with('type')->where('rank', 1)->where('active', 1)->orderBy(DB::raw('RAND()'))->take(8)->get());
        $data['NEW'] = $this->setAppType(Application::with('city')->with('district')->with('defaultImage')->with('type')->where('active', 1)->orderBy('created_at', 'desc')->take(9)->get());

        $meta = $this->getMetaData('index');
        $data['title'] = $meta['title'];
        $data['description'] = $meta['description'];

        return view('site.index', $data);
    }

    private function setAppType($obj, $i = 0){
        if($i == 0){
            foreach($obj as $d){
                if($d->hourly_price != 0 && $d->daily_price != 0){
                    $d->app_type = 'daily-hourly';
                }else if($d->hourly_price == 0 && $d->daily_price != 0){
                    $d->app_type = 'daily';
                }else if($d->hourly_price != 0 && $d->daily_price == 0){
                    $d->app_type = 'hourly';
                }else{
                    $d->app_type = 'daily-hourly';
                }
                $d->end = $this->getReversedDate($d->end_time);
                $d->start = $this->getReversedDate($d->created_at);
            }
        }else{
            if($obj->hourly_price != 0 && $obj->daily_price != 0){
                $obj->app_type = 'daily-hourly';
            }else if($obj->hourly_price == 0 && $obj->daily_price != 0){
                $obj->app_type = 'daily';
            }else if($obj->hourly_price != 0 && $obj->daily_price == 0){
                $obj->app_type = 'hourly';
            }else{
                $obj->app_type = 'daily-hourly';
            }
            $obj->end = $this->getReversedDate($obj->end_time);
            $obj->start = $this->getReversedDate($obj->created_at);
        }
        return $obj;
    }

    private function setCurrency($obj, $currency){

        $cur = Cache::remember('cur', 360, function() {
            return \Helpers::getCurrencyFromNBG();
        });

        foreach($obj as $d){
            if($d->currency != $currency){
                if($currency == 'GEL'){
                    $d->hourly_price = number_format($d->hourly_price * $cur, 0);
                    $d->daily_price = number_format($d->daily_price * $cur, 0);
                    $d->currency = 'GEL';
                }else{
                    $d->hourly_price = number_format($d->hourly_price / $cur, 0);
                    $d->daily_price = number_format($d->daily_price / $cur, 0);
                    $d->currency = 'USD';
                }
            }else{
                $d->hourly_price = number_format($d->hourly_price , 0);
                $d->daily_price = number_format($d->daily_price , 0);
            }
        }

        return $obj;
    }

    private function getCityDistricts($obj){
        $arr = [];
        foreach($obj as $d){
            $arr[] = [
                'title' => \L10nHelper::get($d),
                'districts' => $this->getDistricts($d->districts),
                
            ];
        }
        return $arr;
    }

    private function getDistricts($obj){
        $arr = [];
        foreach($obj as $d){
            $arr[] = [
                'id' => $d->id,
                'title' => \L10nHelper::get($d),
            ];
        }
        return $arr;
    }

    public function search(Request $request){

        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = City::with('districts')->orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = $this->getCityDistricts($districts);

        $data['districtsArray'] = $districts;
        $districts = json_encode($districts);
        $data['districts'] = $districts;
        $data['types'] = Type::all();
        $data['conditions'] = Condition::all();
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();

        $query = Application::with(['city', 'district', 'defaultImage', 'type'])
                        ->orderBy('rank', 'desc')
                        ->orderBy('created_at', 'desc');

        if($request->has('city')) {
            $arr = [];
            foreach ($request->input('city') as $c) {
                $arr[] = explode('_', $c)[0];
            }
            $query->whereIn('city_id', $arr);
        }

        if($request->has('region')){
            $arr = [];
            foreach($request->input('region') as $c){
                $arr[] = explode('_', $c)[0];
            }
            $query->whereIn('district_id', $arr);
        }

        $cur = Cache::remember('cur', 60, function() {
            return \Helpers::getCurrencyFromNBG();
        });

        if($request->input('currency') == 'USD'){
            $query->where(function ($que) use ($request, $cur) {

                $que->where('currency', 'USD')->where(function($q) use ($request){
                    $q->whereBetween('daily_price', [$request->input('priceFrom'), $request->input('priceTo')])
                        ->where('daily_price', '>', 0);
                    $q->orWhereBetween('hourly_price', [$request->input('priceFrom'), $request->input('priceTo')])
                        ->where('hourly_price', '>', 0);
                });

                $que->orWhere('currency', 'GEL')->where(function($q) use ($request, $cur){
                    $q->whereBetween('daily_price', [((int)$request->input('priceFrom'))*$cur, ((int)$request->input('priceTo'))*$cur])
                        ->where('daily_price', '>', 0);;
                    $q->orWhereBetween('hourly_price', [((int)$request->input('priceFrom'))*$cur, ((int)$request->input('priceTo'))*$cur])
                        ->where('hourly_price', '>', 0);
                });
            });
        }else{
            $query->where(function ($que) use ($request, $cur) {

                $que->where('currency', 'GEL')->where(function($q) use ($request){
                    $q->whereBetween('daily_price', [$request->input('priceFrom'), $request->input('priceTo')])
                        ->where('daily_price', '>', 0);
                    $q->orWhereBetween('hourly_price', [$request->input('priceFrom'), $request->input('priceTo')])
                        ->where('hourly_price', '>', 0);

                });

                $que->orWhere('currency', 'USD')->where(function($q) use ($request, $cur){
                    $q->whereBetween('daily_price', [((int)$request->input('priceFrom'))/$cur, ((int)$request->input('priceTo'))/$cur])
                        ->where('daily_price', '>', 0);
                    $q->orWhereBetween('hourly_price', [((int)$request->input('priceFrom'))/$cur, ((int)$request->input('priceTo'))/$cur])
                        ->where('hourly_price', '>', 0);
                });

            });
        }

        $query->whereBetween('room_quantity', [$request->input('roomFrom'), $request->input('roomTo')]);

        if($request->has('type')){
            $query->whereIn('type_id', $request->input('type'));
        }

        if($request->has('condition')){
            $query->whereIn('condition_id', $request->input('condition'));
        }

        if($request->has('attributes')){
            foreach($request->input('attributes') as $in => $t){
                $query = $query->whereHas('attributes', function($q) use ($in) {
                    $q->where('id', $in);
                });
            }

        }

        $applications = $query->where('active', 1)->get();
        $applications = $this->setAppType($applications);
        $applications = $this->setCurrency($applications, $request->input('currency'));

        $data['applications'] = $this->makeApps($applications);
        $meta = $this->getMetaData('search');
        $data['title'] = $meta['title'];
        $data['description'] =  $meta['description'];
        return view('site.search', $data);
    }

    private function makeApps($obj){
        $data = [];
        foreach($obj as $o){
            $data[] = [
                'id' => $o->id,
                'applicationType' => $o->app_type,
                'details' => [
                    'room' => $o->room_quantity,
                    'm2' => $o->m2,
                    'type' => \L10nHelper::get($o->type),
                    'city' => \L10nHelper::get($o->city),
                    'district' => isset($o->district->title_geo) ? \L10nHelper::get($o->district) : "",
                ],
                'mainImage' => $o->defaultImage->src,
                'daily_price' => $o->daily_price,
                'hourly_price' => $o->hourly_price,
                'address' => \L10nHelper::get($o, 'street_addr'),
                'title' => \L10nHelper::get($o),
                'uniqueID' => $o->unique_id,
                'description' => \L10nHelper::get($o, 'description'),
                'currency' => $o->currency,
                'slug' => $o->slug,
                'phone' => $o->phone,
                'rank' => $o->rank,
                'location' => $this->getLocation($o->latitude, $o->longitude)
            ];
        }
        return $data;
    }

    private function getLocation($latitude, $longitude){
        if($latitude == '41.70451' && $longitude == '44.80912'){
            return [];
        }else{
            return [$latitude, $longitude];
        }
    }


    public function showSingle($slug, $id, Request $request){

        $data['application'] = Application::where('unique_id', $id)->with('user')->with('defaultImage')->with('images')->with('attributes')->with('type')->with('condition')->first();
        if(!isset($data['application']->id)){
            $deleted = DeletedLinks::where('unique_id', $id)->first();
            if(!isset($deleted->id)){
                return Redirect::route('index');
            }
            return Redirect::route('search','city[]='.$deleted->city_id.'_0&priceFrom=0&priceTo=900&roomFrom=0&roomTo=15&currency=USD');
        }
        if($slug != $data['application']->slug) return Redirect::route('single', [$data['application']->slug, $id]);
        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = City::with('districts')->orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = $this->getCityDistricts($districts);
        $districts = json_encode($districts);
        $data['districts'] = $districts;
        $data['types'] = Type::all();
        $data['conditions'] = Condition::all();
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();

        $data['related_apps'] = $this->setAppType($this->getRelatedApps($data['application']));

        $data['title'] = \L10nHelper::get($data['application']);
        $data['description'] = mb_substr(\L10nHelper::get($data['application'], 'description'), 0, 155);

        $data['application']->date_created = $this->getReversedDate($data['application']->created_at);
        $data['application'] = $this->setAppType($data['application'], 1);

        $response = new \Illuminate\Http\Response(view('site.single', $data));
        if($request->cookie($id) != 1){
            $response->withCookie(cookie()->forever($id, 1));
            $views = Application::where('unique_id', $id)->first();
            $views->views += 1;
            $views->save();
        }

        return $response;
    }

    private function getReversedDate($date){
        $time = strtotime($date);
        $date = date('d/m/Y',$time);

        return $date;
    }

    private function getRelatedApps($app){
        $apps = Application::where('city_id', $app->city_id)->where('active', 1);
        if($app->daily_price > 0 && $app->hourly_price == 0){
            $apps->whereBetween('daily_price', [$app->daily_price - 20, $app->daily_price - 20]);
        }else if($app->hourly_price > 0 && $app->daily_price == 0){
            $apps->whereBetween('hourly_price', [$app->hourly_price - 20, $app->hourly_price - 20]);
        }else if($app->hourly_price > 0 && $app->daily_price > 0){
            $apps->where(function($q) use ($app){
                $q->whereBetween('daily_price', [$app->daily_price - 20, $app->daily_price + 20]);
                $q->orWhereBetween('hourly_price', [$app->hourly_price - 20, $app->hourly_price + 20]);
            });
        }
        $aps = $apps->get();
        if(!isset($aps['0'])){
            $aps = Application::where('city_id', $app->city_id)->where('active', 1)->get();
        }
        return $aps;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLogin()
    {
        $auth = auth()->guard('siteUsers');
        if($auth->check()){
            $data['user'] = $auth->user();
            return Redirect::route('user.area');
        }else{
            $meta = $this->getMetaData('login');
            $data['heading'] = $meta['title'];
            $data['title'] = $meta['title'];
            $data['description'] = $meta['description'];
            return view('site.reg-login', $data);
        }   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $post = $request;
        $gRecaptchaResponse = $request->input('g-recaptcha-response');
        $remoteIp = $request->getClientIp();
        $secret = '6Ld-uxMTAAAAAGBH4uxVCG_AUECgZdW6M52A6Rri';
        $recaptcha = new \ReCaptcha\ReCaptcha($secret);
        $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
        if ($resp->isSuccess()) {

            $auth = auth()->guard('siteUsers');

            $credentials = [
                'email' =>  $request->input('email'),
                'password' =>  $request->input('password'),
            ];

            if ($auth->attempt($credentials, true)) {
                return ['status' => 1];
            }else{
                return ['status' => 0, 'title' => trans('main.error'), 'msg' => [trans('main.wrong-email-or-pass')]];
            }

        } else {
            $errors = $resp->getErrorCodes();
            return ['status' => 0, 'title' => trans('main.error'), 'msg' => [trans('main.bot')]];

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function register(Request $request)
    {
        $post = $request;
        $gRecaptchaResponse = $request->input('g-recaptcha-response');
        $remoteIp = $request->getClientIp();
        $secret = '6Ld-uxMTAAAAAGBH4uxVCG_AUECgZdW6M52A6Rri';
        $recaptcha = new \ReCaptcha\ReCaptcha($secret);
        $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
        if ($resp->isSuccess()) {
            $names = [
                'email' => trans('main.email'),
                'phone' => trans('main.phone'),
            ];

            $v = Validator::make($request->all(), [
                'email' => 'required|unique:users|max:255|email',
                'password' => 'required|min:8|confirmed',
                'password_confirmation' => 'required',
                'phone' => 'required|min:5|numeric',
            ]);

            $v->setAttributeNames($names);

            if($v->fails()){
                $errorsAll = $v->errors()->all();
                $errors['status'] = 0;
                $errors['title'] = trans('main.error');
                $errors['msg'] = $errorsAll;
                return $errors;
            }

            $user = new SiteUsers();
            $user->fill($request->all());
            $user->password = bcrypt($user->password);
            $unique = new UserController();

            if($request->birth_date != ''){
                $user->birth_date = \Helpers::getDate($request->birth_date);
            }

            $user->unique_id = $unique->getUniqueID();
            $user->save();

            $auth = auth()->guard('siteUsers');
            $credentials = [
                'email' =>  $request->input('email'),
                'password' =>  $request->input('password'),
            ];

            if ($auth->attempt($credentials, true)) {
                return ['status' => 1];
            }else{
                return ['status' => 0, 'title' => trans('main.error'), 'msg' => [trans('main.cant-login')]];
            }

        } else {
            $errors = $resp->getErrorCodes();
            return ['status' => 0, 'title' => trans('main.error'), 'msg' => [trans('main.bot')]];

        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        $auth = auth()->guard('siteUsers');
        if($auth->check()){
            $user = $auth->user();
            $data['apps'] = Application::where('user_id', $user->id)->where('active', 1)->get();
            $data['inactiveApps'] = Application::where('user_id', $user->id)->where('active', 0)->get();
            $data['apps'] = $this->setAppType($data['apps']);
            $data['inactiveApps'] = $this->setAppType($data['inactiveApps']);
            return view('site.user-area', $data);
        }else{
            return Redirect::route('show.login');
        }    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        auth()->guard('siteUsers')->logout();
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
        return Redirect::route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        if(!auth()->guard('siteUsers')->check()){
           return Redirect::route('login');
        }
        $loc = \L10nHelper::getLocale();
        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->lists('title_'.$loc, 'id');
        $data['types'] = Type::lists('title_'.$loc, 'id');
        $firstCity = City::orderBy('rank', 'desc')->firstOrFail();
        $data['districts'] = District::where('city_id', $firstCity->id)->orderBy('title_geo', 'asc')->lists('title_'.$loc, 'id');
        $data['conditions'] = Condition::lists('title_'.$loc, 'id');
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();
        return view('site.application-add', $data);
    }

    private function ifImagesAreValid($request){
        $photos = $request->file('photo');
        foreach ($photos as $key=>$photo) {
            if ($photo != null){
                $validator = Validator::make(['photo' => $photo], [
                    'photo' => 'image|mimes:jpeg,bmp,png'
                ]);

                if ($validator->fails()) {
                    return ['success' => false];
                }

                if (!($photo->isValid()) || $photo->getClientSize() > 4000000) {
                    return 0;
                }
            }
        }
        return 1;

    }

    public function create(Request $request)
    {
        if($this->ifImagesAreValid($request) == 0) return Redirect::route('user.area');

        $validator = Validator::make($request->all(), [
            'title_geo' => 'required|max:50',
            'title_eng' => 'max:50',
            'title_rus' => 'max:50',
            'description_geo' => 'required|max:3000',
            'description_eng' => 'max:3000',
            'description_rus' => 'max:3000',
            'street_addr_geo' => 'required|max:100',
            'street_addr_eng' => 'max:100',
            'street_addr_rus' => 'max:100',
            'phone' => 'required|min:5|numeric',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return ['success' => false];
        }

        $auth = auth()->guard('siteUsers');
        $user = $auth->user();  

        $application = new Application();
        $application->fill($request->all());
        $application->currency = $request->input('currency');
        $application->default_image_id = NULL;
        $application->unique_id = $this->getUniqueID();
        $application->end_time = $this->getEndTime($request->input('num_of_days'));
        $application->district_id = $request->input('district_id') == '' ? NULL : $request->input('district_id');
        $application->user_id =  $user->id;
        $application->slug = $this->makeSlug($request->input('title_geo'));


    

        try{
            $application->save();
        }catch(Exceprion $e){
            return ['success' => false];
        }
        $appl = Application::where('unique_id', $application->unique_id)->firstOrFail();
        $appl->attributes()->attach($request->input('attributes'));

        if($request->hasFile('photo')) {
            $i = 0;
            foreach ($request->file('photo') as $image) {
                if ($image != null) {
                    if ($image->isValid()) {
                        $img = new Image();
                        $img->application_id = $appl->id;
                        $path = public_path() . '/uploads/applications/' . $appl->id . "/";
                        $fileName = str_random(32) . '.' . $image->getClientOriginalExtension();
                        $image->move($path, $fileName);
                        $this->resizeImage($path, $fileName);
                        $img->src = $fileName;
                        $img->save();

                        if ($i == 0) {
                            $defImg = Image::where('src', $fileName)->firstOrFail();
                            $ap = Application::findOrFail($appl->id);
                            $ap->default_image_id = $defImg->id;
                            $ap->save();
                            $i++;
                        }
                    } else {
                        \App::abort(404);
                    }
                }

            }
        }else{
            $ap = Application::findOrFail($appl->id);
            $img = new Image();
            $path = public_path() . '/uploads/applications/'.$appl->id."/";
            File::makeDirectory($path);
            File::copy(public_path().'/uploads/applications/1_default.jpg', $path.'1_default.jpg');
            File::copy(public_path().'/uploads/applications/2_default.jpg', $path.'2_default.jpg');
            $img->src = 'default.jpg';

            $ap->default_image_id = 1;

            $img->save();
            $ap->save();
        }
        return Redirect::route('user.area');
    }

    private function makeSlug($title){
        $geo = [' ', 'ა', 'ბ', 'გ', 'დ', 'ე', 'ვ', 'ზ', 'თ', 'ი', 'კ', 'ლ', 'მ', 'ნ', 'ო', 'პ', 'ჟ', 'რ', 'ს', 'ტ', 'უ', 'ფ', 'ქ', 'ღ', 'ყ', 'შ', 'ჩ', 'ც', 'ძ', 'წ', 'ჭ', 'ხ', 'ჯ', 'ჰ'];
        $geoEng = ['-', 'a', 'b', 'g', 'd', 'e', 'v', 'z', 't', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'zh', 'r', 's', 't', 'u', 'f', 'q', 'gh', 'y', 'sh', 'ch', 'c', 'dz', 'w', 'tch', 'x', 'j', 'h'];

        return str_slug(str_replace($geo, $geoEng, $title), '-');
    }
    public function getUniqueID(){
        $uniqueID = rand(100000000000,999999999999);
        while($this->checkUniqueID($uniqueID) == 1){
            $uniqueID = rand(100000000000,999999999999);
        }
        return $uniqueID;
    }

     public function checkUniqueID($uniqueID){
        $application = Application::where('unique_id', $uniqueID)->get();
        if(isset($application[0])){
            return 1;
        }else{
            return 0;
        }
    }
    public function getEndTime($days){
        date_default_timezone_set('Asia/Tbilisi');
        return date('Y-m-d H:i:s', strtotime('+ '.$days.' days'));
    }


    private function resizeImage($path, $imgName){
        $sizes = getimagesize($path.$imgName);
        if($sizes[0] > $sizes[1]){
            ImageManagerStatic::make($path.$imgName)->fit(920,474)->insert(public_path() . "/uploads/applications/watermark.png",'bottom-right', 30, 30)->save($path."1_".$imgName);
        }else{
            ImageManagerStatic::make($path.$imgName)->heighten(474)->insert(public_path() . "/uploads/applications/watermark.png",'bottom-right', 30, 30)->save($path."1_".$imgName);
        }
        ImageManagerStatic::make($path.$imgName)->fit(440,226)->save($path."2_".$imgName);
        File::delete($path.$imgName);
    }

    public function edit($id){
        $loc = \L10nHelper::getLocale();
        $application = Application::with('attributes')->findOrFail($id);
        if(!$this->checkUser($application)) return Redirect::route('user.area');
        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->lists('title_'.$loc, 'id');
        $data['types'] = Type::lists('title_'.$loc, 'id');
        $data['districts'] = District::where('city_id', $application->city_id)->orderBy('title_geo', 'asc')->lists('title_'.$loc, 'id');
        if(!isset($districts[0])){
            $data['districts'][null] = '';
        }
        $data['conditions'] = Condition::lists('title_'.$loc, 'id');
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();
        return view('site.application-add', $data)->with('obj', $application);
    }

    private function checkUser($application){
        $auth = auth()->guard('siteUsers');
        $user = $auth->user();

        if($application->user_id != $user->id){
            return false;
        }else{
            return true;
        }
    }

    public function update($id, Request $request){
        $application = Application::with('images')->findOrFail($id);

        if(!$this->checkUser($application)) return Redirect::route('user.area');

        if($this->ifImagesAreValid($request) == 0) return Redirect::route('user.area');

        $validator = Validator::make($request->all(), [
            'title_geo' => 'required|max:50',
            'title_eng' => 'max:50',
            'title_rus' => 'max:50',
            'description_geo' => 'required|max:3000',
            'description_eng' => 'max:3000',
            'description_rus' => 'max:3000',
            'street_addr_geo' => 'required|max:100',
            'street_addr_eng' => 'max:100',
            'street_addr_rus' => 'max:100',
            'phone' => 'required|min:5|numeric',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return ['success' => false];
        }

        $application->fill($request->all());
        $application->slug = $this->makeSlug($request->input('title_geo'));
        $application->currency = $request->input('currency');
        $application->district_id = $request->input('district_id') == '' || $request->input('district_id') == 0 ? NULL : $request->input('district_id');
        $attributes = [];
        foreach($application->attributes as $a){
            $attributes[] = $a->id;
        }
        $application->attributes()->detach($attributes);
        $application->attributes()->attach($request->input('attributes'));
        $i = 0;

        foreach($request->file('photo') as $image)
        {
            if($image != null){
                if ($image->isValid()){
                    $img = new Image();
                    $img->application_id = $id;
                    $path = public_path().'/uploads/applications/'. $id."/";
                    $fileName = str_random(32).'.'.$image->getClientOriginalExtension();
                    $image->move($path, $fileName);
                    $this->resizeImage($path, $fileName);
                    $img->src = $fileName;
                    $img->save();

                    if (!isset($application->images[0]) && $i == 0) {
                        $defImg = Image::where('src', $fileName)->firstOrFail();
                        $application->default_image_id = $defImg->id;
                        $application->save();
                        $i++;
                    }

                }else{
                    \App::abort(404);
                }
            }
            
        }
        $application->save();
        return Redirect::route('user.area');
    }

    public function editDefaultImage(Request $request){
        $application = Application::findOrFail($_POST['app_id']);

        if(!$this->checkUser($application)) return Redirect::route('user.area');

        $application->default_image_id = $request->input('img_id');

        try{
            $application->save();
            return 1;
        }catch (Exception $e){
            return 0;
        }
    }

    public function deleteImage(Request $request){
        $id = $request->input('id');
        $appl = Application::findOrFail($request->input('obj_id'));

        if(!$this->checkUser($appl)) return Redirect::route('user.area');

        if($id != $appl->default_image_id){
            $image = Image::findOrFail($id);
            try{
                Image::destroy($id);
                File::delete(public_path() . "/uploads/applications/".$image->application_id."/1_" . $image->src);
                File::delete(public_path() . "/uploads/applications/".$image->application_id."/2_" . $image->src);
            }catch (Exceprion $e){
                App::abort(404);
            }
            return 1;
        }else{
            return 2;
        }    
    }

    public function deleteApp($id){
        $application = Application::with('images')->with('attributes')->findOrFail($id);
        $deletedLink = new DeletedLinks();
        $deletedLink->unique_id = $application->unique_id;
        $deletedLink->city_id = $application->city_id;

        if(!$this->checkUser($application)) return Redirect::route('user.area');

        try{
            if(isset($application->images[0])){
                foreach($application->images as $i){
                    Image::destroy($i->id);
                }
            }
            File::deleteDirectory(public_path() . "/uploads/applications/".$id);
            $application->destroy($id);
            $deletedLink->save();
        }catch (Exceprion $e){
            App::abort(404);
        }
        return Redirect::route('user.area');
    }

    public function profileEdit(){
        $auth = auth()->guard('siteUsers');
        $user = $auth->user();

        return view('site.profile-edit')->with('obj', $user);  
    }

    public function profileUpdate(Request $request){
        $names = [
            'phone' => trans('main.phone'),
        ];

        $v = Validator::make($request->all(), [
            'phone' => 'required|min:5|numeric',
        ]);

        $v->setAttributeNames($names);

        if($v->fails()){
            $errorsAll = $v->errors()->all();
            $errors['status'] = 0;
            $errors['title'] = trans('main.error');
            $errors['msg'] = $errorsAll;
            return $errors;
        }

        $auth = auth()->guard('siteUsers');
        $user = $auth->user();
        $id = $user->id;
        $us = SiteUsers::findOrFail($id);
        $us->fill($request->all());
        $us->birth_date = \Helpers::getDate($request->input('birth_date'));
        $us->save();

        return Redirect::route('user.profile.edit');  
    }

    public function passwordReset(Request $request){
        $names = [
            'old_password' => trans('main.old-pass'),
            'password' => trans('main.pass'),
            'password_confirmation' => trans('main.confirmed-pass'),
        ];

        $v = Validator::make($request->all(), [
            'old_password' => 'required|min:8',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);

        $v->setAttributeNames($names);

        if($v->fails()){
            $errorsAll = $v->errors()->all();
            $errors['status'] = 0;
            $errors['title'] = trans('main.error');
            $errors['msg'] = $errorsAll;
            return $errors;
        }

        $auth = auth()->guard('siteUsers');
        $user = $auth->user();

        if (Hash::check($request->input('old_password'), $user->password)) {
            $us = SiteUsers::findOrFail($user->id);
            $us->password = Hash::make($request->input('password'));
            $us->save();

            return ['status' => 1];
        }else{
            $errors['status'] = 0;
            $errors['title'] = trans('main.error');
            $errors['msg'] = [trans('main.please-enter-old-pass')];
            return $errors;
        }
    }

    public function getDistrictsForCityId(Request $request){
        $districts = District::where('city_id', $request->input('id'))->orderBy('title_geo', 'asc')->get();
        for($i = 0; $i < count($districts); $i++){
            $districts[$i] = "<option value='".$districts[$i]->id."'>".\L10nHelper::get($districts[$i])."</option>";
        }
        return $districts;
    }

    public function reset(){
        $meta = $this->getMetaData('reset');
        $data['heading'] = $meta['title'];
        $data['title'] = $meta['title'];
        $data['description'] = $meta['description'];
        return view('site.reset', $data);
    }

    public function resetPost(Request $request){
        $email = $request->input('email');
        $user = SiteUsers::where('email', $email)->get();
        if(isset($user[0])){
            $reset = new Reset();
            $reset->user_id = $user[0]->id;
            $reset->uri = str_random(32);
            $reset->save();

            $subject = trans('main.restore-pass').' | DGIURAD.GE';
            $message = trans('main.for-restore-go-to-this-link').': <a href="http://dgiurad.ge/ka/login/reset/'.$reset->uri.'">http://dgiurad.ge/ka/login/reset/'.$reset->uri.'</a>';
            $headers = "From: noreply@dgiurad.ge\n";
            $headers .= "MIME-Version: 1.0\n" . "Content-Type: text/html;charset=utf-8\n";
            mail($email, $subject, $message, $headers);

            return ['status' => 1];
        }else{
            return ['status' => 0];
        }

    }

    public function resetPass($uri){
        $meta = $this->getMetaData('reset');
        $data['heading'] = $meta['title'];
        $data['title'] = $meta['title'];
        $data['description'] = $meta['description'];
        $data['_i'] = 1;

        Reset::where('uri', $uri)->firstOrFail();

        return view('site.reset', $data);
    }

    public function resetNewPass($uri, Request $request){
        $names = [
            'password' => trans('main.pass'),
            'password_confirmation' => trans('main.confirmed-pass'),
        ];

        $v = Validator::make($request->all(), [
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);

        $v->setAttributeNames($names);

        if($v->fails()){
            $errorsAll = $v->errors()->all();
            $errors['status'] = 0;
            $errors['msg'] = $errorsAll;
            return $errors;
        }

        $reset = Reset::where('uri', $uri)->firstOrFail();
        $user = SiteUsers::findOrFail($reset->user_id);

        $user->password = Hash::make($request->input('password'));

        try{
            $user->save();
            Reset::destroy($reset->id);
            return ['status' => 1, 'msg' => [trans('main.password-successfully-restored').' <a href="'.route('login').'">'.trans('main.login').'</a>']];
        }catch (Exception $e){
            return ['status' => 0, 'msg' => [trans('main.cant-change-pass')]];
        }
    }

    public function contact(){
        $meta = $this->getMetaData('contact');
        $data['heading'] = $meta['title'];
        $data['title'] = $meta['title'];
        $data['description'] = $meta['description'];

        return view('site.contact',$data);
    }

    public function contactSend(Request $request){
        $post = $request;
        $gRecaptchaResponse = $request->input('g-recaptcha-response');
        $remoteIp = $request->getClientIp();
        $secret = '6Ld-uxMTAAAAAGBH4uxVCG_AUECgZdW6M52A6Rri';
        $recaptcha = new \ReCaptcha\ReCaptcha($secret);
        $P = [
              "debug" => "0",
              "captchaStat" => "1",
              "rec_email_t" => trans('main.message'),
              "rec_email" => "info@dgiurad.ge",
              "rec_email_s" => trans('main.new-messae-from-dgiurad'),
              "from_email_input" => "email",
              "err_captcha_t" => trans('main.bot'),
              "empty_captcha_t" => '*********',
              "sent_email_t" => trans('main.message-sent'),
              "sent_email_body" => trans('main.message-recieved'),
              "err_somthing_t" => trans('main.message-failed'),
              "cc_emails" => "",
              "bcc_emails" => "",
            ];
        $resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);
        function returnF($err, $errMsg, $title = '', $body = ''){
              global $P;
              echo $err. '$%&' .$errMsg. '$%&' .$title;
              exit;
            }

        if ($resp->isSuccess()) {
            

            //load submited data
            $list_data = '';
            $posted_data = $_POST;
            unset($posted_data['aquaria_captcha-form']);
            foreach ($posted_data as $key => $value) {//building list
              $value = htmlspecialchars($value, ENT_QUOTES,"UTF-8");
              if($key == $P['from_email_input']) $from_email = $value; //From email address
              if($value ==! '') $list_data .= "<li><b>$key </b>: $value</li>";
            }
            $body = '<!DOCTYPE html>
                     <html>
                     <html>
                     <head>
                       <meta charset="utf-8">
                     </head>
                     <body>
                         <h4>'.$P['rec_email_t'].'</h4>
                         <ul>'.$list_data.'</ul>
                     </body>
                     </html>';//base html to display message

            // boundary 
            $semi_rand = md5(time()); 
            $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
            $attach = "";

            $headers = "";

            if(isset($from_email)){//if from email input has been set
              $headers .= "From: $from_email" . "\n";
              $headers .= "Reply-To: $from_email" . "\n";
            }

            $headers .= "Cc: ".$P['cc_emails']."" . "\n";
            $headers .= "Bcc: ".$P['bcc_emails']."" . "\n";
            $headers .= "MIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";
            $message = "--{$mime_boundary}\n" . "Content-Type: text/html; charset=utf-8\n" . "Content-Transfer-Encoding:  7bit\n\n" . $body . "\n\n";
            $message .= $attach;//file attach

            //mail send function
            if(mail($P['rec_email'],$P['rec_email_s'],$message,$headers)){
              returnF(0, $P['sent_email_body'], $P['sent_email_t'], $body);
            }
            else returnF(1, $P['err_somthing_t']);

        } else {
            $errors = $resp->getErrorCodes();
            returnF(1, $P['err_captcha_t']);

        }

        //return view('site.contact',$data);
    }
    public function getFooterLink($slug, Request $request){
        $footerLink = FooterLinks::where('slug', $slug)->firstOrFail();

        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = City::with('districts')->orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = $this->getCityDistricts($districts);

        $data['districtsArray'] = $districts;
        $districts = json_encode($districts);
        $data['districts'] = $districts;
        $data['types'] = Type::all();
        $data['conditions'] = Condition::all();
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();

        $query = Application::where('city_id', $footerLink->city_id)
            ->with(['city', 'district', 'defaultImage', 'type'])
            ->orderBy('rank', 'desc')
            ->orderBy('created_at', 'desc');

        $applications = $query->get();
        $applications = $this->setAppType($applications);
        $applications = $this->setCurrency($applications, $request->input('currency'));

        $data['applications'] = $this->makeApps($applications);
        $meta = $this->getMetaData('search');
        $data['title'] = \L10nHelper::get($footerLink);
        $data['description'] =  mb_substr(\L10nHelper::get($footerLink, 'description'), 0, 155);
        $data['headTitle'] = \L10nHelper::get($footerLink);
        $data['headDescription'] = \L10nHelper::get($footerLink, 'description');
        return view('site.search', $data);
    }

    public function instructions(){
        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = City::with('districts')->orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = $this->getCityDistricts($districts);

        $data['districtsArray'] = $districts;
        $districts = json_encode($districts);
        $data['districts'] = $districts;
        $data['types'] = Type::all();
        $data['conditions'] = Condition::all();
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();
        return view('site.instructions', $data);
    }

    public function showFAQ(){
        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = City::with('districts')->orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = $this->getCityDistricts($districts);

        $data['districtsArray'] = $districts;
        $districts = json_encode($districts);
        $data['districts'] = $districts;
        $data['types'] = Type::all();
        $data['conditions'] = Condition::all();
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();
        return view('site.faq', $data);
    }
     public function showPrivacy(){
        $data['cities'] = City::orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = City::with('districts')->orderBy('rank', 'desc')->orderBy('title_geo', 'asc')->get();
        $districts = $this->getCityDistricts($districts);

        $data['districtsArray'] = $districts;
        $districts = json_encode($districts);
        $data['districts'] = $districts;
        $data['types'] = Type::all();
        $data['conditions'] = Condition::all();
        $data['attributes'] = Attribute::orderBy('rank','desc')->get();
        return view('site.privacy', $data);
    }

    public function pay(){
        return view('site.pay');
    }

    public function prePaymentSave(Request $request){
        $prePayment = new PrePayment();
        $auth = auth()->guard('siteUsers');
        $user = $auth->user();
        $prePayment->amount = $request->input('amount');
        $prePayment->trans_id = $request->input('tid');
        $prePayment->user_id = $user->id;
        $prePayment->save();
        return 1;
    }

    public function paymentCheck(Request $request){
        $transID = $request->input('tid');
        $curl = new \anlutro\cURL\cURL;
        $url = $curl->buildUrl('https://new.mypay.ge:1919/api/gw/GetTransactionStatus', ['transactionId' => $transID]);
        $resp = $curl->get($url);
        $data = json_decode($resp->body);

        return $data->Status;
    }


    public function paymentSuccess(){
        if(isset($_COOKIE['trans-id'])) {
            return view('site.payment-success');
        }else{
            return Redirect::route('user.area');
        }
    }

    public function showSuccess(){
        return view('site.pay-success');
    }

    public function makeVIP($id, $vip){
        $application = Application::findOrFail($id);
        $auth = auth()->guard('siteUsers');
        $user = $auth->user();

        if($vip == 1) {
            if ($user->balance < 1) {
                return ['status' => 2];
            }

            $application->rank = 1;
            $user->balance -= 1;
        }else if($vip == 0){
            if ($user->balance < 2) {
                return ['status' => 2];
            }

            $application->rank = 2;
            $user->balance -= 2;
        }
        date_default_timezone_set('Asia/Tbilisi');
        $application->vip_start_date = date('Y-m-d H:i:s', time());
        $application->vip_end_date = date('Y-m-d H:i:s', strtotime('+ 7 days'));
        try{
            $application->save();
            $user->save();
        }catch (Exception $e){
            App::abort(404);
        }

        return ['status' => 1];
    }

    public function makeActive($id){
        $app = Application::findOrFail($id);
        $app->active = 1;
        $app->end_time = \Carbon\Carbon::now()->addMonth();
        $app->save();
        return Redirect::route('user.area');
    }

    private function getMetaData($key){
        $arr = [
            'index' => [
                'title' => trans('main.index-title'),
                'description' => trans('main.index-desc'),
            ],
            'search' => [
                'title' => trans('main.search-title'),
                'description' => trans('main.search-desc'),
            ],
            'login' => [
                'title' => trans('main.login-title'),
                'description' => trans('main.login-desc'),
            ],
            'reset' => [
                'title' => trans('main.reset-title'),
                'description' => trans('main.reset-desc'),
            ],
            'contact' => [
                'title' => trans('main.contact-title'),
                'description' => trans('main.contact-desc'),
            ],

        ];
        return $arr[$key];
    }


}
