<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Attribute;
use Redirect;

class AttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['attributes'] = Attribute::all();
        return view('admin.attributes', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.attributes-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribute = new Attribute();

        $attribute->title_geo = $request->input('title_geo');
        $attribute->title_eng = $request->input('title_eng');
        $attribute->title_rus = $request->input('title_rus');

        $attribute->save();
        return Redirect::route('admin.attributes.show', ['status' => 4, 'name' => $attribute->title_geo]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = Attribute::findOrFail($id);
        return view('admin.attributes-add')->with('obj', $obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attribute = Attribute::findOrFail($id);

        $attribute->title_geo = $request->input('title_geo');
        $attribute->title_eng = $request->input('title_eng');
        $attribute->title_rus = $request->input('title_rus');

        $attribute->save();
        return Redirect::route('admin.attributes.edit',[$id, 'status' => 6]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attribute = Attribute::findOrFail($id);
        try{
            $attribute->destroy($id);
        } catch(Exception $e){
            abort(404, "Can't Delete");
        }
        return Redirect::route('admin.attributes.show', ['status' => 5, 'name' => $attribute->title_geo]);
    }
}
