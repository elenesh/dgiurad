<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Condition;
use Redirect;

class ConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['conditions'] = Condition::all();
        return view('admin.condition', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.condition-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $condition = new Condition();

        $condition->title_geo = $request->input('title_geo');
        $condition->title_eng = $request->input('title_eng');
        $condition->title_rus = $request->input('title_rus');

        $condition->save();
        return Redirect::route('admin.condition.show', ['status' => 4, 'name' => $condition->title_geo]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = Condition::findOrFail($id);
        return view('admin.condition-add')->with('obj', $obj);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $condition = Condition::findOrFail($id);

        $condition->title_geo = $request->input('title_geo');
        $condition->title_eng = $request->input('title_eng');
        $condition->title_rus = $request->input('title_rus');

        $condition->save();
        return Redirect::route('admin.condition.edit',[$id, 'status' => 6]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $condition = Condition::findOrFail($id);
        try{
            $condition->destroy($id);
        } catch(Exception $e){
            abort(404, "Can't Delete");
        }
        return Redirect::route('admin.condition.show', ['status' => 5, 'name' => $condition->title_geo]);
    }
}
