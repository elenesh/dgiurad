<?php

$locale = Request::segment(1);
if($locale == ''){
	$locale = 'ka';
}



if (in_array($locale, Config::get('app.available_locales'))) {
	\App::setLocale($locale);
	L10nHelper::setLocale($locale);

} else {
	\App::abort(404);
}
//site routes
Route::group(['prefix' => $locale], function() {

	Route::get('/', [
			'uses' => 'SiteController@index',
			'as'   => 'index'
		]);

	Route::get('search', [
			'uses' => 'SiteController@search',
			'as'   => 'search'
		]);

	Route::get('contact', [
			'uses' => 'SiteController@contact',
			'as'   => 'contact'
		]);
	Route::post('contact', [
			'uses' => 'SiteController@contactSend',
			'as'   => 'contact.send'
		]);

	Route::get('instructions', [
		'uses' => 'SiteController@instructions',
		'as'   => 'instructions'
	]);

	Route::get('faq', [
		'uses' => 'SiteController@showFAQ',
		'as'   => 'faq'
	]);

	Route::get('privacy', [
		'uses' => 'SiteController@showPrivacy',
		'as'   => 'privacy'
	]);

	Route::get('application/{slug}/{id}', [
			'uses' => 'SiteController@showSingle',
			'as'   => 'single'
		])->where('id', '[0-9]+');


	Route::group(['prefix' => 'ucc'], function() {

		Route::get('debt', [
			"uses" => 'UccController@debt'
		]);

		Route::get('pay', [
			"uses" => 'UccController@pay'
		]);
	});


	Route::group(['prefix' => 'login'], function() {

		Route::get('/', [
			'uses' => 'SiteController@showLogin',
			'as'   => 'show.login'
		]);

		Route::post('/', [
			'uses' => 'SiteController@login',
			'as'   => 'login'
		]);

		Route::get('reset', [
			'uses' => 'SiteController@reset',
			'as'   => 'show.reset'
		]);

		Route::post('reset', [
			'uses' => 'SiteController@resetPost',
			'as' => 'reset.post',
		]);

		Route::get('reset/{uri}', [
			'uses' => 'SiteController@resetPass',
			'as'   => 'show.reset.pass'
		]);

		Route::post('reset/{uri}', [
			'uses' => 'SiteController@resetNewPass',
			'as'   => 'reset.pass'
		]);

	});


	Route::get('logout', [
			'uses' => 'SiteController@logout',
			'as'   => 'logout'
		]);

	Route::post('register', [
			'uses' => 'SiteController@register',
			'as'   => 'register'
		]);

	Route::group(['prefix' => 'user-area'], function() {

		Route::get('/', [
				'uses' => 'SiteController@user',
				'as'   => 'user.area'
		]);

		Route::get('profile-edit', [
				'uses' => 'SiteController@profileEdit',
				'as'   => 'user.profile.edit'
		]);

		Route::post('profile-edit', [
				'uses' => 'SiteController@profileUpdate'
		]);

		Route::post('password-reset', [
			'uses' => 'SiteController@passwordReset',
			'as' => 'password.reset'
		]);

		Route::get('pay', [
			'uses' => 'SiteController@pay',
			'as'   => 'user.pay'
		]);

		Route::get('payment-success', [
			'uses' => 'SiteController@paymentSuccess'
		]);

		Route::get('success', [
			'uses' => 'SiteController@showSuccess',
			'as' => 'payment.success'
		]);

		Route::post('payment-check', [
			'uses' => 'SiteController@paymentCheck',
			'as' => 'payment.check'
		]);

		Route::post('pre-payment-save', [
			'uses' => 'SiteController@prePaymentSave',
			'as' => 'pre.payment.save'
		]);

        Route::get('make-vip/{id}/{vip}', [
            'uses' => 'SiteController@makeVIP',
            'as' => 'make.vip'
        ])->where('id', '[0-9]+');

		Route::get('make-active/{id}', [
			'uses' => 'SiteController@makeActive',
			'as' => 'make.active'
		])->where('id', '[0-9]+');

		Route::group(['prefix' => 'application'], function() {

			Route::get('add', [
					'uses' => 'SiteController@add',
					'as'   => 'app.add'
			]);

			Route::post('add', [
					'uses' => 'SiteController@create',
					'as'   => 'app.create'
			]);

			Route::get('edit/{id}', [
					'uses' => 'SiteController@edit',
					'as'   => 'application.edit'
			])->where('id', '[0-9]+');

			Route::post('delete-image', [
					'uses' => 'SiteController@deleteImage',
					'as'   => 'site.delete.image'
			]);

			Route::post('edit/{id}', [
				'uses' => 'SiteController@update'
			])->where('id', '[0-9]+');

			Route::get('delete/{id}', [
					'uses' => 'SiteController@deleteApp',
					'as'   => 'application.delete'
			])->where('id', '[0-9]+');

			Route::post('edit-default-image', [
				'uses' => 'SiteController@editDefaultImage',
				'as'   => 'edit.default.image'
			]);

			Route::post('getDistrictsForCityId',[
			'uses' => 'SiteController@getDistrictsForCityId',
				'as'   => 'get.districts.for.city.id'
			]);

		});

	});
});

Route::get('/', function(){
	return Redirect::route('index');
});

Route::group(['prefix' => 'admin'], function() {
	
	Route::get('/', function(){
		if (Auth::check()){
			return Redirect::route('admin.index');   
		} else {
			return Redirect::route('admin.login');
		}
	});

	Route::get('login', [
			'uses' => 'HomeController@showLogin',
			'as' => 'admin.login',
		]);
	Route::post('login', ['uses' => 'HomeController@checkLogin']);

	Route::get('logout', [
			'uses' => 'HomeController@adminLogout',
			'as' => 'admin.logout',
		]);
	Route::get('index', [
			'middleware' => 'auth',
			'uses' => 'HomeController@index',
			'as' => 'admin.index',
		]);

	/*
	*
	*
	*	TYPES
	*
	*/
	Route::group(['prefix' => 'types', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'TypeController@index',
			'as' => 'admin.type.show',
		]);

		Route::get('add', [
			'uses' => 'TypeController@create',
			'as' => 'admin.type.add',
		]);

		Route::get('edit/{id}', [
			'uses' => 'TypeController@edit',
			'as' => 'admin.type.edit',
		])->where('id', '[0-9]+');

		Route::post('add', [
			'uses' => 'TypeController@store',
			'as' => 'admin.type.addDB',
		]);

		Route::post('edit/{id}', [
			'uses' => 'TypeController@update',
			'as' => 'admin.type.editDB',
		])->where('id', '[0-9]+');

		Route::get('delete/{id}', [
			'uses' => 'TypeController@destroy',
			'as' => 'admin.type.delete',
		])->where('id', '[0-9]+');

	});


	/*
	*
	*
	*	CITY DISTRICT
	*
	*/
	Route::group(['prefix' => 'city-district', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'CityDistrictController@index',
			'as' => 'admin.city-district.show',
		]);

		Route::get('add', [
			'uses' => 'CityDistrictController@create',
			'as' => 'admin.city-district.add',
		]);

		Route::post('add', [
			'uses' => 'CityDistrictController@store',
			'as' => 'admin.city-district.store',
			]);

		Route::post('add/district', [
			'uses' => 'CityDistrictController@storeDistrict',
			'as' => 'admin.city-district.storeDistrict',
			]);

		Route::get('edit-city/{id}', [
			'uses' => 'CityDistrictController@edit',
			'as' => 'admin.city-district.city.edit',
		])->where('id', '[0-9]+');

		Route::get('edit-district/{id}', [
			'uses' => 'CityDistrictController@editDistrict',
			'as' => 'admin.city-district.district.edit',
		])->where('id', '[0-9]+');

		Route::post('edit-city/{id}', [
			'uses' => 'CityDistrictController@update',
			'as' => 'admin.city-district.city.update',
		])->where('id', '[0-9]+');

		Route::post('edit-district/{id}', [
			'uses' => 'CityDistrictController@updateDistrict',
			'as' => 'admin.city-district.district.update',
		])->where('id', '[0-9]+');

		Route::get('delete-city/{id}', [
			'uses' => 'CityDistrictController@destroy',
			'as' => 'admin.city-district.city.delete',
		])->where('id', '[0-9]+');

		Route::get('delete-district/{id}', [
			'uses' => 'CityDistrictController@destroyDistrict',
			'as' => 'admin.city-district.district.delete',
		])->where('id', '[0-9]+');

	});	


	/*
	*
	*
	*	ATTRIBUTES
	*
	*/
	Route::group(['prefix' => 'attributes', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'AttributesController@index',
			'as' => 'admin.attributes.show',
		]);

		Route::get('add', [
			'uses' => 'AttributesController@create',
			'as' => 'admin.attributes.add',
		]);

		Route::get('edit/{id}', [
			'uses' => 'AttributesController@edit',
			'as' => 'admin.attributes.edit',
		])->where('id', '[0-9]+');

		Route::post('add', [
			'uses' => 'AttributesController@store',
			'as' => 'admin.attributes.addDB',
		]);

		Route::post('edit/{id}', [
			'uses' => 'AttributesController@update',
			'as' => 'admin.attributes.editDB',
		])->where('id', '[0-9]+');

		Route::get('delete/{id}', [
			'uses' => 'AttributesController@destroy',
			'as' => 'admin.attributes.delete',
		])->where('id', '[0-9]+');

	});	

	/*
	*
	*
	*	CONDITIONS
	*
	*/
	Route::group(['prefix' => 'conditions', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'ConditionController@index',
			'as' => 'admin.condition.show',
		]);

		Route::get('add', [
			'uses' => 'ConditionController@create',
			'as' => 'admin.condition.add',
		]);

		Route::get('edit/{id}', [
			'uses' => 'ConditionController@edit',
			'as' => 'admin.condition.edit',
		])->where('id', '[0-9]+');

		Route::post('add', [
			'uses' => 'ConditionController@store',
			'as' => 'admin.condition.addDB',
		]);

		Route::post('edit/{id}', [
			'uses' => 'ConditionController@update',
			'as' => 'admin.condition.editDB',
		])->where('id', '[0-9]+');

		Route::get('delete/{id}', [
			'uses' => 'ConditionController@destroy',
			'as' => 'admin.condition.delete',
		])->where('id', '[0-9]+');

	});

	/*
	*
	*
	*	USERS
	*
	*/
	Route::group(['prefix' => 'users', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'UserController@index',
			'as' => 'admin.users.show',
		]);

		Route::get('detailed/{id}', [
			'uses' => 'UserController@showDetailed',
			'as' => 'admin.users.detailed.show',
		]);

		Route::get('add', [
			'uses' => 'UserController@create',
			'as' => 'admin.users.add',
		]);

		Route::get('edit/{id}', [
			'uses' => 'UserController@edit',
			'as' => 'admin.users.edit',
		])->where('id', '[0-9]+');

		Route::post('add', [
			'uses' => 'UserController@store',
			'as' => 'admin.users.addDB',
		]);

		Route::post('edit/{id}', [
			'uses' => 'UserController@update',
			'as' => 'admin.users.editDB',
		])->where('id', '[0-9]+');

		Route::get('block/{id}', [
			'uses' => 'UserController@block',
			'as' => 'admin.users.block',
		])->where('id', '[0-9]+');

		Route::get('unblock/{id}', [
			'uses' => 'UserController@unblock',
			'as' => 'admin.users.unblock',
		])->where('id', '[0-9]+');

		Route::get('delete/{id}', [
			'uses' => 'UserController@destroy',
			'as' => 'admin.users.delete',
		])->where('id', '[0-9]+');

	});

	/*
	*
	*
	*	INCORPORATED USERS
	*
	*/
	Route::group(['prefix' => 'inc-users', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'IncUserController@index',
			'as' => 'admin.inc-users.show',
		]);

		Route::get('detailed/{id}', [
			'uses' => 'IncUserController@showDetailed',
			'as' => 'admin.inc-users.detailed.show',
		]);

		Route::get('add', [
			'uses' => 'IncUserController@create',
			'as' => 'admin.inc-users.add',
		]);

		Route::get('edit/{id}', [
			'uses' => 'IncUserController@edit',
			'as' => 'admin.inc-users.edit',
		])->where('id', '[0-9]+');

		Route::post('add', [
			'uses' => 'IncUserController@store',
			'as' => 'admin.inc-users.addDB',
		]);

		Route::post('edit/{id}', [
			'uses' => 'IncUserController@update',
			'as' => 'admin.inc-users.editDB',
		])->where('id', '[0-9]+');

		Route::get('block/{id}', [
			'uses' => 'IncUserController@block',
			'as' => 'admin.inc-users.block',
		])->where('id', '[0-9]+');

		Route::get('unblock/{id}', [
			'uses' => 'IncUserController@unblock',
			'as' => 'admin.inc-users.unblock',
		])->where('id', '[0-9]+');

		Route::get('delete/{id}', [
			'uses' => 'IncUserController@destroy',
			'as' => 'admin.inc-users.delete',
		])->where('id', '[0-9]+');

	});

	/*
	*
	*
	*	APPLICATIONS
	*
	*/
	Route::group(['prefix' => 'applications', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'ApplicationController@index',
			'as' => 'admin.applications.show',
		]);

		Route::get('add', [
			'uses' => 'ApplicationController@create',
			'as' => 'admin.applications.add',
		]);

		Route::post('add', [
			'uses' => 'ApplicationController@store',
			'as' => 'admin.applications.addDB',
		]);

		Route::post('addDefaultImage', [
			'uses' => 'ApplicationController@addDefaultImage',
			'as' => 'admin.applications.default.image.add',
		]);

		Route::post('upload', [
			'uses' => 'ApplicationController@uploadImage',
			'as' => 'admin.applications.image.upload',
		]);

		Route::get('edit/{id}', [
			'uses' => 'ApplicationController@edit',
			'as' => 'admin.applications.edit',
		])->where('id', '[0-9]+');

		Route::post('edit/{id}', [
			'uses' => 'ApplicationController@update',
			'as' => 'admin.applications.update',
		])->where('id', '[0-9]+');

		Route::post('delete/image', [
			'uses' => 'ApplicationController@deleteImage',
			'as' => 'admin.applications.image.delete',
		]);

		Route::get('delete/imageFromEdit/{id}', [
			'uses' => 'ApplicationController@deleteImageFromEdit',
			'as' => 'admin.applications.image.from.edit.delete',
		])->where('id', '[0-9]+');

		Route::get('delete/{id}', [
			'uses' => 'ApplicationController@destroy',
			'as' => 'admin.applications.delete',
		])->where('id', '[0-9]+');

	});

	/*
	 *
	 * FOOTER LINKS
	 *
	*/
	Route::group(['prefix' => 'footer-links', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'FooterLinksController@index',
			'as' => 'admin.footer-links.show',
		]);

		Route::get('add', [
			'uses' => 'FooterLinksController@create',
			'as' => 'admin.footer-links.add',
		]);

		Route::get('edit/{id}', [
			'uses' => 'FooterLinksController@edit',
			'as' => 'admin.footer-links.edit',
		])->where('id', '[0-9]+');

		Route::post('add', [
			'uses' => 'FooterLinksController@store',
			'as' => 'admin.footer-links.addDB',
		]);

		Route::post('edit/{id}', [
			'uses' => 'FooterLinksController@update',
			'as' => 'admin.footer-links.editDB',
		])->where('id', '[0-9]+');

		Route::get('delete/{id}', [
			'uses' => 'FooterLinksController@destroy',
			'as' => 'admin.footer-links.delete',
		])->where('id', '[0-9]+');

	});

	/*
	*
	*
	*	PAYMENTS
	*
	*/
	Route::group(['prefix' => 'payments', 'middleware' => 'auth'], function() {

		Route::get('/', [
			'uses' => 'PaymentController@index',
			'as' => 'admin.payments.show',
		]);

	});

});

Route::get($locale.'/{slug}', [
	'uses' => 'SiteController@getFooterLink',
	'as'   => 'get.footer.link'
]);


Route::get('sitemap.xml', 'HomeController@sitemap');
