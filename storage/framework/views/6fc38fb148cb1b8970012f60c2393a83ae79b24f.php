<?php $__env->startSection('content'); ?>
    <div class="col-md-10 main-content-admin">
        <ol class="breadcrumb">
          <li><a href="<?php echo e(route('admin.users.show')); ?>">მომხმარებლები</a></li>
          <li class="active">
	          <?php if(Request::segment(3) == 'add'): ?>
	          	დამატება
	          <?php else: ?>
	          	რედაქტირება
	          <?php endif; ?>
          </li>
        </ol>
        <div class="row">
            <div class="col-md-6">
                <?php if(!empty($obj)): ?>
                    <?php echo Form::model($obj,['method' => 'post']); ?>

                <?php else: ?>
                    <?php echo Form::open(['method' => 'post']); ?>

                <?php endif; ?>
                <div class="row"> 
                  <div class="col-md-6 form-group">
                      <label for="first-name">სახელი</label>
                      <?php echo Form::text('first_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'first-name',
                                'placeholder'=>'სახელი')); ?>

                  </div>
                  <div class="col-md-6 form-group">
                      <label for="last-name">გვარი</label>
                      <?php echo Form::text('last_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'last-name',
                                'placeholder'=>'გვარი')); ?>

                  </div>
                </div>
                <div class="row user-email-parent">  
                  <div class="col-md-6 form-group user-email">
                      <label for="email">ელ. ფოსტა</label>
                      <?php echo Form::text('email', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'email',
                                'placeholder'=>'ელ. ფოსტა')); ?>

                  </div>
                  <?php if(empty($obj)): ?>
                    <div class="col-md-6 form-group">
                        <label for="password">პაროლი</label>
                        <?php echo Form::password('password',
                            array('required',
                                  'class'=>'form-control',
                                   'id' => 'password',
                                  'placeholder'=>'მინ. 8 სიმბოლო')); ?>

                    </div>
                  <?php else: ?>
                    <div class="col-md-6 form-group">
                      <label for="password">პაროლი</label>
                      <?php echo Form::password('password',
                          array('disabled',
                                'class'=>'form-control',
                                 'id' => 'password',
                                'placeholder'=>'მინ. 8 სიმბოლო')); ?>

                    </div>
                  <?php endif; ?>
                </div>
                <div class="row">
                  <div class="col-md-6 form-group">
                      <label for="phone">ტელეფონი</label>
                      <?php echo Form::text('phone', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'phone',
                                'placeholder'=>'xxx xx xx xx')); ?>

                  </div>
                  <div class="col-md-6 form-group">
                      <label for="timepicker1">დაბადების თარიღი</label>
                      <div class="input-group bootstrap-timepicker timepicker">
                          <?php echo Form::text('birth_date',null,[
                              'required',
                              'class'=>'form-control input-small',
                              'id' => 'timepicker1',
                              'placeholder'=>'წელი - თვე - რიცხვი (1993-08-05)'
                          ]); ?>

                          <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                  </div>
                </div>  
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <button type="submit" class="btn btn-primary">შენახვა</button>
                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
    <script type="text/javascript">
    $('#timepicker1').datepicker({
          format: "yyyy-mm-dd"
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>