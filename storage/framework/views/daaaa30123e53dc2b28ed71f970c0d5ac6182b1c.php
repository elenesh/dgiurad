<footer>
	<div class="bc"></div>
	<div class="footer-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<?php for($i = 0; $i < 6; $i++): ?>
						<a href="<?php echo e(Request::root()); ?>/<?php echo e(\App::getLocale()); ?>/<?php echo e($footerLinks[$i]->slug); ?>"><?php echo e(L10nHelper::get($footerLinks[$i])); ?></a>
					<?php endfor; ?>
				</div>
				<div class="col-md-3">
					<?php for($i = 6; $i < 12; $i++): ?>
						<a href="<?php echo e(Request::root()); ?>/<?php echo e(\App::getLocale()); ?>/<?php echo e($footerLinks[$i]->slug); ?>"><?php echo e(L10nHelper::get($footerLinks[$i])); ?></a>
					<?php endfor; ?>
				</div>
				<div class="col-md-3">
					<?php for($i = 12; $i < 18; $i++): ?>
						<a href="<?php echo e(Request::root()); ?>/<?php echo e(\App::getLocale()); ?>/<?php echo e($footerLinks[$i]->slug); ?>"><?php echo e(L10nHelper::get($footerLinks[$i])); ?></a>
					<?php endfor; ?>
				</div>
				<div class="col-md-3">
					<?php for($i = 18; $i < 24; $i++): ?>
						<a href="<?php echo e(Request::root()); ?>/<?php echo e(\App::getLocale()); ?>/<?php echo e($footerLinks[$i]->slug); ?>"><?php echo e(L10nHelper::get($footerLinks[$i])); ?></a>
					<?php endfor; ?>
				</div>
				<div class="col-md-12">
				  <div class="footer-single-text">
					<h1>კერძო სახლები, აპარტამენტები, ბინები დღიურად მთელი საქართველოს მასშტაბით. </h1>
					<hr>
					<p>ვებგვერდზე განთავსებულია ბინები, რომელთა ქირაობა შესაძლებელია დღიურად და საათობრივად. ჩვენი მიზანია მომხმარებელს შევუქმნათ ყველა პირობა, რათა სწრაფად და მარტივად მოძებნონ მათთვის სასურველი ბინა.  </p>
					<p>უძრავი ქონების სააგენტოებს ვთავაზობთ ჩვენი მომსახურებით სარგებლობას უფასოდ. სტანდარტული სერვისის გარდა შესაძლებელია VIP და სუპერ VIP განცხადებების განთავსება. განცხადებები იდება სრული აღწერით, შესაძლებელია ადგილის მონიშვნა რუკაზე. </p>
					<p>ვებგვერდის დახმარებით მარტივად იპოვნით ბინას დღიურად ან საათობრივად საქირაოდ, თქვენთვის მისაღებ ადგილას მთელი საქართველოს მასშტაბით! მოძებნეთ სასურველი ბინა დღიურად და დაზოგეთ თანხა!</p>
				</div>
				</div>

			</div>
		</div>
	</div>

	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=152100168475769";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<?php if(\App::getLocale() == 'ka'): ?>

	<div class="footer-ex-menu">
		<div class="container">
			<div class="col-md-8 footer-links" style="position: relative; top:22px; ">
				<a href="<?php echo e(route('contact')); ?>" style="color: white; font-size: 15px; float: left; padding-left: 21px;">კონტაქტი</a>
				<a href="<?php echo e(route('faq')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">ხშირად დასმული კითხვები</a>
				<a href="<?php echo e(route('privacy')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">კონფიდენციალურობა</a>
				<a href="<?php echo e(route('instructions')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">ინსტრუქციები</a>
				<div class="social">
		
					<div class="fb-like" data-href="https://www.facebook.com/dgiurad.ge/" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
				
					<a href="//plus.google.com/+DgiuradGebinebi/posts"
					   rel="publisher" target="_top" style="text-decoration:none;">
					<img src="//ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
					</a>
				
		        </div>		
			</div>
			<div class="col-md-3">
				<a class="created" target="_blank" href="http://advertwise.ge/"></a>
			</div>
		</div>
		
	</div>
</footer>

<?php elseif(\App::getLocale() == 'en'): ?>

<div class="footer-ex-menu">
		<div class="container">
			<div class="col-md-8 footer-links" style="position: relative; top:22px; ">
				<a href="<?php echo e(route('contact')); ?>" style="color: white; font-size: 15px; float: left; padding-left: 21px;">Contact</a>
				<a href="<?php echo e(route('faq')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">FAQ</a>
				<a href="<?php echo e(route('privacy')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">Privacy Policy</a>
				<a href="<?php echo e(route('instructions')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">Instructions</a>
			</div>
			<div class="col-md-4">
				<a class="created" target="_blank" href="http://advertwise.ge/"></a>
			</div>
		</div>
	</div>

	</footer>

	<?php elseif(\App::getLocale() == 'ru'): ?>


	<div class="footer-ex-menu">
		<div class="container">
			<div class="col-md-8 footer-links" style="position: relative; top:22px; ">
				<a href="<?php echo e(route('contact')); ?>" style="color: white; font-size: 15px; float: left; padding-left: 21px;">Контакт</a>
				<a href="<?php echo e(route('faq')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">Часто задаваемые вопросы</a>
				<a href="<?php echo e(route('privacy')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">Конфиденциальность</a>
				<a href="<?php echo e(route('instructions')); ?>" style="color: white; font-size: 15px; float: left; padding-left:21px;">Инструкции</a>
			</div>
			<div class="col-md-4">
				<a class="created" target="_blank" href="http://advertwise.ge/"></a>
			</div>
		</div>
	</div>

	</footer>


	<?php endif; ?>
