<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<section class="main">
		<div class="container reg-login">
            <div class="col-md-6 cols-ref">
            	<h2><?php echo e(trans('main.profile-edit')); ?></h2>
                <?php echo Form::model($obj,['method' => 'post', 'id' => 'user-info-form']); ?>

                <div class="row"> 
                  <div class="col-md-6 form-group">
                      <label for="first-name"><?php echo e(trans('main.first-name')); ?></label>
                      <?php echo Form::text('first_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'first-name',
                                'placeholder'=> trans('main.first-name'))); ?>

                  </div>
                  <div class="col-md-6 form-group">
                      <label for="last-name"><?php echo e(trans('main.last-name')); ?></label>
                      <?php echo Form::text('last_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'last-name',
                                'placeholder'=> trans('main.last-name'))); ?>

                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 form-group">
                      <label for="phone"><?php echo e(trans('main.phone')); ?></label>
                      <?php echo Form::text('phone', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'phone',
                                'placeholder'=>'xxx xx xx xx')); ?>

                  </div>
                  <?php if($obj->type == 0): ?>
	                  <div class="col-md-6 form-group">
	                      <label for="timepicker1"><?php echo e(trans('main.birth-date')); ?></label>

                        <div id="birth_date" class="input-group date-picker">
                            <input value="<?php echo e(\Helpers::getDateForView($obj->birth_date)); ?>" data-validation="empty-value" type="text" name="birth_date" data-format="L" data-locale="ka" class="form-control" placeholder="თარიღი" autocomplete="off" required>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i> </span>
                        </div>

	                      
	                  </div>
                  <?php else: ?>
                  <div class="clearfix"></div>
					<div class="col-md-6 form-group">
                      <label for="incorporated-id"><?php echo e(trans('main.inc-id')); ?></label>
                      <?php echo Form::text('incorporated_id', null,
                          ['required',
                            'class'=>'form-control',
                            'id' => 'incorporated-id',
                            'placeholder'=>'xxx xxx xxx']); ?>

                  	</div>
                  	<div class="col-md-12 form-group">
                      <label for="incorporated-name"><?php echo e(trans('main.inc-name')); ?></label>
                      <?php echo Form::text('incorporated_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'incorporated-name',
                                'placeholder'=> trans('main.inc-name'))); ?>

                  	</div>
                  <?php endif; ?>
                </div>  
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <button type="submit" class="pull-right btn btn-success"><?php echo e(trans('main.save')); ?></button>
                <?php echo Form::close(); ?>

            </div>
            <div class="col-md-6">
            	<h2><?php echo e(trans('main.change-pass')); ?></h2>
            	<?php echo Form::open(['method' => 'post', 'id' => 'pass-resset-form']); ?>


            	<div class="col-md-12">
	            	<div class="form-group">
		    			<label for=""><?php echo e(trans('main.old-pass')); ?></label>
		    			<input type="password" name="old_password" class="form-control" placeholder="<?php echo e(trans('main.old-pass')); ?>" required>
		    		</div>

		    		<div class="form-group">
		    			<label for=""><?php echo e(trans('main.new-pass')); ?></label>
		    			<input type="password" name="password" class="form-control" placeholder="<?php echo e(trans('main.new-pass')); ?>" required>
		    		</div>

		    		<div class="form-group">
		    			<label for=""><?php echo e(trans('main.conf-new-pass')); ?></label>
		    			<input type="password" name="password_confirmation" class="form-control" placeholder="<?php echo e(trans('main.conf-new-pass')); ?>" required>
		    		</div>
            	</div>

            	<div class="col-md-6 pull-right">
            		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            		<button type="submit" class="pull-right btn btn-success"><?php echo e(trans('main.save')); ?></button>
            	</div>
            	
            	<?php echo Form::close(); ?>

            </div>
        </div>
    </div>	
		</div>
	</section>

  <!-- Modal -->
  <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modal1Label"></h4>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
        </div>
      </div>
    </div>
  </div>

  <script src="http://dgiurad.ge/assets/site/js/date-time.min.js" type="text/javascript"></script>
  <script src="http://dgiurad.ge/assets/site/js/validation.min.js" type="text/javascript"></script>

  <script type="text/javascript">
  $(function() {
    $('#user-info-form').validator();
    $('#pass-resset-form').validator().on('submit', function (e) { 
      if (e.isDefaultPrevented()) {
        // handle the invalid form...
      } else {
        // everything looks good!
        e.preventDefault();

        formData = $(this).serialize();
        e.preventDefault();
        $.ajax({
          url: "<?php echo e(route('password.reset')); ?>",
          type: 'POST',
          data: formData,
          success: function(data){
            switch(data['status']) {
              case 0:
                popupModal(data);
                break;
              case 1:
                window.location.href = "<?php echo e(route('user.profile.edit')); ?>";
                break;
            }
            console.log(data);
          }
        })
        //popupModal(status_msg);
      }
    })
  });
  </script>

	<?php echo $__env->make('helpers.delete-popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>