<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 status">
            <h1 style="color:green; text-align: center;">თანხა წარმატებით ჩაირიცხა</h1>
        </div>
        <div class="col-md-4"></div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>