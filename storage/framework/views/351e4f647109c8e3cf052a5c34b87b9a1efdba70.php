<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<section class="main">
		<div class="container reg-login">
            <div class="col-md-6" style="border-right: 1px dashed #A5A5A5;padding-right: 30px;">
            	<h2>პირადი ინფორმაციის ცვლილება</h2>
                <?php echo Form::model($obj,['method' => 'post']); ?>

                <div class="row"> 
                  <div class="col-md-6 form-group">
                      <label for="first-name">სახელი</label>
                      <?php echo Form::text('first_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'first-name',
                                'placeholder'=>'სახელი')); ?>

                  </div>
                  <div class="col-md-6 form-group">
                      <label for="last-name">გვარი</label>
                      <?php echo Form::text('last_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'last-name',
                                'placeholder'=>'გვარი')); ?>

                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 form-group">
                      <label for="phone">ტელეფონი</label>
                      <?php echo Form::text('phone', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'phone',
                                'placeholder'=>'xxx xx xx xx')); ?>

                  </div>
                  <?php if($obj->type == 0): ?>
	                  <div class="col-md-6 form-group">
	                      <label for="timepicker1">დაბადების თარიღი</label>
	                      <div class="input-group bootstrap-timepicker timepicker">
	                          <?php echo Form::text('birth_date',null,[
	                              'required',
	                              'class'=>'form-control input-small',
	                              'id' => 'timepicker1',
	                              'placeholder'=>'წელი - თვე - რიცხვი (1993-08-05)'
	                          ]); ?>

	                          <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
	                      </div>
	                  </div>
                  <?php else: ?>
					<div class="col-md-6 form-group">
                      <label for="incorporated-id">საიდენტიფიკაციო ნომერი</label>
                      <?php echo Form::text('incorporated_id', null,
                          ['required',
                            'class'=>'form-control',
                            'id' => 'incorporated-id',
                            'placeholder'=>'9 ნიშნა კოდი (xxxxxxxxx)']); ?>

                  	</div>
                  	<div class="col-md-12 form-group">
                      <label for="incorporated-name">კომპანიის სახელი</label>
                      <?php echo Form::text('incorporated_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'incorporated-name',
                                'placeholder'=>'კომპანიის სახელი')); ?>

                  	</div>
                  <?php endif; ?>
                </div>  
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <button type="submit" class="pull-right btn btn-success">შენახვა</button>
                <?php echo Form::close(); ?>

            </div>
            <div class="col-md-6">
            	<h2>პაროლის ცვლილება</h2>
            	<?php echo Form::open(['method' => 'post', 'url' => '/asd/']); ?>


            	<div class="col-md-12">
	            	<div class="form-group">
		    			<label for="">ძველი პაროლი</label>
		    			<input type="password" name="password" class="form-control" placeholder="პაროლი" required>
		    		</div>

		    		<div class="form-group">
		    			<label for="">ახალი პაროლი</label>
		    			<input type="password" name="password" class="form-control" placeholder="პაროლი" required>
		    		</div>

		    		<div class="form-group">
		    			<label for="">გაიმეორეთ ახალი პაროლი</label>
		    			<input type="password" name="password" class="form-control" placeholder="პაროლი" required>
		    		</div>
            	</div>

            	<div class="col-md-6 pull-right">
            		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            		<button type="submit" class="pull-right btn btn-success">შენახვა</button>
            	</div>
				
            	
            	<?php echo Form::close(); ?>

            </div>
        </div>
    </div>	
		</div>
	</section>
	<?php echo $__env->make('helpers.delete-popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>