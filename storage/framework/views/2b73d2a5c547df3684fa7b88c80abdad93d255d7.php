<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="row" style="margin-top:30px;font-family: 'Studio-02' !important;">
        <div class="col-md-4"></div>
        <div class="col-md-4 input-group">
            <label for="payAmount">შეიყვანეთ თანხა:</label>
            <div class="input-group">
              <input type="number" class="form-control" id="payAmount" placeholder="2.0"/>
              <span class="input-group-btn">
                  <button class="btn btn-success" type="button" onclick="pay()">გადახდა</button>
              </span>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <?php
        $auth = auth()->guard('siteUsers');
        $user = $auth->user();
    ?>
    <script type="text/javascript">

        function pay() {
            var amount = parseFloat($('#payAmount').val());
            var TransactionPaymentModel = {
                TransactionItems: [{
                    AbonentCode: '<?php echo e($user->unique_id); ?>',
                    Amount: amount
                }],
                ServiceCode: 'DGIURAD',
                Language: 'en',
                ReturnUrl: 'http://dgiurad.ge/<?php echo e(Request::segment(1)); ?>/user-area/payment-success'
            };

            if (amount >= 0.5 && amount <= 1500)
                $.post('https://new.mypay.ge:1919/api/gw', TransactionPaymentModel).done(function (data) {
                    var myData = data.split("trans_id=");
                    document.cookie = "trans-id=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                    document.cookie = "trans-id=" + myData[1];
                    var re = savePayment(myData[1]);
                    if(re == 1){
                        window.location.replace(data);
                    }
                });
            else alert('გთხოვთ თახა შეიყვანოთ 0.5 დან 1500-ის შუალედში');
        }
        function savePayment(transID){
            var token = '<?php echo e(csrf_token()); ?>';
            var resp = null;
            $.ajax({
                url: "<?php echo e(route('pre.payment.save')); ?>",
                type:"POST",
                async: false,
                data: { '_token': token, 'tid': transID, 'amount': parseFloat($('#payAmount').val())},
                success:function(data){
                    if(data == 1){
                        resp = 1;
                    }else{
                        resp = 0;
                    }
                },error:function(){
                    resp = 0;
                }
            });
            return resp;
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>