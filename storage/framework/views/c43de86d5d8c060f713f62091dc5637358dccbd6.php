<?php if(!empty($_GET['status'])): ?>
<?php if($_GET['status'] == 1): ?>
<div class="alert alert-danger alert-dismissible status-text" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	ასეთი <strong>ელ. ფოსტა</strong> უკვე არსებობს
</div>
<?php elseif($_GET['status'] == 2): ?>
<div class="alert alert-danger alert-dismissible status-text" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	ასეთი <strong>ტელეფონის ნომერი</strong> უკვე არსებობს
</div>
<?php elseif($_GET['status'] == 3): ?>
<div class="alert alert-danger alert-dismissible status-text" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	ასეთი <strong>საიდენტიფიკაციო ნომერი</strong> უკვე არსებობს
</div>
<?php elseif($_GET['status'] == 4): ?>
<div class="alert alert-success alert-dismissible status-text" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	წარმატებით <strong>დაემატა</strong>: <?php echo e($_GET['name']); ?>

</div>
<?php elseif($_GET['status'] == 5): ?>
<div class="alert alert-success alert-dismissible status-text" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	წარმატებით <strong>წაიშალა</strong>: <?php echo e($_GET['name']); ?>

</div>
<?php elseif($_GET['status'] == 6): ?>
<div class="alert alert-success alert-dismissible status-text" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	წარმატებით <strong>დარედაქტირდა</strong>
</div>
<?php elseif($_GET['status'] == 7): ?>
<div class="alert alert-success alert-dismissible status-text" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	წარმატებით <strong>დაიბლოკა</strong>: <?php echo e($_GET['name']); ?>

</div>
<?php elseif($_GET['status'] == 8): ?>
<div class="alert alert-success alert-dismissible status-text" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	წარმატებით <strong>განიბლოკა</strong>: <?php echo e($_GET['name']); ?>

</div>
<?php endif; ?>
<?php endif; ?>