<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<?php //print_r($application) ?>


	<div class="container single-page">
		<div class="row">
			<div class="home-slider">
				<ul id="vertical">
					<li data-thumb="/uploads/applications/<?php echo e($application->id); ?>/2_<?php echo e($application->defaultImage->src); ?>">
					    <img src="/uploads/applications/<?php echo e($application->id); ?>/1_<?php echo e($application->defaultImage->src); ?>" />
				  	</li>
				  <?php foreach($application->images as $a): ?>
					<?php if($a->id != $application->default_image_id): ?>
					  	<li data-thumb="/uploads/applications/<?php echo e($application->id); ?>/2_<?php echo e($a->src); ?>">
					  	  <img src="/uploads/applications/<?php echo e($application->id); ?>/1_<?php echo e($a->src); ?>" />
					  	</li>
					<?php endif; ?>
				  <?php endforeach; ?>

				</ul>
				<ul class="meta-info">
					<li>ID <?php echo e($application->unique_id); ?></li>
					<li><i class="fa fa-eye"></i> <?php echo e($application->views); ?></li>
					<li><i class="fa fa-calendar"></i> <?php echo e($application->date_created); ?></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			

			<div class="application-info">
				<div class="col-md-8">

					<section>
						<img width="52" class="user-avatar" src="/assets/site/images/avatars/default-avatar.svg">
						<h1>
							<?php echo e(L10nHelper::get($application)); ?>

							<span><?php echo e(L10nHelper::get($application, 'street_addr')); ?><?php echo e(isset($application->district->title_geo) ? ", ".L10nHelper::get($application->district) : ''); ?>, <?php echo e(L10nHelper::get($application->city)); ?> </span>
						</h1>
					</section>
					<div class="clearfix"></div>

					
					<div class="price-table mob2">
						<?php if($application->daily_price != 0): ?>
							<div class="price daily"><?php echo Helpers::getPrice($application, 1); ?></div>
						<?php endif; ?>
						<?php if($application->hourly_price != 0): ?>
							<div class="price hourly"><?php echo Helpers::getPrice($application, 2); ?></div>
						<?php endif; ?>
						<br>
					</div>


					

					<fieldset class="well the-fieldset">
					    <legend class="the-legend"><h3><?php echo e(trans('main.about-appl')); ?></h3></legend>
					    <p class="col-md-12">
					    	<?php echo e(L10nHelper::get($application, 'description')); ?>

					    </p>
					</fieldset>



					<?php 
						(int)$count = count($attributes);
						if((int)($count%2) == 0){
							$firstC = (int)($count/2);
							$secondC = (int)($count);
						}else{
							$firstC = (int)($count/2) + 1;
							$secondC = (int)($count);
						}

						function getAtrributeClass($application, $attr){
							$class = 'passive';
							foreach($application->attributes as $a){
								if($a->id == $attr->id){
									$class = 'active';
								}
							}
							return $class;
						}
						
					?>
					<fieldset class="well the-fieldset space">
				        <legend class="the-legend"><h3><?php echo e(trans('main.area')); ?></h3></legend>
						<div class="col-md-6">
							<table >
							  <tr>
								<td><?php echo e(trans('main.real-estate-type')); ?>: </td>
								<td><b><?php echo e(L10nHelper::get($application->type)); ?></b></td>
							  </tr>
							  <tr>
								<td><?php echo e(trans('main.condition')); ?>: </td>
								<td><b><?php echo e(L10nHelper::get($application->condition)); ?></b></td>
							  </tr>
							  <tr>
								<td><?php echo e(trans('main.area')); ?>: </td>
								<td><b><?php echo e((int)$application->m2); ?> <?php echo e(trans('search.m2')); ?></b></td>
							  </tr>
							</table>
						</div>
						<div class="col-md-6">
							<table>
								<tr>
									<td><?php echo e(trans('main.floor')); ?>: </td>
									<td><b><?php echo e($application->floor); ?></b></td>
								</tr>
								<tr>
									<td><?php echo e(trans('main.room-quantity')); ?>: </td>
									<td><b><?php echo e($application->room_quantity); ?></b></td>
								</tr>
								<tr>
									<td><?php echo e(trans('main.bedroom')); ?>: </td>
									<td><b><?php echo e($application->bedroom_quantity); ?></b></td>
								</tr>
								<tr>
									<td><?php echo e(trans('main.bathroom')); ?>: </td>
									<td><b><?php echo e($application->bathroom_quantity); ?></b></td>
								</tr>
							</table>
						</div>
					</fieldset>

						<fieldset class="well the-fieldset">
					        <legend class="the-legend"><h3><?php echo e(trans('main.comfort')); ?></h3></legend>
					        <ul class="col-md-6">
					        	<?php for($i = 0; $i < $firstC; $i++ ): ?>
					        		<li class="<?php echo e(getAtrributeClass($application, $attributes[$i])); ?>"><i class="fa"></i> <?php echo e(L10nHelper::get($attributes[$i])); ?></li>
					        	<?php endfor; ?>
					        </ul>
					        <ul class="col-md-6">
					        	<?php for($i = $firstC; $i < $secondC; $i++ ): ?>
					        		<li class="<?php echo e(getAtrributeClass($application, $attributes[$i])); ?>"><i class="fa"></i> <?php echo e(L10nHelper::get($attributes[$i])); ?></li>
					        	<?php endfor; ?>
					        </ul>
					    </fieldset>


					<div class="clearfix"></div>

				</div>

				<div class="col-md-4">
					<div class="price-table desc2">
						<?php if($application->daily_price != 0): ?>
							<div class="price daily"><?php echo Helpers::getPrice($application, 1); ?></div>
						<?php endif; ?>
						<?php if($application->hourly_price != 0): ?>
							<div class="price hourly"><?php echo Helpers::getPrice($application, 2); ?></div>
						<?php endif; ?>
					</div>
					<div class="contact-table">
						<h5><?php echo e(trans('main.contact')); ?></h5>
						<ul>
							<li><i class="fa fa-user"></i>&nbsp <?php echo e($application->user->first_name); ?></li>
							<li><i class="fa fa-phone"></i>&nbsp <?php echo e($application->phone); ?></li>
						</ul>
					</div>
					<div style="width:100%; height:400px; margin-top: 8px" id="map"></div>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="similar-apps">
				<div class="col-md-12 app-rank-cont">
					<h5 class="app-rank-header"><?php echo e(trans('main.related-apps')); ?></h5>
				</div>

				<ul id="similar-apps" style="padding-top: 20px; " class="block-items style-1">
					<?php foreach($related_apps as $r): ?>
                    <li>
                        <a href="<?php echo e(route('single', [$r->slug, $r->unique_id])); ?>" class=" item <?php echo e($r->app_type); ?>">
								<div class="preview-img">
									<img alt="house-4" src="/uploads/applications/<?php echo e($r->id); ?>/2_<?php echo e($r->defaultImage->src); ?>">
									<div class="caption">
										<?php echo Helpers::getPrice($r); ?>

									</div>
								</div>
								<div class="info-block">
									<h4>
										<?php echo e(L10nHelper::get($r->city)); ?><?php echo e(isset($r->district->title_geo) ? ", ".L10nHelper::get($r->district) : ''); ?>

									</h4>
								</div>
							</a>
                    </li>
					<?php endforeach; ?>	    
				</ul>
			</div>


			
		</div>
	</div>

	<script src="http://dgiurad.ge/assets/site/js/lightslider.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			if($(document).width() < 1200){
				$('#vertical').lightSlider({
				  	gallery:true,
				  	item:1,
				  	//vertical:true,
				 	//verticalWidth:920,
				  	verticalHeight:474,
				  	vThumbWidth:220,
				  	//vThumbHeight:226,
				  	thumbItem:4,
				  	thumbMargin:4,
				  	slideMargin:0,

				  	autoWidth: false,
				  	responsive : [],
				}); 
			}
			else{
				$('#vertical').lightSlider({
				  	gallery:true,
				  	item:1,
				  	vertical:true,
				 	//verticalWidth:920,
				  	verticalHeight:474,
				  	vThumbWidth:220,
				  	//vThumbHeight:226,
				  	thumbItem:4,
				  	
				  	slideMargin:0,

				  	autoWidth: false,
				  	responsive : [],
				});
			}

			$('#similar-apps').lightSlider({
		        item:4,
		        loop:false,
		        slideMove:2,
		        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
		        speed:600,
		        slideWidth:400,
		        slideMargin:15,
		        responsive : [
		            {
		                breakpoint:1200,
		                settings: {
		                    item:3,
		                    slideMove:1,
		                  }
		            },
		            {
		                breakpoint:992,
		                settings: {
		                    item:2,
		                    slideMove:1
		                  }
		            },
		            {
		                breakpoint:769,
		                settings: {
		                    item:1,
		                    slideMove:1
		                  }
		            }
		        ]
			}); 
		});
	</script>

	<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
	<script type="text/javascript">

	var Appdata = <?php echo json_encode($application); ?>

	if(Appdata['latitude'] != 41.70451 && Appdata['longitude'] != 44.80912){
		var map = new google.maps.Map(document.getElementById('map'), {
		  zoom: 12,
		  center: new google.maps.LatLng(Appdata['latitude'], Appdata['longitude']),
		  mapTypeId: google.maps.MapTypeId.ROADMAP,
		  scrollwheel: false,
		  zoomControl: true,
		  zoomControlOptions: {
		          position: google.maps.ControlPosition.LEFT_CENTER
		      }
		});

		var marker, i;

		var myLatlng = new google.maps.LatLng(Appdata['latitude'], Appdata['longitude']);
		marker = new google.maps.Marker({
		    position: myLatlng,
		      map: map
		});
		
	

	}

	else $('#map').hide();


	
	</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>