<?php 
	$auth = auth()->guard('siteUsers');
    $user = $auth->user(); 
?>
<section class="heading profile-info">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img width="100" class="user-avatar" src="/assets/site/images/avatars/default-avatar.svg">
				<ul>
					<li style="font-size: 20px;"><?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?></li>
					<li><?php echo e($user->email); ?></li>
					<li><?php echo e($user->phone); ?></li>
				</ul>
			</div>

			<div class="col-md-4">
				<a href="<?php echo e(route('user.profile.edit')); ?>" class="btn btn-success profile-edit-btn"><i class="fa fa-pencil"></i> პროფილის რედაქტირება</a>
				<a class="btn btn-success balance-edit-btn"><i class="fa fa-credit-card"></i> ბალანსის შევსება</a>
			</div>

			<div class="col-md-4">
				<div class="balance">
					ბალანსი
					<i><?php echo e($user->balance); ?></i>
					ლარი
				</div>
			</div>
		</div>

	</div>
</section>