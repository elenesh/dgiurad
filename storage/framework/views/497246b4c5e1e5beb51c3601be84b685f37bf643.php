<?php $__env->startSection('content'); ?>
<div class="col-md-10">
	<h2>ანგარიშსწორება</h2>
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>მომხმარებელი</th>
			<th>ოდენობა</th>
			<th>გადახდის საშუალება</th>
			<th>თარიღი</th>
		</tr>
		<?php foreach($payments as $m): ?>
			<tr>
				<td style="padding-top: 13px;"><?php echo e($m->id); ?></td>
				<td style="padding-top: 13px;">
					<?php if($m->user->type == 0): ?>
						<a href="<?php echo e(route('admin.users.detailed.show', $m->user->unique_id)); ?>">
					<?php else: ?>
						<a href="<?php echo e(route('admin.inc-users.detailed.show', $m->user->unique_id)); ?>">	
					<?php endif; ?>	
					<?php echo e($m->user->email); ?></a>
				</td>
				<td style="padding-top: 13px;">
					<?php if($m->amount > 0): ?>
						<span style="color:green;">+<?php echo e($m->amount); ?></span>
					<?php else: ?>
						<span style="color:red;"><?php echo e($m->amount); ?></span>
					<?php endif; ?>	
				</td>
				<td style="padding-top: 13px;"><?php echo e($m->type); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->created_at); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>