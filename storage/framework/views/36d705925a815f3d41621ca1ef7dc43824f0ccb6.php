<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


	<div id="search-page">

		<div class="container">
			<div class="row">

				<div class="col-md-7 items">

					<div ng-app="sampleapp" class="block-items style-3">

					    <div class="row" ng-controller="samplecontoller" ng-init="showData()">

					    	<a href="/<?php echo e(\App::getLocale()); ?>/application/<% i.title %>/<% i.uniqueID %>" class="col-md-12 item <% i.applicationType %>" ng-repeat="i in houses | limitTo: paginationLimit()">
					    	 	<div class="preview-img">
					    	 		<img alt="house-1" ng-src="/uploads/applications/<% i.id %>/2_<% i.mainImage %>">
					    	 	</div>
					    	 	<div class="info-block">
					    	 		<ul>
					    	 			<li><% i.title %></li>
					    	 			<li><% i.details.room %> <?php echo e(trans('search.room')); ?>, <% i.details.m2 %> <?php echo e(trans('search.m2')); ?>, <% i.details.type %>, <% i.details.city %>, <% i.details.district %></li>
					    	 			<li>ტელ: 598-11-22-33</li>
					    	 		</ul>
					    	 		<div class="caption"><% i.price %> ლარი / დღეში</div>
					    	 	</div>
					    	</a>
					    	 
					    	
					    	<div class="pagination pagination-centered">
					    	    <button class="btn style-1" ng-show="hasMoreItemsToShow()" ng-click="showMoreItems()">აჩვენე მეტი</button>
					    	</div>
					    </div>
					</div>

				</div>


				<div class="col-md-5">
					<div class="map-area">
						<div id="map"></div>
						banner
					</div>
				</div>
				
			</div>
			
		</div>

	</div>
	<!--// search-page -->

	<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
	<script type="text/javascript">

	var Appdata = <?php echo json_encode($applications); ?>



		$(function() {
			//build_map(locations);

			// angular.element('#getAplications').scope().getAplications('1');


			$(document).scroll(function() {
				headH = $('.search-area').height()+$('header').height();

				toScroll = $(document).height() - $('footer').height()-800;
				marginTop = $('.items').height()+headH-980;
				scrollTop = $(document).scrollTop();
				//console.log(scrollTop + " px " + toScroll + " px " + marginTop);

				if(scrollTop > headH && scrollTop < toScroll){
					$('.map-area').addClass('fixed-elem');

					$('.map-area').css('margin-top', 'inherit');
				}
				else if(scrollTop < headH){
					$('.map-area').removeClass('fixed-elem');
					$('.map-area').css('margin-top', 'inherit');
				}
				else{
					$('.map-area').removeClass('fixed-elem');
					$('.map-area').css('margin-top',marginTop );
				}
			});
			
		});

		


		var build_map = function(locations){


			var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 11,
			  center: new google.maps.LatLng(locations[0][1], locations[0][2]),
			  mapTypeId: google.maps.MapTypeId.ROADMAP,
			  scrollwheel: false,
			  zoomControl: true,
			  zoomControlOptions: {
			          position: google.maps.ControlPosition.LEFT_CENTER
			      }
			});

			var infowindow = new google.maps.InfoWindow();

			var marker, i;

			//Create LatLngBounds object.
	        var latlngbounds = new google.maps.LatLngBounds();

			for (i = 0; i < locations.length; i++) { 
			  var myLatlng = new google.maps.LatLng(locations[i][1], locations[i][2]);
			  marker = new google.maps.Marker({
			    position: myLatlng,
			    map: map
			  });

			  google.maps.event.addListener(marker, 'click', (function(marker, i) {
			    return function() {
			      infowindow.setContent(locations[i][0]);
			      infowindow.open(map, marker);
			    }
			  })(marker, i));

			  //Extend each marker's position in LatLngBounds object.
			  latlngbounds.extend(marker.position);

			}

			//Get the boundaries of the Map.
			var bounds = new google.maps.LatLngBounds();
			
			//Center map and adjust Zoom based on the position of all markers.
			map.setCenter(latlngbounds.getCenter());
			map.fitBounds(latlngbounds);

		}

		
	</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>