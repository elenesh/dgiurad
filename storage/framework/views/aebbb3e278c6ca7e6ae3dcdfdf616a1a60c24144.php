<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 status">
            <h1 style="text-align: center;">მუშავდება</h1>
            <h3 style="color:red; text-align:center;">გთხოვთ დაელოდოთ</h3>
            <img src="<?php echo e(url('assets/site/loading.gif')); ?>" style="height:125px;margin:auto;display:block;"/>
        </div>
        <div class="col-md-4"></div>
    </div>
    <script type="application/javascript">
        gettransStatus();
        function gettransStatus() {
            var token = '<?php echo e(csrf_token()); ?>';
            $.ajax({
                url: "<?php echo e(route('payment.check')); ?>",
                type:"POST",
                data: { '_token': token, 'tid': '<?php echo e($_COOKIE['trans-id']); ?>' },
                success:function(data){
                    if(data == 'Rejected'){
                        $('.status').html('<h1 style="text-align:center;">შეცდომა!</h1><h3 style="color:red; text-align: center;">გადახდა ვერ განხორციელდა</h3>');
                    }else if(data == 'OK'){
                        document.cookie = "trans-id=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
                        $('.status').html('<h3 style="color:green; text-align: center;">თანხა წარმატებით ჩაირიცხა</h3>');
                        setTimeout(function(){
                            window.location.href = '<?php echo e(route('user.area')); ?>';
                        }, 3000);
                    }else if(data == 'Waiting'){
                        gettransStatus();
                    }
                },error:function(){
                    gettransStatus();
                }
            });
    }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>