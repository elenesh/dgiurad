<?php $__env->startSection('content'); ?>
<?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
	$auth = auth()->guard('siteUsers');
    $user = $auth->user(); 
?>
<section class="main">
	<div class="container application-page">
		<div class="rows">
			<?php if(!empty($obj)): ?>
		        <?php echo Form::model($obj,['method' => 'post', 'files' => true, 'id' => 'post-form']); ?>

		    <?php else: ?>
		        <?php echo Form::open(['method' => 'post', 'files' => true, 'id' => 'post-form']); ?>

		    <?php endif; ?>

		    <div class="first-part">
		      <div class="row">
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('city-id', 'ქალაქი'); ?>

		            <?php echo Form::select('city_id', $cities, !empty($obj) ? $obj->city_id: null, ['class' => 'form-control', 'id' => 'city-id']); ?>

		          </div>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('title', 'სათაური'); ?>

		            <?php echo Form::text('title', null,
		                        ['required',
		                        'class'       => 'form-control',
		                         'id'         => 'title',
		                        'placeholder' => 'სათაური']); ?>

		          </div>
		        </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('district-id', 'უბანი'); ?>

		            <?php echo Form::select('district_id', $districts, !empty($obj) ? $obj->district_id: null, ['class' => 'form-control', 'id' => 'district-id']); ?>

		          </div>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('description', 'აღწერა'); ?>

		            <?php echo Form::textarea('description', null,
		                        ['required',
		                        'class'       => 'form-control',
		                        'id'          => 'description',
		                        'placeholder' => 'აღწერა',
		                        'style'       => 'height:185px;']); ?>                      
		          </div>
		        </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('street-addr', 'მისამართი'); ?>

		            <?php echo Form::text('street_addr', null,
		                        ['required',
		                        'class'       => 'form-control',
		                         'id'         => 'street-addr',
		                        'placeholder' => 'მისამართი']); ?>

		          </div>
		          <div class="form-group">
		            <?php echo Form::label('m2', 'კვადრატულობა (m2)'); ?>

		            <?php echo Form::input('number','m2', null,
		                        ['required',
		                        'class'       => 'form-control',
		                         'id'         => 'm2',
		                        'placeholder' => 'კვადრატულობა (m2)']); ?>

		          </div>
		          <div class="form-group">
		            <?php echo Form::label('room-quantity', 'ოთახების რაოდენობა'); ?>

		            <?php 
		            $rooms = [
		              '1' => '1', 
		              '2' => '2', 
		              '3' => '3', 
		              '4' => '4', 
		              '5' => '5', 
		              '6' => '6', 
		              '7' => '7', 
		              '8' => '8', 
		              '9' => '9', 
		              '10' => '10', 
		              '11' => '11', 
		              '12' => '12'
		            ]; ?>
		            <?php echo Form::select('room_quantity', $rooms, !empty($obj) ? $obj->room_quantity: null, ['class' => 'form-control', 'id' => 'room-quantity']); ?>

		          </div>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('num-of-days', 'ვადა (დღე)'); ?>

		            <?php if(isset($obj)): ?>
		              <select name="num_of_days" id="num-of-days" class="form-control" disabled>
		            <?php else: ?>
		              <select name="num_of_days" id="num-of-days" class="form-control">
		            <?php endif; ?>
		              <option value="7">7</option>
		              <option value="15">15</option>
		              <option value="30">30</option>
		              <option value="60">60</option>
		              <option value="90">90</option>
		            </select>
		          </div>
		        </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('floor', 'სართული'); ?>

		            <?php echo Form::input('number','floor', null,
		                        ['required',
		                        'class'       => 'form-control',
		                         'id'         => 'floor',
		                        'placeholder' => 'სართული']); ?>

		          </div>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('type-id', 'ტიპი'); ?>

		            <?php echo Form::select('type_id', $types, !empty($obj) ? $obj->type_id: null, ['class' => 'form-control', 'id' => 'type-id']); ?>

		          </div>
		        </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('condition-id', 'მდგომარეობა'); ?>

		            <?php echo Form::select('condition_id', $conditions, !empty($obj) ? $obj->condition_id: null, ['class' => 'form-control', 'id' => 'condition-id']); ?>

		          </div>
		        </div>
		      </div>
		      <div class="row" style="margin-bottom: 20px;">
		        <div class="col-md-5">
		          <?php echo Form::label('daily-price', 'დღიური თანხა'); ?>

		          <?php echo Form::text('daily_price', null,
		                        ['class'       => 'form-control',
		                         'id'         => 'daily-price',
		                        'placeholder' => 'დღიური თანხა']); ?>

		        </div>
		        <div class="col-md-5">
		          <?php echo Form::label('hourly-price', 'საათობრივი თანხა'); ?>

		          <?php echo Form::text('hourly_price', null,
		                        ['class'       => 'form-control',
		                         'id'         => 'hourly-price',
		                        'placeholder' => 'საათობრივი თანხა']); ?>

		        </div>
		        <div class="col-md-2">
		          <?php echo Form::label('currency', 'ვალუტა'); ?>

		          <?php 
		            $currencies = [
		              'GEL' => 'GEL',
		              'USD' => 'USD'
		            ];
		          ?>
		          <?php echo Form::select('currency', $currencies, !empty($obj) ? $obj->currency: null, ['class' => 'form-control', 'id' => 'currency']); ?>

		        </div>
		      </div>
		      <div class="form-group">
		        <div class="row">
		            <div class="col-md-4">
		                <label for="attributes">ატრიბუტები</label>
		                <div id="attributes" style="border: 1px solid #D4D4D4; border-radius: 5px; padding: 0 0 0 15px ;">
		                <?php foreach($attributes as $a): ?>
		                    <?php $i=0; ?>
		                    <div class="row">
		                        <div class="col-md-12">
		                        <?php if(!empty($obj)): ?>
		                            <?php foreach($obj->attributes as $oa): ?>
		                                <?php if($oa->id == $a->id): ?>
		                                        <?php $i=1; ?>
		                                <?php endif; ?>
		                            <?php endforeach; ?>
		                            <?php if($i == 1): ?>
		                                <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>" checked>
		                                <label for="attribute-<?php echo e($a->id); ?>"><?php echo e($a->title_geo); ?></label>
		                            <?php else: ?>
		                                <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>">
		                                <label for="attribute-<?php echo e($a->id); ?>"><?php echo e($a->title_geo); ?></label>
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>">
		                            <label for="attribute-<?php echo e($a->id); ?>"><?php echo e($a->title_geo); ?></label>
		                        <?php endif; ?>
		                        </div>
		                    </div>
		                <?php endforeach; ?>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <?php echo Form::label('comment', 'კომენტარი'); ?>

		                <?php echo Form::textarea('comment', null,
		                              ['class'       => 'form-control',
		                               'id'         => 'comment',
		                              'placeholder' => 'კომენტარი']); ?>


		            </div>
		            <div class="col-md-4">
		                <?php echo Form::label('phone', 'ტელეფონი'); ?>

		                <?php echo Form::text('phone', null,
		                              ['class'       => 'form-control',
		                               'id'         => 'phone',
		                              'placeholder' => 'ტელეფონის ნომერი (XXXXXXXXX)']); ?>


		            </div>
		        </div>
		      </div>

		      <div class="row">
		        <div class="col-md-12">
		          <div class="form-group" id="map" style="height:300px;"></div>
		        </div>
		      </div>
		    </div>


		    <div class="row image-upload">
		      <div class="col-md-12 upload-file-area">

		      <div class="upload-file">
		      <?php if(isset($obj)): ?>
			      <?php foreach($obj->images as $i): ?>
				      <article class="img-thumbnail">
				     	  <a title="წაშლა" data-id="<?php echo e($i->id); ?>" data-obj-id="<?php echo e($obj->id); ?>" class="ajax-delete"><i class="fa fa-trash"></i></a>
				          <img class="add-image prewimg" src="/uploads/applications/<?php echo e($obj->id); ?>/2_<?php echo e($i->src); ?>">

				      </article>
				  <?php endforeach; ?>
			  <?php endif; ?>	
		      	<article class="img-thumbnail">
		      	    <input name="photo[]" onchange="previewFile($(this))" type="file" />
		      	    <img class="add-image prewimg" alt="+" src="https://cdn0.iconfinder.com/data/icons/math-business-icon-set/93/1_1-512.png">

		      	</article>

		      </div>

		      

		    </div>


		    <button  class="btn btn-success submit-form" type="submit">
		    <?php if(Request::segment(4) == 'edit'): ?>
				რედაქტირება
		    <?php else: ?>
		    	დამატება
		    <?php endif; ?>
		    </button>
			
		</div>
	</div>

</div>
</select>

<?php if(isset($obj)): ?>
  <input type="hidden" name="latitude" id="latitude" value="<?php echo e($obj->latitude); ?>">
  <input type="hidden" name="longitude" id="longitude" value="<?php echo e($obj->longitude); ?>">
  <input type="hidden" name="default_image_id" id="default-image-id" value="<?php echo e($obj->default_image_id); ?>">
<?php else: ?>
  <input type="hidden" name="latitude" id="latitude" value="41.704510">
  <input type="hidden" name="longitude" id="longitude" value="44.809120">
  <input type="hidden" name="default_image_id" id="default-image-id" value="">
<?php endif; ?>
<input type="hidden" id="appl-id" value="">

<input type="hidden" id="token" name="_token" value="<?php echo csrf_token(); ?>">


	<?php echo Form::close(); ?>


<script type="text/javascript" src="/assets/js/admin-map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgP8HLE8PuUTobmMq25uRGetbbTqPgW6A&callback=initMap" async defer></script>
<script type="text/javascript">

$(document).ready(function() {
	$('.upload-file').on('click', '.add-image', function(event) {
		event.preventDefault();
		$(this).closest('article').find('input[type=file]').click();
	});

	$('.upload-file').on('click', '.delete', function(event) {
		$(this).closest('article').remove();
	});

	$('.upload-file').on('click', '.ajax-delete', function(event) {
		id = $(this).attr('data-id');
		obj_id = $(this).attr('data-obj-id');
		this_ = $(this);

		$.ajax({
			url: "<?php echo e(route('site.delete.image')); ?>",
			type: 'post',
			data: {id: id, _token: $('#token').val(),obj_id: obj_id },
			success: function(data){
				if(data == 1){
					this_.closest('article').remove();
				}
				else if(data == 2){
					alert('გთხოვთ მთავარ ფოტოდ დააყენოთ სხვა ფოტო, წინააღმდეგ შემთხვევაში ვერ შეძლებთ ამ ფოტოს წაშლას');
				}
				else{
					alert('ტექნიკური შეფერხება')
				}
			}
		})
	});

	
});

function previewFile(elem) {
	
  var preview = elem.closest('article').find('.prewimg');

  var file    = elem.closest('article').find('input[type=file]')[0].files[0];
  var reader  = new FileReader();


  reader.onloadend = function () {
  	console.log(reader.result)
    preview.attr('src', reader.result);
    elem.closest('article'). prepend('<a title="წაშლა" class="delete"><i class="fa fa-trash"></i></a>');

  }

  if (file) {

    html = '<article class="img-thumbnail">'+
          	    '<input name="photo[]" onchange="previewFile($(this))" type="file" />'+
          	    '<img class="add-image prewimg" alt="+" src="https://cdn0.iconfinder.com/data/icons/math-business-icon-set/93/1_1-512.png">'+
          	'</article>';

    $('.prewimg').removeClass('add-image');

    

    $('.upload-file').append(html);

    reader.readAsDataURL(file);

  } else {
    preview.src = "";
  }
}

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>