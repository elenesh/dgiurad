<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<section class="main">
		<div class="container application-list">
			<h3 class="page-header"><?php echo e(trans('main.apps')); ?></h3>
			<div class="row" >
				<div class="block-items style-1">
				<?php if(isset($apps[0])): ?>
					<?php foreach($apps as $a): ?>
						
						<div  class="col-md-4 item <?php echo e($a->app_type); ?> rank_<?php echo e($a->rank); ?>">
							<div class="app-date"><span class="start"><?php echo e(trans('main.added')); ?>: <?php echo $a->start; ?></span> <span class="end"> <?php echo e(trans('main.validity')); ?>: <?php echo $a->end; ?></span></div>
							<div class="preview-img">
								<a style="float:none" href="<?php echo e(route('single', [$a->slug, $a->unique_id])); ?>">
									<img src="/uploads/applications/<?php echo e($a->id); ?>/2_<?php echo e($a->defaultImage->src); ?>">
								</a>
								<div class="caption">
									<?php echo Helpers::getPrice($a); ?>

								</div>
								<div class="overlay actions" style="left:0; right:0; z-index:999">
									<a>
										<?php if($a->rank == 0): ?>
											<i class="fa fa-battery-quarter" style="color:#FF9100; margin-right:60px;"></i>
										<?php elseif($a->rank == 1): ?>
											<i class="fa fa-battery-half" style="color:#AFAF00; margin-right:60px;"></i>
										<?php elseif($a->rank == 2): ?>
											<i class="fa fa-battery-full" style="color:#5CB85C; margin-right:60px;"></i>
										<?php endif; ?>
									</a>
									<a href="<?php echo e(route('application.edit', $a->id)); ?>" class="edit" title="<?php echo e(trans('main.edit')); ?>"><?php echo e(trans('main.edit')); ?> <i class="fa fa-pencil"></i></a>
									<a data-url="<?php echo e(route('application.delete', $a->id)); ?>" data-toggle="modal" data-target=".bs-example-modal-sm" title="<?php echo e(trans('main.delete')); ?>" class="delete delete-button"><?php echo e(trans('main.delete')); ?> <i class="fa fa-trash"></i></a>
								</div>
								<?php if($a->rank != 2): ?>
									<?php if($a->rank != 1): ?>
										<div class="overlay actions" style="height:45px;bottom:0px;left:0px;top:140px;">
									<?php else: ?>
										<div class="overlay actions" style="height:30px;bottom:0px;left:0px;top:155px;">
									<?php endif; ?>
										<?php if($a->rank != 1): ?>
											<a data-url="<?php echo e(route('make.vip', [$a->id, 1])); ?>" data-toggle="modal" data-target=".bs-1-modal-sm" title="<?php echo e(trans('main.make-VIP')); ?>" class="make-vip"><i class="fa fa-battery-half" style="color:#AFAF00;"></i> <?php echo e(trans('main.make-VIP')); ?></a>
										<?php endif; ?>
										<a data-url="<?php echo e(route('make.vip', [$a->id, 0])); ?>" data-toggle="modal" data-target=".bs-2-modal-sm" title="<?php echo e(trans('main.make-SVIP')); ?>" class="make-svip"><i class="fa fa-battery-full" style="color:#5CB85C;"></i> <?php echo e(trans('main.make-SVIP')); ?></a>
								</div>
								<?php endif; ?>
							</div>
							<div class="info-block">
								<ul>
									<li><?php echo e($a->m2); ?> <?php echo e(trans('main.m')); ?><sup>2</sup></li>
									<li><?php echo e($a->room_quantity); ?> <?php echo e(trans('main.room')); ?></li>
									<li><?php echo e(L10nHelper::get($a->type)); ?></li>
								</ul>
								<div class="clearfix"></div>

								<h4><?php echo e(L10nHelper::get($a->city)); ?>

									<?php if(isset($n->district->title_geo)): ?>
										, <?php echo e(L10nHelper::get($a->district)); ?>

									<?php endif; ?>
								</h4>
							</div>
						</div>

					<?php endforeach; ?>
				<?php else: ?>


					<div class="bs-callout bs-callout-info" id="callout-navs-tabs-plugin">
						<h4><?php echo e(trans('main.you-have-no-apps')); ?></h4>
						<h5><a class="btn btn-danger" href="<?php echo e(route('app.add')); ?>"><?php echo e(trans('main.add-app')); ?></a></h5>
					</div>
			
				<?php endif; ?>
				</div>
			</div>
		</div>

		<?php if(isset($inactiveApps[0])): ?>
		<div class="container application-list">
			<h3 class="page-header"><?php echo e(trans('main.inactive-apps')); ?></h3>
			<div class="row" >
				<div class="block-items style-1">
					<?php foreach($inactiveApps as $a): ?>
						
						<div  class="col-md-4 item <?php echo e($a->app_type); ?> rank_<?php echo e($a->rank); ?>">
							<div class="app-date"><span class="start"><?php echo e(trans('main.added')); ?>: <?php echo $a->start; ?></span> <span class="end"> <?php echo e(trans('main.validity')); ?>: <?php echo $a->end; ?></span></div>
							<div class="preview-img">
								<a style="float:none" href="<?php echo e(route('single', [$a->slug, $a->unique_id])); ?>">
									<img src="/uploads/applications/<?php echo e($a->id); ?>/2_<?php echo e($a->defaultImage->src); ?>">
								</a>
								<div class="caption">
									<?php echo Helpers::getPrice($a); ?>

								</div>
								<div class="overlay actions" style="left:0; right:0; z-index:999">
									<a>
										<i class="fa fa-battery-empty" style="color:red; margin-right:60px;"></i>
									</a>
									<a href="<?php echo e(route('application.edit', $a->id)); ?>" class="edit" title="<?php echo e(trans('main.edit')); ?>"><?php echo e(trans('main.edit')); ?> <i class="fa fa-pencil"></i></a>
									<a data-url="<?php echo e(route('application.delete', $a->id)); ?>" data-toggle="modal" data-target=".bs-example-modal-sm" title="<?php echo e(trans('main.delete')); ?>" class="delete delete-button"><?php echo e(trans('main.delete')); ?> <i class="fa fa-trash"></i></a>
								</div>
								<div class="overlay actions" style="height:30px;bottom:0px;left:0px;top:155px;">
									<a data-url="<?php echo e(route('make.active', $a->id)); ?>" data-toggle="modal" data-target=".bs-4-modal-sm" title="<?php echo e(trans('main.activate')); ?>" class="make-active">
										<i class="fa fa-battery-quarter" style="color:#FF9100;"></i>
										<?php echo e(trans('main.activate')); ?>

									</a>
								</div>
							</div>
							<div class="info-block">
								<ul>
									<li><?php echo e($a->m2); ?> <?php echo e(trans('main.m')); ?><sup>2</sup></li>
									<li><?php echo e($a->room_quantity); ?> <?php echo e(trans('main.room')); ?></li>
									<li><?php echo e(L10nHelper::get($a->type)); ?></li>
								</ul>
								<div class="clearfix"></div>

								<h4><?php echo e(L10nHelper::get($a->city)); ?>

									<?php if(isset($n->district->title_geo)): ?>
										, <?php echo e(L10nHelper::get($a->district)); ?>

									<?php endif; ?>
								</h4>
							</div>
						</div>

					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</section>
	<?php echo $__env->make('helpers.delete-popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->make('helpers.vip-popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>