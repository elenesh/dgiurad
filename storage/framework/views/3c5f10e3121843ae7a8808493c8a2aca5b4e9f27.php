<header>
	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<a class="logo" href="<?php echo e(url(Request::segment(1))); ?>"></a>
			</div>
			<div class="col-md-2">
				<div class="language-swither">
					<a class="active" href="">GEO</a>
					<a href="">ENG</a>
					<a href="">RUS</a>
				</div>
			</div>
			<div class="col-md-4">
				<nav>
					<a>კონტაქტი</a>
					<a class="btn btn-danger">ბინის დამატება</a>
				</nav>
			</div>
			<div class="col-md-3">
				<div class="user-area">
					<div class="text">
						გამარჯობა, შედით<br>
						<a>ანგარიშზე</a>
					</div>
					<a class="avatar"><img alt="avatar" src="<?php echo e(asset('assets/site/images/avatars/avatar-1.jpg')); ?>"></a></a>
				</div>
			</div>
		</div>

	</div>
</header>


