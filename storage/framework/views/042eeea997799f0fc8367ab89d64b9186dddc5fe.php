<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="container reg-login">

			<div class="col-md-6 cols-ref">
				<h2><?php echo e(trans('main.register')); ?></h2>

				<div>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab"><?php echo e(trans('main.normal-user')); ?></a></li>
				    <li role="presentation"><a href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab"><?php echo e(trans('main.inc-user')); ?></a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="tab-1">
						<?php echo Form::open(['method' => 'post', 'id' => 'registrationFrom', 'url' => route('register')]); ?>

				    	<?php echo csrf_field(); ?>

				    		<div class="form-group">
				    			<label for=""><?php echo e(trans('main.email')); ?></label>
				    			<input  focus type="email" name="email" class="email1 form-control" placeholder="<?php echo e(trans('main.email')); ?>" data-error="<?php echo e(trans('main.email')); ?> <?php echo e(trans('main.wrong')); ?>" required>
				    			<div class="help-block with-errors"></div>
				    		</div>

				    		<div class="row">

					    		<div class="form-group col-md-6">
					    			<label for=""><?php echo e(trans('main.pass')); ?></label>
					    			<input type="password" name="password" class="form-control" placeholder="<?php echo e(trans('main.pass')); ?>" required>
					    		</div>
					    		<div class="form-group col-md-6">
					    			<label for="exampleInputEmail1"><?php echo e(trans('main.repeat-pass')); ?></label>
					    			<input type="password" name="password_confirmation" class="form-control" placeholder="<?php echo e(trans('main.pass')); ?>" required>
					    		</div>
				    			
					    		<div class="form-group col-md-6">
					    			<label for=""><?php echo e(trans('main.first-name')); ?></label>
					    			<input type="text" name="first_name" class="form-control" placeholder="<?php echo e(trans('main.first-name')); ?>" required>
					    		</div>

					    		<div class="form-group col-md-6">
					    			<label for=""><?php echo e(trans('main.last-name')); ?></label>
					    			<input type="text" name="last_name" class="form-control" placeholder="<?php echo e(trans('main.last-name')); ?>" required>
					    		</div>

					    		<div class="form-group col-md-6">
					    			<label for=""><?php echo e(trans('main.phone')); ?></label>
					    			<input  name="phone" type="text" class="form-control" placeholder="<?php echo e(trans('main.phone')); ?>" required>
					    		</div>

					    		<div class="form-group col-md-6 tab-1 show">
					    			<label for=""><?php echo e(trans('main.birth-date')); ?></label>
					    			<div id="birth_date" class="input-group date-picker">
					    			    <input value="" data-validation="empty-value" type="text" name="birth_date" data-format="L" data-locale="ka" class="form-control" placeholder="<?php echo e(trans('main.birth-date')); ?>" autocomplete="off" required>
					    			    <span class="input-group-addon"><i class="fa fa-calendar"></i> </span>
					    			</div>

					    		</div>

					    		<div class="form-group col-md-6 tab-2 hide">
					    			<label for=""><?php echo e(trans('main.inc-name')); ?></label>
					    			<input type="text" name="incorporated_name" class="form-control" placeholder="<?php echo e(trans('main.inc-name')); ?>" required>
					    		</div>
 
					    		<div class="form-group col-md-6 tab-2 hide">
					    			<label for=""><?php echo e(trans('main.inc-id')); ?></label>
					    			<input  maxlength="9" minlength="9" type="number" name="incorporated_id" class="form-control arr-none" placeholder="xxx xxx xxx" required>
					    		</div>


					    		<div class="clearfix"></div>
					    		<div class="form-group col-md-6">
					    			<div id="RecaptchaField1"></div>
					    		</div>

					    		<input id="userType" name="type" value="0" type="hidden">

					    		<div class="form-group col-md-6">
					    			<br>
					    			<button class="btn btn-success pull-right" type="submit"><?php echo e(trans('main.register')); ?></button>
					    		</div>

				    		</div>
				    	</form>
				    </div>

				  </div>

				</div>

			</div>

			<div class="col-md-6">
				<h2><?php echo e(trans('main.login')); ?></h2>
				<?php echo Form::open(['method' => 'post', 'id' => 'loginFrom']); ?>

				    <?php echo csrf_field(); ?>


				    <div class="col-md-12">
		        		<div class="form-group">
		        			<label for=""><?php echo e(trans('main.email')); ?></label>
		        			<input type="email" name="email" class="form-control" placeholder="<?php echo e(trans('main.email')); ?>" data-error="<?php echo e(trans('main.email')); ?> <?php echo e(trans('main.wrong')); ?>" required>
		        			<div class="help-block with-errors"></div>
		        		</div>

	    	    		<div class="form-group">
	    	    			<label for=""><?php echo e(trans('main.pass')); ?></label>
	    	    			<input type="password" name="password" class="form-control" placeholder="<?php echo e(trans('main.pass')); ?>" required>
	    	    		</div>

	    	    		<div class="form-group">
	    	    			<a href="<?php echo e(route('show.reset')); ?>"><?php echo e(trans('main.forgot-pass')); ?></a>
	    	    		</div>


					    <div class="clearfix"></div>
					    <div class="row">
						    <div class="form-group col-md-6">
						    	<div id="RecaptchaField2"></div>
						    </div>

						    <div class="form-group col-md-6">
						    	<br>
						    	<button class="btn btn-success pull-right" type="submit"><?php echo e(trans('main.login')); ?></button>
						    </div>
					    </div>

				    </div>
				<?php echo Form::close(); ?>

			</div>

	</div>




	<!-- Modal -->
	<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="modal1Label"></h4>
	      </div>
	      <div class="modal-body"></div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
	      </div>
	    </div>
	  </div>
	</div>

	<script type="text/javascript">
	var reg = null;
	var log = null;
    var CaptchaCallback = function(){
		reg = grecaptcha.render('RecaptchaField1', {'sitekey' : '6Ld-uxMTAAAAAHJSXtCjdtk8jsXkWy54qHrZK219'});
		log = grecaptcha.render('RecaptchaField2', {'sitekey' : '6Ld-uxMTAAAAAHJSXtCjdtk8jsXkWy54qHrZK219'});
    };
	</script>

	<script src="//www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit&hl=<?php echo e(\App::getLocale()); ?>" async defer></script>
	<script src="http://dgiurad.ge/assets/site/js/date-time.min.js" type="text/javascript"></script>
	<script src="http://dgiurad.ge/assets/site/js/validation.min.js" type="text/javascript"></script>
	

	<script type="text/javascript">
		$(function() {
			$('.email1').focus();
			status_msg = {'title':'შეცდომაა!!!','msg':'user incorrect'};
			
			$('#registrationFrom').validator().on('submit', function (e) { 
				if (e.isDefaultPrevented()) {
				  // handle the invalid form...
				} else {
				  // everything looks good!
				  formData = $(this).serialize();
				  e.preventDefault();
				  $.ajax({
				  	url: "<?php echo e(route('register')); ?>",
				  	type: 'POST',
				  	data: formData,
				  	success: function(data){
				  		switch(data['status']) {
				  			case 0:
								grecaptcha.reset(reg);
				  				popupModal(data);
				  				break;
				  			case 1:
								window.location.href = "<?php echo e(route('user.area')); ?>";
				  				break;
				  		}

				  		
				  		//console.log(data);
				  	}
				  })
				  
				}
			});
 
			$('#loginFrom').validator().on('submit', function (e) { 
			  if (e.isDefaultPrevented()) {
			    // handle the invalid form...
			  } else {
			    // everything looks good!
			    e.preventDefault();

			    formData = $(this).serialize();
			    e.preventDefault();
			    $.ajax({
			    	url: "<?php echo e(route('login')); ?>",
			    	type: 'POST',
			    	data: formData,
			    	success: function(data){
			    		switch(data['status']) {
				  			case 0:
								grecaptcha.reset(log);
				  				popupModal(data);
				  				break;
				  			case 1:
				  				window.location.href = "<?php echo e(route('user.area')); ?>";
				  				break;
				  		}
			    		console.log(data);
			    	}
			    })
			    //popupModal(status_msg);
			  }
			})


			$('.nav').on('click', 'li a', function() {
				tab = $(this).attr('aria-controls');
				if(tab == "tab-2"){
					$('.tab-2').removeClass('hide').addClass('show');
					$('.tab-1').removeClass('show').addClass('hide');
					$('#userType').val(1);
				}else{
					$('.tab-1').removeClass('hide').addClass('show');
					$('.tab-2').removeClass('show').addClass('hide');
					$('#userType').val(0);
					$('#birth_date input').val('');
				}
			});
		});
	</script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>