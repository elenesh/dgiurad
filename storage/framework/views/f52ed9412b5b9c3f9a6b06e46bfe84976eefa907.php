<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<section class="main">
		<div class="container application-list">
			<h3 class="page-header">განცხადებები</h3>
			<div class="row" >
				<div class="block-items style-1">
				<?php if(isset($apps[0])): ?>
					<?php foreach($apps as $a): ?>

							<div  class="col-md-4 item <?php echo e($a->app_type); ?>">
								<div class="preview-img">
									<a href="<?php echo e(route('single', [$a->slug, $a->unique_id])); ?>">
										<img src="/uploads/applications/<?php echo e($a->id); ?>/1_<?php echo e($a->defaultImage->src); ?>">
									</a>
									<div class="caption">
										<?php echo Helpers::getPrice($a); ?>

									</div>
									<div class="overlay actions">
										<a href="<?php echo e(route('application.edit', $a->id)); ?>" class="edit" title="რედაქტირება">რედაქტირება <i class="fa fa-pencil"></i></a>
										
										<a data-url="<?php echo e(route('application.delete', $a->id)); ?>" data-toggle="modal" data-target=".bs-example-modal-sm" title="წაშლა" class="delete delete-button">წაშლა <i class="fa fa-trash"></i></a>
									</div>
								</div>
								<div class="info-block">
									<ul>
										<li><?php echo e($a->m2); ?></li>
										<li><?php echo e($a->room_quantity); ?> ოთახი</li>
										<li><?php echo e($a->type->title_geo); ?></li>
									</ul>
									<div class="clearfix"></div>

									<h4><?php echo e($a->city->title_geo); ?>, <?php echo e($a->district->title_geo); ?></h4>
								</div>
							</div>

					<?php endforeach; ?>
				<?php else: ?>


					<div class="bs-callout bs-callout-info" id="callout-navs-tabs-plugin">
						<h4>თქვენ არ გაქვთ განცხადებები !!</h4>
						<h5><a class="btn btn-danger" href="<?php echo e(route('app.add')); ?>">განცხადების დამატება</a></h5>
					</div>
			
				<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<?php echo $__env->make('helpers.delete-popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>