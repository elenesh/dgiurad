<?php $__env->startSection('content'); ?>
    <div class="col-md-10 main-content-admin">
     <?php if(Request::segment(3) == 'add'): ?>
      <h2>განცხადების დამატება</h2>
    <?php else: ?>
      <h2>განცხადების რედაქტირება ID - <?php echo e($obj->unique_id); ?></h2>
    <?php endif; ?>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(route('admin.applications.show')); ?>">განცხადებები</a></li>
          <li class="active">
	          <?php if(Request::segment(3) == 'add'): ?>
	          	დამატება
	          <?php else: ?>
	          	რედაქტირება
	          <?php endif; ?>
          </li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <?php if(!empty($obj)): ?>
                    <?php echo Form::model($obj,['method' => 'post', 'files' => true, 'id' => 'post-form']); ?>

                <?php else: ?>
                    <?php echo Form::open(['method' => 'post', 'files' => true, 'id' => 'post-form']); ?>

                <?php endif; ?>
                <div class="first-part">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('user-id', 'მომხმარებელი'); ?>

                        <?php echo Form::select('user_id', $users, !empty($obj) ? $obj->user_id: null, ['class' => 'form-control', 'id' => 'user-id']); ?>

                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('city-id', 'ქალაქი'); ?>

                        <?php echo Form::select('city_id', $cities, !empty($obj) ? $obj->city_id: null, ['class' => 'form-control', 'id' => 'city-id']); ?>

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('title', 'სათაური'); ?>

                        <?php echo Form::text('title', null,
                                    ['required',
                                    'class'       => 'form-control',
                                     'id'         => 'title',
                                    'placeholder' => 'სათაური']); ?>

                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('district-id', 'უბანი'); ?>

                        <?php echo Form::select('district_id', $districts, !empty($obj) ? $obj->district_id: null, ['class' => 'form-control', 'id' => 'district-id']); ?>

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('description', 'აღწერა'); ?>

                        <?php echo Form::textarea('description', null,
                                    ['required',
                                    'class'       => 'form-control',
                                    'id'          => 'description',
                                    'placeholder' => 'აღწერა',
                                    'style'       => 'height:185px;']); ?>                      
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('street-addr', 'მისამართი'); ?>

                        <?php echo Form::text('street_addr', null,
                                    ['required',
                                    'class'       => 'form-control',
                                     'id'         => 'street-addr',
                                    'placeholder' => 'მისამართი']); ?>

                      </div>
                      <div class="form-group">
                        <?php echo Form::label('m2', 'კვადრატულობა (m2)'); ?>

                        <?php echo Form::input('number','m2', null,
                                    ['required',
                                    'class'       => 'form-control',
                                     'id'         => 'm2',
                                    'placeholder' => 'კვადრატულობა (m2)']); ?>

                      </div>
                      <div class="form-group">
                        <?php echo Form::label('room-quantity', 'ოთახების რაოდენობა'); ?>

                        <?php 
                        $rooms = [
                          '1' => '1', 
                          '2' => '2', 
                          '3' => '3', 
                          '4' => '4', 
                          '5' => '5', 
                          '6' => '6', 
                          '7' => '7', 
                          '8' => '8', 
                          '9' => '9', 
                          '10' => '10', 
                          '11' => '11', 
                          '12' => '12'
                        ]; ?>
                        <?php echo Form::select('room_quantity', $rooms, !empty($obj) ? $obj->room_quantity: null, ['class' => 'form-control', 'id' => 'room-quantity']); ?>

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('num-of-days', 'ვადა (დღე)'); ?>

                        <?php if(isset($obj)): ?>
                          <select name="num_of_days" id="num-of-days" class="form-control" disabled>
                        <?php else: ?>
                          <select name="num_of_days" id="num-of-days" class="form-control">
                        <?php endif; ?>
                          <option value="7">7</option>
                          <option value="15">15</option>
                          <option value="30">30</option>
                          <option value="60">60</option>
                          <option value="90">90</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('floor', 'სართული'); ?>

                        <?php echo Form::input('number','floor', null,
                                    ['required',
                                    'class'       => 'form-control',
                                     'id'         => 'floor',
                                    'placeholder' => 'სართული']); ?>

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('type-id', 'ტიპი'); ?>

                        <?php echo Form::select('type_id', $types, !empty($obj) ? $obj->type_id: null, ['class' => 'form-control', 'id' => 'type-id']); ?>

                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <?php echo Form::label('condition-id', 'მდგომარეობა'); ?>

                        <?php echo Form::select('condition_id', $conditions, !empty($obj) ? $obj->condition_id: null, ['class' => 'form-control', 'id' => 'condition-id']); ?>

                      </div>
                    </div>
                  </div>
                  <div class="row" style="margin-bottom: 20px;">
                    <div class="col-md-5">
                      <?php echo Form::label('daily-price', 'დღიური თანხა'); ?>

                      <?php echo Form::text('daily_price', null,
                                    ['class'       => 'form-control',
                                     'id'         => 'daily-price',
                                    'placeholder' => 'დღიური თანხა']); ?>

                    </div>
                    <div class="col-md-5">
                      <?php echo Form::label('hourly-price', 'საათობრივი თანხა'); ?>

                      <?php echo Form::text('hourly_price', null,
                                    ['class'       => 'form-control',
                                     'id'         => 'hourly-price',
                                    'placeholder' => 'საათობრივი თანხა']); ?>

                    </div>
                    <div class="col-md-2">
                      <?php echo Form::label('currency', 'ვალუტა'); ?>

                      <?php 
                        $currencies = [
                          'GEL' => 'GEL',
                          'USD' => 'USD'
                        ];
                      ?>
                      <?php echo Form::select('currency', $currencies, !empty($obj) ? $obj->currency: null, ['class' => 'form-control', 'id' => 'currency']); ?>

                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="attributes">ატრიბუტები</label>
                            <div id="attributes" style="border: 1px solid #D4D4D4; border-radius: 5px; padding: 0 0 0 15px ;">
                            <?php foreach($attributes as $a): ?>
                                <?php $i=0; ?>
                                <div class="row">
                                    <div class="col-md-12">
                                    <?php if(!empty($obj)): ?>
                                        <?php foreach($obj->attributes as $oa): ?>
                                            <?php if($oa->id == $a->id): ?>
                                                    <?php $i=1; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <?php if($i == 1): ?>
                                            <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>" checked>
                                            <label for="attribute-<?php echo e($a->id); ?>"><?php echo e($a->title_geo); ?></label>
                                        <?php else: ?>
                                            <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>">
                                            <label for="attribute-<?php echo e($a->id); ?>"><?php echo e($a->title_geo); ?></label>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>">
                                        <label for="attribute-<?php echo e($a->id); ?>"><?php echo e($a->title_geo); ?></label>
                                    <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <?php echo Form::label('phone', 'ტელეფონი'); ?>

                            <?php echo Form::text('phone', null,
                                          ['class'       => 'form-control',
                                           'id'         => 'phone',
                                          'placeholder' => 'ტელეფონის ნომერი (XXXXXXXXX)']); ?>


                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group" id="map" style="height:300px;"></div>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-bottom: 20px;">
                  <?php if(!empty($obj)): ?>
                    <?php foreach($obj->images as $o): ?>
                      <div class="col-xs-6 col-md-3">
                        <a href="/uploads/applications/<?php echo e($obj->id); ?>/2_<?php echo e($o->src); ?>" class="thumbnail appl-img">
                          <img src="/uploads/applications/<?php echo e($obj->id); ?>/2_<?php echo e($o->src); ?>" style="height:100px;">
                        </a>
                        <?php if($o->id == $obj->default_image_id): ?>
                          <button type="button" data-id="<?php echo e($o->id); ?>" class="btn btn-success imgs" style="width: 100%">მთავარი</button>
                        <?php else: ?>
                          <button type="button" data-id="<?php echo e($o->id); ?>" class="btn btn-primary imgs" style="width: 100%">მთავარზე დაყენება</button>
                        <?php endif; ?>
                          <a href="<?php echo e(route('admin.applications.image.from.edit.delete', $o->id)); ?>"><button class="btn btn-danger" style="width: 100%">წაშლა</button> </a>
                      </div>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </div>
                <div class="row image-upload">
                  <div class="col-md-12">
                    <input id="input-1" name="image" multiple="true" type="file"  data-preview-file-type="text">
                  </div>
                </div>
                <div class="first-part">
                  <?php if(isset($obj)): ?>
                    <input type="hidden" name="latitude" id="latitude" value="<?php echo e($obj->latitude); ?>">
                    <input type="hidden" name="longitude" id="longitude" value="<?php echo e($obj->longitude); ?>">
                  <?php else: ?>
                    <input type="hidden" name="latitude" id="latitude" value="41.704510">
                    <input type="hidden" name="longitude" id="longitude" value="44.809120">
                  <?php endif; ?>
                  <input type="hidden" name="default_image_id" id="default-image-id" value="">
                  <input type="hidden" id="appl-id" value="">
                  <input type="hidden" id="token" name="_token" value="<?php echo csrf_token(); ?>">
                  <div class="form-group"  style="margin-top: 15px;">
                    <button type="submit" class="btn btn-primary">
                      <?php if(isset($obj)): ?>
                        შენახვა
                      <?php else: ?>
                        შემდეგი &#8594; 
                      <?php endif; ?>
                    </button>
                  </div>
                </div>
                <?php echo Form::close(); ?>

                <div class="row complete-button">
                  <div class="form-group" style="margin:15px;">
                    <a href="<?php echo e(route('admin.applications.show')); ?>"><button class="btn btn-primary">დასრულება</button></a>
                  </div>
                </div>
                <img src="/assets/loading.gif" alt="loading.gif" style="display:none;">
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/assets/js/admin-map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgP8HLE8PuUTobmMq25uRGetbbTqPgW6A&callback=initMap" async defer></script>
    <script>

          var applID = "<?php echo e(!empty($obj)? $obj->id : 'null'); ?>";;
          function makeUploader(id){
            $("#input-1").addClass('file')
            var url = "<?php echo e(route('admin.applications.image.upload')); ?>";
            $("#input-1").fileinput({
              dataType: 'json',
              previewFileType: 'any',
              showUpload: false,
              showRemove: false,
              uploadAsync: true,
              minFileCount: 1,
              maxFileCount: 8,
              acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
              uploadUrl: url,
              uploadExtraData: {
                '_token': $('#token').val(),
                'id': id
              },
              deleteExtraData: {
                '_token': $('#token').val()
              },
              previewMaxWidth: 100,
              previewMaxHeight: 100
            }).on("filebatchselected", function(event, files) {
                $("#input-1").fileinput("upload");
            });

            $('.file-preview-thumbnails').on('click', '.set-profile', function(event) {
              addDefaultImage($(this).attr('data-id'));
              $('.set-profile').removeClass('btn-success');
              $('.set-profile').html('მთავარზე დაყენება');
              $(this).html('მთავარი');
              $(this).addClass('btn-success');
              if(id == 0){
                $('.complete-button').show();
              }
            });
          }

          $('.imgs').on('click', function(){
            addDefaultImage($(this).attr('data-id'));
            $('.imgs').removeClass('btn-success');
            $('.imgs').addClass('btn-primary');
            $('.imgs').html('მთავარზე დაყენება');
            $(this).html('მთავარი');
            $(this).addClass('btn-success');
          });

          function addDefaultImage(id){
            $.ajax({
              url: '<?php echo e(route("admin.applications.default.image.add")); ?>',
              type: 'POST',
              data: {id: id, '_token': $('#token').val(), 'appl_id': applID},
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            });
          }


          $('#post-form').submit(function(event){
            event.preventDefault();
            $.ajax({
              url: $('#post-form').attr('action'),
              type: 'POST',
              data: $('#post-form').serialize(),
            })
            .done(function(data) {
              if(data.success){
                if(edit == 0){
                  applID = data.applicationId;
                  makeUploader(data.applicationId);
                  $('.first-part').hide();
                  $('.image-upload').show(1000);
                }else{
                  window.location.href = "<?php echo e(route('admin.applications.show')); ?>";
                }
              }
            })
            .fail(function() {
              console.log("error");
            });
            
          });
          $('.complete-button').hide();
          var edit = "<?php echo e(!empty($obj)? $obj->id : '0'); ?>";
          if(edit == 0){
            $('.image-upload').hide();
          }else{
            makeUploader(edit);
          }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>