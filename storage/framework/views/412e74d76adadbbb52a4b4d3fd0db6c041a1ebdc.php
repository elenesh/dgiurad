<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<div class="container reg-login">

			<div class="col-md-6" style="border-right: 1px dashed #A5A5A5;padding-right: 30px;">
				<h2>რეგისტრაცია</h2>

				<div>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">ფიზიკური პირი</a></li>
				    <li role="presentation"><a href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">იურიდიული პირი</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="tab-1">
				    	<form id="registrationFrom" action="">
				    	<?php echo csrf_field(); ?>

				    		<div class="form-group">
				    			<label for="">ელ-ფოსტა</label>
				    			<input type="email" name="email" class="form-control" placeholder="ელ-ფოსტა" data-error="ელ-ფოსტა არასწორია" required>
				    			<div class="help-block with-errors"></div>
				    		</div>

				    		<div class="row">

					    		<div class="form-group col-md-6">
					    			<label for="">პაროლი</label>
					    			<input type="password" class="form-control" placeholder="პაროლი" required>
					    		</div>
					    		<div class="form-group col-md-6">
					    			<label for="exampleInputEmail1">გაიმეორეთ პაროლი</label>
					    			<input type="password" class="form-control" placeholder="პაროლი" required>
					    		</div>
				    			
					    		<div class="form-group col-md-6">
					    			<label for="">სახელი</label>
					    			<input type="text" class="form-control" placeholder="სახელი" required>
					    		</div>

					    		<div class="form-group col-md-6">
					    			<label for="">გვარი</label>
					    			<input type="text" class="form-control" placeholder="გვარი" required>
					    		</div>

					    		<div class="form-group col-md-6">
					    			<label for="">ტელეფონი</label>
					    			<input type="number" type="text" class="form-control" placeholder="ტელეფონი" required>
					    		</div>

					    		<div class="form-group col-md-6 tab-1 show">
					    			<label for="">დაბადების თარიღი</label>
					    			<div id="from" class="input-group date-picker">
					    			    <input value="" data-validation="empty-value" type="text" name="agreement_date" data-format="L" data-locale="ka" class="form-control" placeholder="თარიღი" autocomplete="off" required>
					    			    <span class="input-group-addon"><i class="fa fa-calendar"></i> </span>
					    			</div>

					    		</div>

					    		<div class="form-group col-md-6 tab-2 hide">
					    			<label for="">კომპანიის სახელი</label>
					    			<input type="text" class="form-control" placeholder="კომპანიის სახელი" required>
					    		</div>
 
					    		<div class="form-group col-md-6 tab-2 hide">
					    			<label for="">საიდენტიფიკაციო კოდი</label>
					    			<input type="text" class="form-control" placeholder="11 ნიშნა კოდი" required>
					    		</div>


					    		<div class="clearfix"></div>
					    		<div class="form-group col-md-6">
					    			<div id="RecaptchaField1"></div>
					    		</div>

					    		<input id="" value="type-1" type="hidden">

					    		<div class="form-group col-md-6">
					    			<br>
					    			<button class="btn btn-success pull-right" type="submit">რეგისტრაცია</button>
					    		</div>

				    		</div>
				    	</form>
				    </div>

				  </div>

				</div>

			</div>

			<div class="col-md-6">
				<h2>შესვლა</h2>
				<form id="loginFrom" action="">
				    <?php echo csrf_field(); ?>


				    <div class="col-md-12">
		        		<div class="form-group">
		        			<label for="">ელ-ფოსტა</label>
		        			<input type="email" name="email" class="form-control" placeholder="ელ-ფოსტა" data-error="ელ-ფოსტა არასწორია" required>
		        			<div class="help-block with-errors"></div>
		        		</div>

	    	    		<div class="form-group">
	    	    			<label for="">პაროლი</label>
	    	    			<input type="password" class="form-control" placeholder="პაროლი" required>
	    	    		</div>


					    <div class="clearfix"></div>
					    <div class="row">
						    <div class="form-group col-md-6">
						    	<div id="RecaptchaField2"></div>
						    </div>

						    <div class="form-group col-md-6">
						    	<br>
						    	<button class="btn btn-success pull-right" type="submit">შესვლა</button>
						    </div>
					    </div>

				    </div>
				</form>
			</div>

	</div>


	<!-- Modal -->
	<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="modal1Label"></h4>
	      </div>
	      <div class="modal-body"></div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
	      </div>
	    </div>
	  </div>
	</div>

	<script type="text/javascript">

    var CaptchaCallback = function(){
        grecaptcha.render('RecaptchaField1', {'sitekey' : '6Ld-uxMTAAAAAHJSXtCjdtk8jsXkWy54qHrZK219'});
        grecaptcha.render('RecaptchaField2', {'sitekey' : '6Ld-uxMTAAAAAHJSXtCjdtk8jsXkWy54qHrZK219'});
    };
	</script>

	<script src="//www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
	<script src="http://dgiurad.ge/assets/site/js/date-time.min.js" type="text/javascript"></script>
	<script src="http://dgiurad.ge/assets/site/js/validation.min.js" type="text/javascript"></script>
	

	<script type="text/javascript">
		$(function() {
			status_msg = {'title':'შეცდომაა!!!','msg':'user incorrect'};
			
			$('#registrationFrom').validator().on('submit', function (e) { 
				if (e.isDefaultPrevented()) {
				  // handle the invalid form...
				} else {
				  // everything looks good!
				  e.preventDefault();
				  console.log( $(this).serialize() )
				}
			});
 
			$('#loginFrom').validator().on('submit', function (e) { 
			  if (e.isDefaultPrevented()) {
			    // handle the invalid form...
			  } else {
			    // everything looks good!
			    e.preventDefault();
			    console.log( $(this).serialize() )
			    popupModal(status_msg);
			  }
			})


			$('.nav').on('click', 'li a', function() {
				tab = $(this).attr('aria-controls');
				if(tab == "tab-2"){
					$('.tab-2').removeClass('hide').addClass('show');
					$('.tab-1').removeClass('show').addClass('hide');
				}
				else{
					$('.tab-1').removeClass('hide').addClass('show');
					$('.tab-2').removeClass('show').addClass('hide');
				}
			});
		});
	</script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>