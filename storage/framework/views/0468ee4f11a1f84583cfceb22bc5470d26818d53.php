<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="/assets/fileinput/css/fileinput.min.css">
		<link rel="stylesheet" href="/assets/css/admin.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="/assets/css/bootstrap-theme.min.css">
		<!-- Latest compiled and minified JavaScript -->
		<script src="/assets/js/jquery-1.11.3.min.js"></script>
		<script src="/assets/fileinput/js/plugins/canvas-to-blob.min.js"></script>
		<script src="/assets/fileinput/js/fileinput.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/bootstrap-datepicker.min.js"></script>

	</head>
	<body>
		<?php echo $__env->make('admin.components.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php echo $__env->make('admin.components.alerts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="row" style="margin-right:0px">
			<?php echo $__env->make('admin.components.side-bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<?php echo $__env->yieldContent('content'); ?>
		</div>
		
		<div id="delete_popup" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="gridSystemModalLabel">წაშლა</h4>
					</div>
					<div class="modal-body">
						ნამდვილად გსურთ წაშლა?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
						<a href="#" id="delete_popup_ok" type="button" class="btn btn-danger">წაშლა</a>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('body').on('click', '.actions .delete-button', function(event) {
					url = $(this).attr('data-url');
					$('#delete_popup_ok').attr('href', url);
				});
			});
		</script>
	</body>
</html>