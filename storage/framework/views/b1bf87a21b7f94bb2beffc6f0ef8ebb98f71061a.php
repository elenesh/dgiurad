


<?php $__env->startSection('content'); ?>

<?php echo $__env->make('site.components.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<body>
	<?php if(\App::getLocale() == 'ka'): ?>	  
		<div class="container reg-login" style="background: #fff;color: #565a5c;padding: 25px 5px; padding-bottom: 40px;margin: 30px auto;min-height: 410px;">
      <div class="col-md-12 both">	

	        <p>DGIURAD.GE -ზე რეგისტრაცია და განცხადების განთავსება შეუძლიათ როგორც ფიზიკურ, ასევე იურიდიულ პირებს.</p>
			<p>სტანდარტული განცხადების განთავსება არის უფასო და შესაძლებელია განათავსოთ შეუზღუდავი რაოდენობით.
			Super VIP -ში და VIP -ში განცხადების დამატება არის ფასიანი.</p>
			<p>Super VIP -ში განცხადების 1 კვირით განთავსების საფასური შედგენს 2 ლარს 
			VIP -ში განცხადების 1 კვირით განთავსების საფასური შეადგენს 1 ლარს.</p>
			<p>Super VIP -ში და VIP -ში განცხადების დამატებისას საფასური შეგიძლიათ გადაიხადოთ თქვენი ვირტუალური ბალანსიდან, ან პირდაპირ გადაიხადოთ კრედიტ/დებიტ (VISA / Mastercard) ბარათებით.</p>
			 <h3>ვირტუალური ბალანსის შევსება შესაძლებელია 2 გზით:</h3>
	        <ul>
	   		<li>კრედიტ/დებიტ (VISA / Mastercard) ბარათებით</li>
	   		<li>სწრაფი გადახდის აპარატებიდან: თქვენი უნიკალური კოდის მითითებითა და სასურველი რაოდენობის თანხის შეტანით.</li>
	        </ul>		
	   </div>
	  
	
	
		<div class="col-md-6 super-vip" style="margin-top: 37px;">
			<h3>რა არის Super VIP:</h3>
				<ul>
					<li>საიტის პირველ გვერდზე ზედა პოზიციებზე მორიგეობით ჩნდება 6 შემთხვევითი Super VIP განცხადება.</li>
					<li>ძებნის შედეგებში შესაბამისი კატეგორიის განცხადებები ჩანს შემდეგი თანმიმდევრობით: პირველი გამოდის Super VIP განცხადებები, შემდეგ VIP, ხოლო შემდეგ ყველა განცხადება განთავსების ქრონოლოგიის მიხედვით.</li>
				</ul>	
		</div>


			<div class="col-md-6 vip"style=" border-right: 1px dashed #A5A5A5;
    padding-right: 30px; margin-top: 37px;">
			<h3>რა არის VIP:</h3>
				<ul>
					<li>საიტის პირველ გვერდზე ზედა პოზიციებზე მორიგეობით ჩნდება 8 შემთხვევითი  VIP განცხადება.</li>
					<li>ძებნის შედეგებში შესაბამისი კატეგორიის განცხადებები ჩანს შემდეგი თანმიმდევრობით: პირველი გამოდის Super VIP განცხადებები, შემდეგ VIP, ხოლო შემდეგ ყველა განცხადება განთავსების ქრონოლოგიის მიხედვით.</li>
				</ul>	

		</div>

	</div>
	<?php elseif(\App::getLocale() == 'en'): ?>

	<div class="container reg-login" style="background: #fff;color: #565a5c;padding: 25px 5px; padding-bottom: 40px;margin: 30px auto;min-height: 410px;">
      <div class="col-md-12 both">	

	        <p>Registration on DGIURAD.GE is allowed for Individual Persons and Legal Entities also.
Uploading standard Ad is free and you can upload unlimited amount of standard ads.
</p>
			<p>To make your Ads VIP or Super VIP is chargeable.</p>
			<p>1 week duration of Super VIP Ad is 2Gel
			</p>
			<p>1 week duration of VIP Ad is1 Gel</p>
			<p>You can pay the fee from your virtual account or directly with your credit / debit card (VISA / Mastercard).</p>
			 <h3>There are 2 options to top up you virtual account:</h3>
	        <ul>
	   		<li>1.	With credit / debit (VISA / Mastercard) cards;</li>
	   		<li>2.	By Express Pay machines: entering your unique code and indicating the amount you want to charge on.</li>
	        </ul>		
	   </div>
	  
	
		<div class="col-md-6 vip"style=" border-right: 1px dashed #A5A5A5;
    padding-right: 30px; margin-top: 37px;">
			<h3>What is Super VIP ad:</h3>
				<ul>
					<li>On the main page on top positions there will be randomly displayed 6 Super VIP ads</li>
					<li>Ads search results will be displayed according the following rule (top to down): Super VIP ads, VIP ads, Standard Ads.</li>
				</ul>	

		</div>
		<div class="col-md-6 super-vip" style="margin-top: 37px;">
			<h3>What is VIP ad:</h3>
				<ul>
					<li>On the main page on the lower position than Super VIP, there will be randomly displayed 8 VIP ads</li>
					<li>Ads search results will be displayed according the following rule (top to down): Super VIP ads, VIP ads, Standard Ads.</li>
				</ul>	
		</div>
	</div>


	<?php elseif(\App::getLocale() == 'ru'): ?>


		<div class="container reg-login" style="background: #fff;color: #565a5c;padding: 25px 5px; padding-bottom: 40px;margin: 30px auto;min-height: 410px;">
      <div class="col-md-12 both">	

	        <p>Регистрация на DGIURAD.GE возможно для физических и юридических лиц.
</p>
			<p>Загрузка стандартных объявлений бесплатно и и количество неограниченно</p>
			<p>Для того, чтобы попали в статус VIP или в Супер VIP, нужно оплатить.
			</p>
			<p>Ниделя объявлении в Супер VIP, стоит 2 лари.</p>
			<p>Ниделя объявлении в VIP, стоит 1 лари.</p>
			<p>Оплата возможна с виртуального счета или с вашего кредитной / дебетовой карты (VISA / MasterCard).</p>
			 <h3>Есть 2 варианта чтобы пополнить виртуальный счет:</h3>
	        <ul>
	   		<li>1.	С кредитной / дебетовой (VISA / MasterCard) карты</li>
	   		<li>2.	Через терминалы: введите уникальный код и укажите сумму которую вы хотите поставить на счёт</li>
	        </ul>		
	   </div>
	  
	
		<div class="col-md-6 vip"style=" border-right: 1px dashed #A5A5A5;
    padding-right: 30px; margin-top: 37px;">
			<h3>Что такое Супер VIP объявление:</h3>
				<ul>
					<li>На главной странице на верхних позициях по очереди будут отображаться 6 супер VIP объявления</li>
					<li>Результаты поиска объявления будет отображаться в соответствии со следующим правилом (сверху вниз): Супер VIP объявления VIP объявления, стандартные объявления</li>
				</ul>	

		</div>
		<div class="col-md-6 super-vip" style="margin-top: 37px;">
			<h3>Что такое VIP объявление:</h3>
				<ul>
					<li>На главной странице в нижней позиции, чем Супер VIP, по очереди будут отображаться 8 VIP объявления.</li>
					<li>Результаты поиска объявления будет отображаться в соответствии со следующим правилом (сверху вниз): Супер VIP объявления VIP объявления, стандартные объявления.</li>
				</ul>	
		</div>
	</div>


	
	<?php endif; ?>
</body>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>