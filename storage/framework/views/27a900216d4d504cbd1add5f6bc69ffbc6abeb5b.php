<?php $__env->startSection('content'); ?>
<div class="col-md-10">
	<h2>განცხადებები</h2>
	<a href="<?php echo e(route('admin.applications.add')); ?>"><button class="btn btn-primary add-button">დამატება</button></a>
	<table class="table table-striped">
		<tr>
			<th>სურათი</th>
			<th>უნიკალური ID</th>
			<th>სათაური</th>
			<th>მომხმარებელი</th>
			<th>მოქმედება</th>
		</tr>
		<?php foreach($applications as $m): ?>
			<tr>
				<td style="padding-top: 13px;">
					<?php if($m->defaultImage): ?>
						<img src='/uploads/applications/<?php echo e($m->id); ?>/2_<?php echo e($m->defaultImage->src); ?>' style="height:40px;"/>
					<?php else: ?>
						<img src='/uploads/applications/default.jpg' style="height:40px;"/>
					<?php endif; ?>
				</td>
				<td style="padding-top: 13px;"><?php echo e($m->unique_id); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->title); ?></td>
				<td style="padding-top: 13px;">
					<?php if($m->user->type == 0): ?>
						<a href="<?php echo e(route('admin.users.detailed.show', $m->user->unique_id)); ?>">
					<?php else: ?>
						<a href="<?php echo e(route('admin.inc-users.detailed.show', $m->user->unique_id)); ?>">	
					<?php endif; ?>	
					<?php echo e($m->user->email); ?></a>
				</td>
				<td class="actions">
					<a href="<?php echo e(route('admin.applications.edit', $m->id)); ?>"><button style="width: 49%" class="btn btn-warning edit-button">ჩასწორება</button></a>
					<button style="width: 49%" data-url="<?php echo e(route('admin.applications.delete', $m->id)); ?>" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>