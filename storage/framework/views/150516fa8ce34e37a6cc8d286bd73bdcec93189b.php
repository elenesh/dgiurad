<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


	<div id="getAplications" ng-controller="AplicationsCtrl"  data-ng-init="getAplications('1')">

		<section class="main">
			<div class="container">
				<div class="row" >

					<div class="block-items style-1">

						<?php foreach($superVIP as $a): ?>

							<a href="<?php echo e(route('single', [$a->slug, $a->unique_id])); ?>" class="col-md-4 item <?php echo e($a->app_type); ?>">
								<div class="preview-img">
									<img alt="house-1" src="/uploads/applications/<?php echo e($a->id); ?>/2_<?php echo e($a->defaultImage->src); ?>">
									<div class="caption">
										<?php echo Helpers::getPrice($a); ?>

									</div>
								</div>
								<div class="info-block">
									<ul>
										<li><?php echo e($a->m2); ?></li>
										<li><?php echo e($a->room_quantity); ?> ოთახი</li>
										<li><?php echo e($a->type->title_geo); ?></li>
									</ul>
									<div class="clearfix"></div>

									<h4><?php echo e($a->city->title_geo); ?>, <?php echo e($a->district->title_geo); ?></h4>
								</div>
							</a>


						<?php endforeach; ?>

						<div class="clearfix"></div>
					</div>
					<!--// block-items style-1 -->
				</div>



				<div class="banner-style-1">
					<img alt="banner" src="/assets/site/images/banners/banner-2.jpg">
				</div>

				<div class="row">
					<div class="block-items style-1">
						<?php foreach($VIP as $v): ?>
							<a href="<?php echo e(route('single', [$v->slug, $v->unique_id])); ?>" class="col-md-3 item <?php echo e($v->app_type); ?>">
								<div class="preview-img">
									<img alt="house-4" src="/uploads/applications/<?php echo e($v->id); ?>/2_<?php echo e($v->defaultImage->src); ?>">
									<div class="caption">
										<?php echo Helpers::getPrice($v); ?>

									</div>
								</div>
								<div class="info-block">
									<h4>
										<?php echo e($v->city->title_geo); ?>, <?php echo e($v->district->title_geo); ?>

									</h4>
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				</div>

				<div class="banner-style-1">
					<img alt="banner" src="/assets/site/images/banners/banner-3.jpg">
				</div>

				<div class="row">
					<div class="block-items style-2">
						<?php foreach($NEW as $n): ?>
							<a href="<?php echo e(route('single', [$n->slug, $n->unique_id])); ?>" class="col-md-4 item <?php echo e($n->app_type); ?>">
								<div class="preview-img">
									<img alt="house-4" src="/uploads/applications/<?php echo e($n->id); ?>/2_<?php echo e($n->defaultImage->src); ?>">
								</div>
								<div class="info-block">
									<h4><?php echo e($n->city->title_geo); ?>, <?php echo e($n->district->title_geo); ?></h4>
									<div class="caption">
										<?php echo Helpers::getPrice($n); ?>

									</div>
								</div>
							</a>
						<?php endforeach; ?>
					</div>
				</div>

			</div>
		</section>


	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>