<div id="delete_popup" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="gridSystemModalLabel">წაშლა</h4>
			</div>
			<div class="modal-body">
				ნამდვილად გსურთ წაშლა?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
				<a href="#" id="delete_popup_ok" type="button" class="btn btn-danger">წაშლა</a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('body').on('click', '.actions .delete-button', function(event) {
			url = $(this).attr('data-url');
			$('#delete_popup_ok').attr('href', url);
		});
	});
</script>