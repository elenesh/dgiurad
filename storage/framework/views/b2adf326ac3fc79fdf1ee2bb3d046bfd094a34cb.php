<?php $__env->startSection('content'); ?>
<div class="col-md-10">
	<h2>ტიპები</h2>
	<a href="<?php echo e(route('admin.type.add')); ?>"><button class="btn btn-primary add-button">დამატება</button></a>
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>სახელი [GEO]</th>
			<th>სახელი [ENG]</th>
			<th>სახელი [RUS]</th>
			<th>მოქმედება</th>
		</tr>
		<?php foreach($types as $m): ?>
			<tr>
				<td style="padding-top: 13px;"><?php echo e($m->id); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->title_geo); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->title_eng); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->title_rus); ?></td>
				<td class="actions">
					<a href="<?php echo e(route('admin.type.edit', $m->id)); ?>"><button class="btn btn-warning edit-button">ჩასწორება</button></a>
					<button  data-url="<?php echo e(route('admin.type.delete', $m->id)); ?>" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>