<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">

    <?php /*Static Pages*/ ?>
    <url>
        <loc><?php echo e(Request::root()); ?></loc>
        <priority>1.0</priority>
    </url>

    <?php /*Dynamic Pages*/ ?>

    <?php foreach($footerLinks as $s): ?>
        <url>
            <loc><?php echo e(Request::root() . '/ka/'.$s->slug); ?></loc>
            <priority>0.8</priority>
        </url>
    <?php endforeach; ?>

    <?php foreach($apps as $s): ?>
        <url>
            <loc><?php echo e(Request::root() . '/ka/application/'.$s->slug.'/'.$s->unique_id); ?></loc>
            <priority>0.4</priority>
        </url>
    <?php endforeach; ?>

</urlset>