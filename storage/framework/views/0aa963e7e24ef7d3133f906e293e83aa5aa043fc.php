<?php $__env->startSection('content'); ?>
<div class="col-md-10">
	<h2>მომხმარებლები</h2>
	<a href="<?php echo e(route('admin.users.add')); ?>"><button class="btn btn-primary add-button">დამატება</button></a>
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>უნიკალური ID</th>
			<th>სახელი</th>
			<th>გვარი</th>
			<th>ელ. ფოსტა</th>
			<th>ტელეფონი</th>
			<th>მოქმედება</th>
		</tr>
		<?php foreach($users as $m): ?>
			<tr>
				<td style="padding-top: 13px;"><?php echo e($m->id); ?></td>
				<td style="padding-top: 13px;"><a href="<?php echo e(route('admin.users.detailed.show', $m->unique_id)); ?>"><?php echo e($m->unique_id); ?></a></td>
				<td style="padding-top: 13px;"><?php echo e($m->first_name); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->last_name); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->email); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->phone); ?></td>
				<td class="actions">
					<a href="<?php echo e(route('admin.users.edit', $m->id)); ?>"><button style="width: 32%" class="btn btn-warning edit-button">ჩასწორება</button></a>
					<?php if($m->blocked == 0): ?>
						<a href="<?php echo e(route('admin.users.block', $m->id)); ?>"><button style="width: 32%" class="btn btn-danger block-button">დაბლოკვა</button></a>
					<?php else: ?>
						<a href="<?php echo e(route('admin.users.unblock', $m->id)); ?>"><button style="width: 32%" class="btn btn-primary block-button">განბლოკვა</button></a>
					<?php endif; ?>
					<button style="width: 32%" data-url="<?php echo e(route('admin.users.delete', $m->id)); ?>" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>