<div class="col-md-2 side-menu-col">
	<ul class="nav nav-pills nav-stacked side-menu">
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'index' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.index')); ?>">მთავარი</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'types' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.type.show')); ?>">ტიპები</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'city-district' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.city-district.show')); ?>">ქალაქები / უბნები</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'attributes' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.attributes.show')); ?>">მახასიათებლები</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'conditions' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.condition.show')); ?>">მდგომარეობა</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'users' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.users.show')); ?>">მომხმარებლები</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'inc-users' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.inc-users.show')); ?>">იურიდიული პირები</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'applications' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.applications.show')); ?>">განცხადებები</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'footer-links' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.footer-links.show')); ?>">Footer Links</a></li>
		<li role="presentation" class="<?php echo e(Request::segment(2) == 'payments' ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.payments.show')); ?>">ანგარიშსწორება</a></li>
	</ul>
</div>