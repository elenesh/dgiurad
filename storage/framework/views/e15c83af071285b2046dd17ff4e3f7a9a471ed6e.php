<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>dgiurad.ge</title>
	<meta name="description" content="">

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/site/css/app.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/site/css/sumoSelect.css')); ?>">


	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/jquery.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/angular.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/bootstrap.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/sumoselect.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/jquery.nstSlider.js')); ?>"></script>

	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/app.js')); ?>"></script>

	<script type="text/javascript" src="http://localhost:48626/takana.js"></script>
	<script type="text/javascript">
	  takanaClient.run({
	    host: 'localhost:48626' // optional, defaults to localhost:48626
	  });
	</script>


</head>
<body ng-app="myApp">

	<?php echo $__env->make('site.components.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
	<?php echo $__env->make('site.components.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</body>
</html>