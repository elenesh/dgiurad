<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container site-page">
		<div class="col-md-7 cols-ref" >

			<h2><?php echo e(trans('main.send-us-message')); ?></h2>
			<!--Alert error messages-->
			<div id="aquaria_form_alert" class="alert" role="alert" style="font-size: 14px;">
			  <strong></strong> <span></span>
			</div>
			<!--Contact Form-->
			<?php echo Form::open(['method' => 'post', 'class' => 'aquaria_form', 'role' => 'form', 'onsubmit' => 'return false']); ?>


			  <section id="aquaria_module_2" class="input-group aquaria_module col-sm-6" style="margin-bottom: 20px;">
			      <span class="input-group-addon" style="border-radius: 4px;"><i class="fa fa-envelope-o" style="font-size: 14px !important;"></i></span>
			      <input type="email" name="email" data-validation="email" data-err-msg="<?php echo e(trans('main.your-email')); ?> <?php echo e(trans('main.wrong')); ?>" class="form-control" placeholder="<?php echo e(trans('main.your-email')); ?>" autofocus="" style="font-size: 14px; height: 40px; border-radius: 4px;">
			  </section>

			  <section id="aquaria_module_3" class="input-group aquaria_module col-sm-6" style="margin-bottom: 20px;">
			      <span class="input-group-addon" style="border-radius: 4px;"><i class="fa fa-user" style="font-size: 14px !important;"></i></span>
			      <input type="text" name="name" data-validation="empty-value" data-err-msg="<?php echo e(trans('main.your-name')); ?> <?php echo e(trans('main.wrong')); ?>" class="form-control" placeholder="<?php echo e(trans('main.your-name')); ?>" style="font-size: 14px; height: 40px; border-radius: 4px;">
			  </section>


			  <section id="aquaria_module_14" class="input-group aquaria_module col-sm-12" style="margin-bottom: 20px;">
			      <textarea name="text" data-validation="" data-err-msg="" class="form-control" placeholder="<?php echo e(trans('main.text')); ?>" style="font-size: 14px; border-radius: 4px;"></textarea>
			  </section>

			  <div class="form-group">
			  	<div id="RecaptchaField1"></div>
			  </div>

			  <section id="aquaria_form_submit" class="input-group aquaria_module col-sm-5" style="margin-bottom: 20px;">
			      <button class="btn btn-lg btn-success btn-block" data-loading-text="Sending ..." autocomplete="off" type="submit" style="font-size: 18px; border-radius: 4px;"><?php echo e(trans('main.send')); ?></button>
			  </section>

			  
			</form><!--End contact Form-->
			<div class="clearfix"></div>
		</div>
		<div class="col-md-5">
			<h2><?php echo e(trans('main.contact-info')); ?></h2>
			<ul class="contact-info">
				<li>
					<i class="fa fa-map-marker"></i> <span><?php echo e(trans('main.contact-addr')); ?></span>
				</li>
				<li>
					<i class="fa fa-phone"></i> <span><?php echo e(trans('main.contact-phone')); ?></span>
				</li>
				<li>
					<i class="fa fa-envelope"></i> <span><?php echo e(trans('main.contact-email')); ?></span>
				</li>
			</ul>
		</div>
	</div>
	<div id="aquariaModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="aquariaModalLabel" aria-hidden="true" style="display: none;">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	          <h4 class="modal-title" id="aquariaModalLabel"><i style="color: green;" class="fa fa-check-circle"></i> <span id="aquariaModalLabelTitle"></span><a class="anchorjs-link" href="#aquariaModalLabel"><span class="anchorjs-icon"></span></a></h4>
	        </div>
	        <div class="modal-body">
	            <p></p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div>

	<script type="text/javascript">
	var reg = null;
    var CaptchaCallback = function(){
		reg = grecaptcha.render('RecaptchaField1', {'sitekey' : '6Ld-uxMTAAAAAHJSXtCjdtk8jsXkWy54qHrZK219'});
    };
	</script>

	<script src="//www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit&hl=ka" async defer></script>

	<script type="text/javascript">
	$(document).ready(function() {

	    //submit form
	    $('#aquaria_form_submit').on('click', 'button', function(){
	        $('#aquaria_form_alert').fadeOut(100);
	        var data = new Array();
	        function validation(){ // validation function 
	            var return_data, inp_leng = $('.form-control').length;
	            $('.form-control').each(function() {
	                var selector = $(this),
	                    validation = selector.attr('data-validation'),
	                    msg = selector.attr('data-err-msg');
	                    name = selector.attr('name');
	                    value = selector.val();

	                switch(validation) {
	                    case 'email':
	                        atpos = selector.val().indexOf("@"),
	                        dotpos = selector.val().lastIndexOf(".");
	                        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= selector.val().length) {//email validation
	                            alerting(1, msg);
	                            selector.focus();
	                            return_data = 1;
	                            return false;
	                        }
	                        break;
	                    case 'empty-value':
	                        if (selector.val() === null || selector.val() === "") {//empty value validation
	                            alerting(1, msg);
	                            selector.focus();
	                            return_data = 1;
	                            return false;
	                        }
	                        break;
	                    case 'radio':
	                        selector.parent().find('input[type=radio]').each(function() {//radio button validation
	                            if(this.checked){
	                                check = 1;
	                                return false;
	                            } 
	                            else check = 0;
	                        });
	                        if (check == 0) {
	                            alerting(1, msg);
	                            selector.parent().find('input[type=radio]').focus();
	                            return_data = 1;
	                            return false;
	                        }
	                        break;                        

	                }//End switch
	            });
	            if (return_data != 1) return true;
	        };
	        if (!validation()) return false;


	        $.ajax({ //transfer submited parameters into 'dataToMail.php' to send message
	            type: 'POST',
	            url: "<?php echo e(route('contact.send')); ?>",
	            data: new FormData($(".aquaria_form").get(0)),
	            contentType: false,
	            cache: false,
	            processData:false,
	            success: function(mailData) {//transfer success function
	                $('#aquaria_captcha-form').val('');
	                var mailData = mailData.split('$%&');
	                alerting(mailData[0], mailData[1], mailData[2]);          
	            } //End transfer success function
	        }); //End transfer function

	    }); //End submit function
	});//document ready

	// alerting data function
	function alerting(error, msg, title) {
	    var alert = $('#aquaria_form_alert'),
	        alert_strong = $('#aquaria_form_alert strong'),
	        alert_span = $('#aquaria_form_alert span'),
	        icon = "";
	    if (error == '1') {//error message
	        alert.removeClass('alert-success').addClass('alert-danger');
	        icon = '<i class="fa fa-exclamation-triangle"></i>'// error icon
	        alert_strong.html(icon);
	        alert_span.html(msg);//set error message
	        alert.fadeIn(300);
	    }
	    if (error == '0') {//success message
	        alert.fadeOut(100);
	        $('#aquariaModal').modal('show');
	        $('#aquariaModalLabelTitle').html(title);
	        $('.modal-body p').html(msg);
	        $('#aquaria_form_submit button').button('reset');
	        $('#aquariaModal').on('hidden.bs.modal', function () {
	            location.reload();
	        })
	    }
	    grecaptcha.reset(reg);
	    
	}//End alerting data function
	</script>
	<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>