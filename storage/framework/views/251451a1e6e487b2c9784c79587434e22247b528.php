<?php $__env->startSection('content'); ?>
<div class="col-md-10">
	<h2>ქალაქები / უბნები</h2>
	<a href="<?php echo e(route('admin.city-district.add', ['isDistrict' => '1'])); ?>"><button class="btn btn-primary add-button">უბნის დამატება</button></a>
	<a href="<?php echo e(route('admin.city-district.add')); ?>"><button style="margin-right:0px;" class="btn btn-primary add-button">ქალაქის დამატება</button></a>
	
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>სახელი [GEO]</th>
			<th>სახელი [ENG]</th>
			<th>სახელი [RUS]</th>
			<th>კოორდინატები</th>
			<th>მოქმედება</th>
		</tr>
		<?php foreach($cityDistrict as $m): ?>
			<tr style="background-color: #2B6699; color: white;">
				<td style="padding-top: 13px;"><?php echo e($m->id); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->title_geo); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->title_eng); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->title_rus); ?></td>
				<td style="padding-top: 13px;"><?php echo e($m->latitude); ?>, <?php echo e($m->longitude); ?></td>
				<td class="actions">
					<a href="<?php echo e(route('admin.city-district.city.edit', $m->id)); ?>"><button class="btn btn-warning edit-button">ჩასწორება</button></a>
					<button  data-url="<?php echo e(route('admin.city-district.city.delete', $m->id)); ?>" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
				</td>
			</tr>
			<?php foreach($m->districts as $d): ?>
				<tr style="background-color: white;">
					<td></td>
					<td style="padding-top: 13px;">— <?php echo e($d->title_geo); ?></td>
					<td style="padding-top: 13px;">— <?php echo e($d->title_eng); ?></td>
					<td style="padding-top: 13px;">— <?php echo e($d->title_rus); ?></td>
					<td></td>
					<td class="actions">
						<a href="<?php echo e(route('admin.city-district.district.edit', [$d->id, 'isDistrict' => '1'])); ?>"><button class="btn btn-warning edit-button">ჩასწორება</button></a>
						<button  data-url="<?php echo e(route('admin.city-district.district.delete', $d->id)); ?>" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
					</td>
				</tr>	
			<?php endforeach; ?>
		<?php endforeach; ?>
	</table>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>