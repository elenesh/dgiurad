<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<?php //print_r($application) ?>

	<div class="container single-page">
		<div class="row">
			<div class="home-slider">
				<ul id="vertical">
					<li data-thumb="/uploads/applications/<?php echo e($application->id); ?>/2_<?php echo e($application->defaultImage->src); ?>">
					    <img src="/uploads/applications/<?php echo e($application->id); ?>/1_<?php echo e($application->defaultImage->src); ?>" />
				  	</li>
				  <?php foreach($application->images as $a): ?>
					<?php if($a->id != $application->default_image_id): ?>
					  	<li data-thumb="/uploads/applications/<?php echo e($application->id); ?>/2_<?php echo e($a->src); ?>">
					  	  <img src="/uploads/applications/<?php echo e($application->id); ?>/1_<?php echo e($a->src); ?>" />
					  	</li>
					<?php endif; ?>
				  <?php endforeach; ?>

				</ul>
			</div>
			

			<div class="application-info">
				<div class="col-md-8">

					<section>
						<img width="52" class="user-avatar" src="/assets/site/images/avatars/default-avatar.svg">
						<h1>
							<?php echo e($application->title); ?>

							<span><?php echo e($application->street_addr); ?>, <?php echo e($application->district->title_geo); ?>, <?php echo e($application->city->title_geo); ?> </span>
						</h1>
					</section>


					<section>
						<h2>ბინის შესახებ</h2>
						<p class="col-md-12">
							<?php echo e($application->description); ?> 
						</p>
					</section>

					<?php 
						(int)$count = count($application->attributes);
						if((int)($count%2) == 0){
							$firstC = (int)($count/2);
							$secondC = (int)($count);
						}else{
							$firstC = (int)($count/2) + 1;
							$secondC = (int)($count);
						}
						
					?>

					<section>
						<h3>ფართი</h3>
						<ul class="col-md-6">
							<li>უძრავის ქონების ტიპი: <b><?php echo e($application->type->title_geo); ?></b></li>
							<li>მდგომარეობა: <b><?php echo e($application->condition->title_geo); ?></b></li>
							<li>ფართი: <b><?php echo e((int)$application->m2); ?> <?php echo e(trans('search.m2')); ?></b></li>
							<li>ოთახების რაოდენობა: <b><?php echo e($application->room_quantity); ?></b></li>
							<li>სართული: <b><?php echo e($application->floor); ?></b></li>
						</ul>
					</section>


					<section>
						<h3>კომფორტი</h3>
						<ul class="col-md-6">
							<?php for($i = 0; $i < $firstC; $i++ ): ?>
								<li><?php echo e($application->attributes[$i]->title_geo); ?></li>
							<?php endfor; ?>
						</ul>

						<ul class="col-md-6">
							<?php for($i = $firstC; $i < $secondC; $i++ ): ?>
								<li><?php echo e($application->attributes[$i]->title_geo); ?></li>
							<?php endfor; ?>
						</ul>
					</section>

				</div>

				<div class="col-md-4">
					<div class="price-table">
						<?php if($application->daily_price != 0): ?>
							<div class="price daily"><i><?php echo e($application->daily_price); ?></i> ლარი / დღეში </div>
						<?php endif; ?>
						<?php if($application->hourly_price != 0): ?>
							<div class="price hourly"><i><?php echo e($application->hourly_price); ?></i> ლარი / საათში </div>
						<?php endif; ?>
					</div>
					<div class="contact-table">
						<h5>კონტაქტი</h5>
						<ul>
							<li><?php echo e($application->user->first_name); ?> <?php echo e($application->user->last_name); ?></li>
							<li><?php echo e($application->phone); ?></li>
							<li><?php echo e($application->comment); ?></li>
						</ul>
					</div>
					<div id="map"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<script src="http://dgiurad.ge/assets/site/js/lightslider.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#vertical').lightSlider({
		      gallery:true,
		      item:1,
		      vertical:true,
		      verticalHeight:295,
		      vThumbWidth:50,
		      thumbItem:8,
		      thumbMargin:4,
		      slideMargin:0
		    });  
		 });
	</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>