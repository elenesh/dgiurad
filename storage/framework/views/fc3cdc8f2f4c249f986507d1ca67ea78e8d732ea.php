<?php $__env->startSection('content'); ?>
    <div class="col-md-10 main-content-admin">
        <ol class="breadcrumb">
          <li><a href="<?php echo e(route('admin.city-district.show')); ?>">ქალაქები / უბნები</a></li>
          <li class="active">
	          <?php if(Request::segment(3) == 'add'): ?>
	          	დამატება
	          <?php else: ?>
	          	რედაქტირება
	          <?php endif; ?>
          </li>
        </ol>
        <div class="row">
            <div class="col-md-6">
            	<?php 
        		if(Request::input('isDistrict')){
        			if(empty($obj)){
        				$action = route('admin.city-district.storeDistrict');
        			}else{
        				$action = route('admin.city-district.district.update', $obj->id);
        			}
        		}else{
        			if(empty($obj)){
        				$action = route('admin.city-district.store');
        			}else{
        				$action = route('admin.city-district.city.update', $obj->id);
        			}
        		}
            	?>
                <?php if(!empty($obj)): ?>
                    <?php echo Form::model($obj,['method' => 'post', 'url' => $action]); ?>

                <?php else: ?>
                    <?php echo Form::open(['method' => 'post', 'url' => $action]); ?>

                <?php endif; ?>
                <?php if(Request::input('isDistrict')): ?>
	                <div class="form-group">
	                    <label for="title-geo">ქალაქი</label>
	                    <?php echo Form::select('city_id', $cities, null, [
	                    			'class' => 'form-control'
	                    ]); ?>

	                </div> 
                <?php endif; ?>   
                <div class="form-group">
                    <label for="title-geo">სათაური [GEO]</label>
                    <?php echo Form::text('title_geo', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-geo',
                              'placeholder'=>'ქართული სათაური')); ?>

                </div>
                <div class="form-group">
                    <label for="title-eng">სათაური [ENG]</label>
                    <?php echo Form::text('title_eng', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-eng',
                              'placeholder'=>'ინგლისური სათაური')); ?>

                </div>
                <div class="form-group">
                    <label for="title-rus">სათაური [RUS]</label>
                    <?php echo Form::text('title_rus', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-rus',
                              'placeholder'=>'რუსული სათაური')); ?>

                </div>
                <?php if(!Request::input('isDistrict')): ?>
	                <div class="form-group">
	                    <label for="latitude">გრძედი</label>
	                    <?php echo Form::text('latitude', null,
	                        array('class'=>'form-control',
	                               'id' => 'latitude',
	                              'placeholder'=>'კოორდინატი: გრძედი')); ?>

	                </div>
	                <div class="form-group">
	                    <label for="longitude">განედი</label>
	                    <?php echo Form::text('longitude', null,
	                        array('class'=>'form-control',
	                               'id' => 'longitude',
	                              'placeholder'=>'კოორდინატი: განედი')); ?>

               		</div>
               	<?php endif; ?>

                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <button type="submit" class="btn btn-primary">შენახვა</button>
                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>