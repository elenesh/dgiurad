<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php if(Request::segment(2) == ''): ?>
			<?php echo e(isset($title) ?  $title : ''); ?>

		<?php else: ?>
			<?php echo e(isset($title) ?  $title .' | DGIURAD.GE' : 'DGIURAD.GE'); ?>

		<?php endif; ?>
	</title>
	<meta name="description" content="<?php echo e(isset($description) ?  $description : ''); ?>">
	<link rel="alternate" href="<?php echo e(Helpers::getLocaleChangerURL('ka')); ?>" hreflang="ka" />
	<link rel="alternate" href="<?php echo e(Helpers::getLocaleChangerURL('en')); ?>" hreflang="en" />
	<link rel="alternate" href="<?php echo e(Helpers::getLocaleChangerURL('ru')); ?>" hreflang="ru" />

	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/site/css/app.css')); ?>">

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('assets/icons/apple-icon-57x57.png')); ?>">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('assets/icons/apple-icon-60x60.png')); ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('assets/icons/apple-icon-72x72.png')); ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('assets/icons/apple-icon-76x76.png')); ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('assets/icons/apple-icon-114x114.png')); ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('assets/icons/apple-icon-120x120.png')); ?>">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('assets/icons/apple-icon-144x144.png')); ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('assets/icons/apple-icon-152x152.png')); ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('assets/icons/apple-icon-180x180.png')); ?>">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('assets/icons/android-icon-192x192.png')); ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('assets/icons/favicon-32x32.png')); ?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('assets/icons/favicon-96x96.png')); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('assets/icons/favicon-16x16.png')); ?>">
	<link rel="manifest" href="<?php echo e(asset('assets/icons/manifest.json')); ?>">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo e(asset('assets/icons/ms-icon-144x144.png')); ?>">
	<meta name="theme-color" content="#ffffff">


	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/jquery.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/angular.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/bootstrap.min.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/multiple-select.js')); ?>"></script>
	<script type="text/javascript" src="<?php echo e(asset('assets/site/js/jquery.nstSlider.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/site/js/app.js')); ?>"></script>


	<script type="application/javascript">
		var lang_object = {
			'choose' : '<?php echo e(trans('main.choose')); ?>',
			'selected' : '<?php echo e(trans('main.selected')); ?>',
			'nothing-found' : '<?php echo e(trans('main.nothing-found')); ?>',
			'all-district' : '<?php echo e(trans('main.all-district')); ?>',
			'daily' : '<?php echo e(trans('main.in-day')); ?>',
			'hourly' : '<?php echo e(trans('main.in-hour')); ?>',
			'room' : '<?php echo e(trans('main.room')); ?>',
			'tel' : '<?php echo e(trans('main.tel')); ?>'
		};
	</script>
	



</head>
<body ng-app="myApp" data-lang="<?php echo e(Request::segment(1)); ?>">
	
	<?php echo $__env->make('site.components.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->yieldContent('content'); ?>
	<?php echo $__env->make('site.components.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script type="text/javascript">
	$('.nstSlider').nstSlider({
		"rounding": {
	        "20": "1000"
	    },
	   	"left_grip_selector": ".leftGrip",
	   	"right_grip_selector": ".rightGrip",
	   	"value_bar_selector": ".bar",
	   	"value_changed_callback": function(cause, leftValue, rightValue) {
	   	    $(this).parent().find('.leftLabel input').val(leftValue);
	   	    $(this).parent().find('.rightLabel input').val(rightValue);
	   	}
	});
</script>
</body>
</html>