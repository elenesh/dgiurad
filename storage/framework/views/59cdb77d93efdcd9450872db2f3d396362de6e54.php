<?php $__env->startSection('content'); ?>
	<?php echo $__env->make('site.components.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	

	<div id="getAplications" ng-controller="AplicationsCtrl"  data-ng-init="getAplications('1')">

		<section class="main">
			<div class="container">
				<div class="row" >
					<div class="col-md-12">
						<h5 class="app-rank-header"><?php echo e(trans('main.super-vip-apps')); ?></h5>
					</div>
					
					<div class="block-items style-1">

                     
						<?php foreach($superVIP as $a): ?>
                       
                       
                   <?php /*  <ul id ="imageGallery">
				            <?php foreach($a->images as $i): ?> 
				            <li>  
				             <img src= "/uploads/applications/<?php echo e($a->id); ?>/2_<?php echo e($i->src); ?>">
				             </li>
							<?php endforeach; ?>	
							</ul> */ ?>
							<a href="<?php echo e(route('single', [$a->slug, $a->unique_id])); ?>" class="col-md-4 item <?php echo e($a->app_type); ?>">
                            

                            
                         
								<div class="preview-img rank">
                         
							
									<img alt="house-1" src="/uploads/applications/<?php echo e($a->id); ?>/2_<?php echo e($a->defaultImage->src); ?>">
									
							
						
							
									<div class="caption">
										<?php echo Helpers::getPrice($a); ?>

									</div>
								</div>


								

                                
								


								<div class="info-block">
									<ul>
										<li><?php echo e($a->m2); ?> <?php echo e(trans('main.m')); ?><sup>2</sup></li>
										<li><?php echo e($a->room_quantity); ?> <?php echo e(trans('main.room')); ?></li>
										<li><?php echo e(L10nHelper::get($a->type)); ?></li>
									</ul>
									<div class="clearfix"></div>

									<h4><?php echo e(L10nHelper::get($a->city)); ?><?php echo e(isset($a->district->title_geo) ? ", ".L10nHelper::get($a->district) : ''); ?></h4>
								</div>
							</a>


						<?php endforeach; ?>

						</ul>	

						

						<div class="clearfix"></div>
					</div>
					<!--// block-items style-1 -->
				</div>



				<div class="banner-style-1">
					<a href="http://sesxebi.ge/" rel="nofollow" target="_blank">
						<img alt="sesxebi.ge" src="/assets/site/images/banners/sesxebi-ge.png">
					</a>
					<a class="ban-caption"><?php echo e(trans('main.ad')); ?></a>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h5 class="app-rank-header"><?php echo e(trans('main.vip-apps')); ?></h5>
					</div>
					<div class="block-items style-1">

					
						<?php foreach($VIP as $v): ?>
						

						
							<a href="<?php echo e(route('single', [$v->slug, $v->unique_id])); ?>" class="col-md-3 item <?php echo e($v->app_type); ?>">
								<div class="preview-img">
									<img alt="house-4" src="/uploads/applications/<?php echo e($v->id); ?>/2_<?php echo e($v->defaultImage->src); ?>">
									<div class="caption">
										<?php echo Helpers::getPrice($v); ?>

									</div>
								</div>
								<div class="info-block">
									<h4>
										<?php echo e(L10nHelper::get($v->city)); ?><?php echo e(isset($v->district->title_geo) ? ", ".L10nHelper::get($v->district) : ''); ?>

									</h4>
								</div>
							</a>
							
						<?php endforeach; ?>
						
					</div>
				</div>

				<div class="banner-style-1">
					<a href="http://sesxebi.com/" rel="nofollow" target="_blank">
						<img alt="sesxebi.com" src="/assets/site/images/banners/sesxebi-com.jpg">
					</a>
					<a class="ban-caption"><?php echo e(trans('main.ad')); ?></a>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h5 class="app-rank-header"><?php echo e(trans('main.last-apps')); ?></h5>
					</div>
					<div class="block-items style-2 latest">
						<?php foreach($NEW as $n): ?>
							<a href="<?php echo e(route('single', [$n->slug, $n->unique_id])); ?>" class="col-lg-4 col-md-6 col-sm-6 col-xs-12 item <?php echo e($n->app_type); ?>">
								<div style="background:url('/uploads/applications/<?php echo e($n->id); ?>/2_<?php echo e($n->defaultImage->src); ?>'); background-size: 100% 100%;" class="preview-img">
									
								</div>
								<div class="info-block">
									<h4><?php echo e(L10nHelper::get($n->city)); ?><?php echo e(isset($n->district->title_geo) ? ", ".L10nHelper::get($n->district) : ''); ?></h4>
									<div class="caption">
										<?php echo Helpers::getPrice($n); ?>

									</div>
								</div>
							</a>
						<?php endforeach; ?>
						<div class="clearfix"></div>
					</div>
				</div>

			</div>
		</section>
			


	</div>
<script src="http://dgiurad.ge/assets/site/js/lightslider.min.js" type="text/javascript"></script>
	
<script type="text/javascript">
	$(document).ready(function() {
    $('#imageGallery').lightSlider({
        adaptiveHeight:true,
        item:1,
        slideMargin:0,
        loop:true
    });
});
 
	</script>
    
   

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>