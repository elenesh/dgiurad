
<style type="text/css">
	.usd-sign[data-tug="usd"]{
		display:none;
	}
	.gel-sign[data-tug="gel"]{
		display:none!important;
	}
</style>

<section  class="search-area" >
	<form action="<?php echo e(route('search')); ?>" method="GET" class="search-form">
		<div class="container">

			<a class="_search-link mob"><?php echo e(trans('main.search')); ?></a>
			<div class="row _search desc">
				<div class="col-lg-2 col-md-2">
					<div class="option-group">
						<span class="caption"><?php echo e(trans('main.city')); ?></span>
						<select id="city-select" name="city[]" multiple="multiple" class="multiple-select">
							<?php $i=0; ?>
							<?php foreach($cities as $c): ?>
								<?php $selected = ''; ?>
								<?php if(NULL !== Request::input('city')): ?>
									<?php foreach(Request::input('city') as $s): ?>
										<?php if($s == $c->id): ?>
											<?php $selected = 'selected'; ?>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php endif; ?>
								<option value="<?php echo e($c->id); ?>_<?php echo e($i); ?>" <?php echo e($selected); ?>> <?php echo e(L10nHelper::get($c)); ?></option>
									<?php $i++; ?>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-lg-3 col-md-2">
					<div class="option-group region-select">
						<span class="caption"><?php echo e(trans('main.district')); ?></span>
						 
						<select id="region-select"  disabled name="region[]" multiple="multiple" class="multiple-select">

						<?php if(NULL !== Request::input('city')): ?>

							<?php foreach(Request::input('city') as $s): 
								
								$parts = explode('_', $s);


								echo '<optgroup label='.$districtsArray[$parts[1]]['title'].'>';
								foreach($districtsArray[$parts[1]]['districts'] as $k)
								{
									if(Request::input('region') !== null)
										{
											foreach(Request::input('region') as $reg)
											{
												$selected = '';
												if($reg == $k['id'])
												{
													$selected = 'selected';
													break;
												}
											}
										}
									echo '<option '.$selected.' value="'.$k['id'].'">'.$k['title'].'</option>';
								}
								echo '</optgroup>';
							?>
							<?php endforeach;  ?>

						<?php endif; ?>
						
						</select>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 from-to-input">
					<div class="option-group">
						<span class="caption"><?php echo e(trans('main.price')); ?></span>

						<?php
						function getValue($elem,$def = ''){
							return Request::input($elem) == '' ? $def : Request::input($elem);
						}
						?>

						<div class="leftLabel"> <input name="priceFrom" type="text" /> <i class="usd-sign switcher active" data-tug="gel" style="display:inline-block;"></i><i class="gel-sign switcher" data-tug="usd" style="display:none;"></i></div>
							<div class="nstSlider" data-range_min="0" data-range_max="1000" data-cur_min="<?php echo getValue('priceFrom','0') ?>" data-cur_max="<?php echo getValue('priceTo','900') ?>">
						    	<div class="bar"></div>
						    	<div class="leftGrip"></div>
						    	<div class="rightGrip"></div>

				

							</div>

						

						<div class="rightLabel"> <input name="priceTo" type="text"/><i class="usd-sign switcher active" data-tug="gel" style="display:inline-block;"></i><i class="gel-sign switcher" data-tug="usd" style="display:none;"></i></div>
						
					</div>
				</div>
				<div class="col-lg-2 col-md-2">
					<div class="option-group">
						<div class="room-select">
							<span class="caption"><?php echo e(trans('main.area')); ?></span>
							<p class="room"><% roomFrom %>-<% roomTo %> <?php echo e(trans('main.room')); ?><label><i></i></label></p>
							<div class="wrapper">
								<input min="0" name="roomFrom" ng-model="roomFrom" ng-init="roomFrom=<?php echo getValue('roomFrom','0') ?>" maxlength="2"  value="" type="number"><?php echo e(trans('main.from')); ?><br>
								<input min="0" name="roomTo" ng-model="roomTo" ng-init="roomTo=<?php echo getValue('roomTo','15') ?>" maxlength="2"  value="" type="number"><?php echo e(trans('main.to-room')); ?>

							</div>
						</div>
					</div>
				</div>
				
				<div class="col-lg-2">
					<a class="detail-search-link"><?php echo e(trans('main.detailed-search')); ?></a>
					<button type="submit" id="search-btn" class="btn">
						<?php echo e(trans('main.search')); ?>

					</button>
				</div>
			</div>


			<div class="detail-search">
				<div class="col-lg-12 separator2"></div>

				<div class="row">
					<div class="col-lg-3 col-md-3">
						<div class="option-group free">

						<span class="caption"><?php echo e(trans('main.currency')); ?></span>
							<div class="radio-switcher">

								<input value="USD"  type="radio" id="switcher-1" name="currency">
								<label class="switcher active" data-tug="usd" for="switcher-1">USD</label>

								<input value="GEL" type="radio" id="switcher-2" name="currency">
								<label class="switcher" data-tug="gel" for="switcher-2">GEL</label>
							</div>
						</div>

						<div class="option-group">
							<span class="caption"><?php echo e(trans('main.real-estate-type')); ?></span>
							<select name="type[]" multiple="multiple" class="multiple-select2">
								<?php $i=0; ?>
								<?php foreach($types as $t): ?>
									<?php $selected = ''; ?>
									<?php if(NULL !== Request::input('type')): ?>
										<?php foreach(Request::input('type') as $s): ?>
											<?php if($s == $t->id): ?>
												<?php $selected = 'selected'; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
									<option <?php echo e($selected); ?> value="<?php echo e($t->id); ?>"><?php echo e(L10nHelper::get($t)); ?></option>
										<?php $i++; ?>
								<?php endforeach; ?>
							</select>
						</div>

						<div class="option-group">
							<span class="caption"><?php echo e(trans('main.condition')); ?></span>
							<select name="condition[]" multiple="multiple" class="multiple-select2">
								<?php $i=0; ?>
								<?php foreach($conditions as $c): ?>
									<?php $selected = ''; ?>
									<?php if(NULL !== Request::input('condition')): ?>
										<?php foreach(Request::input('condition') as $s): ?>
											<?php if($s == $c->id): ?>
												<?php $selected = 'selected'; ?>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
									<option <?php echo e($selected); ?> value="<?php echo e($c->id); ?>"><?php echo e(L10nHelper::get($c)); ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<?php 
						(int)$count = count($attributes);
						if((int)($count%3) > 1){
							$firstC = (int)($count/3) + 1;
							$secondC = (int)($count/3)*2 + 2;
							$thirdC = (int)($count/3)*3 + (int)($count%3);
						}else{
							$firstC = (int)($count/3) + (int)($count%3);
							$secondC = (int)($count/3)*2 + (int)($count%3);
							$thirdC = (int)($count/3)*3 + (int)($count%3);
						}
						
					?>
					<div class="col-lg-2 col-md-2">
						<?php for($i = 0; $i < $firstC; $i++): ?>
							<div class="checkbox">
							    <input type="checkbox" name='attributes[<?php echo e($attributes[$i]->id); ?>]' id="labelId_<?php echo e($attributes[$i]->id); ?>">
							    <label for="labelId_<?php echo e($attributes[$i]->id); ?>"> <?php echo e(L10nHelper::get($attributes[$i])); ?></label>
							</div>
						<?php endfor; ?>
					</div>
					<div class="col-lg-2 col-md-2">
						<?php for($i = $firstC; $i<$secondC; $i++): ?>
							<div class="checkbox">
							    <input type="checkbox" name='attributes[<?php echo e($attributes[$i]->id); ?>]' id="labelId_<?php echo e($attributes[$i]->id); ?>">
							    <label for="labelId_<?php echo e($attributes[$i]->id); ?>"> <?php echo e(L10nHelper::get($attributes[$i])); ?></label>
							</div>
						<?php endfor; ?>
					</div>
					<div class="col-lg-3 col-md-3">
						<?php for($i = $secondC; $i < $thirdC; $i++): ?>
							<div class="checkbox">
							    <input type="checkbox" name='attributes[<?php echo e($attributes[$i]->id); ?>]' id="labelId_<?php echo e($attributes[$i]->id); ?>">
							    <label for="labelId_<?php echo e($attributes[$i]->id); ?>"> <?php echo e(L10nHelper::get($attributes[$i])); ?></label>
							</div>
						<?php endfor; ?>
					</div>
				</div>
				<div class="row btns">
					<button class="btn style-1"><?php echo e(trans('main.filter')); ?></button>
				</div>

				
			</div>
			
		</div>
	</form>
	
</section>
<div id="loading-page">
	<img width="128" alt="loader" src="<?php echo e(asset('assets/site/images/')); ?>/loader.gif">
</div>

<script type="text/javascript">

	var SD = <?php  echo (json_encode( $_GET ) ) ?>;
	
	for (var key in SD['attributes']) {
	  if (SD['attributes'].hasOwnProperty(key)) {
	  	$('#labelId_'+key).click();
	    //alert(key + " -> " + SD['attributes'][key]);
	  }
	}


	$(function() {
		<?php if(isset($_GET['region'])) echo '$("#region-select").multipleSelect("enable");'; ?>

		page = '<?php echo e(Request::segment(2)); ?>'+'_';

		if(page == 'search_'){
			
			<?php echo '$("#region-select").removeAttr("disabled");' ?>
			$('#region-select').multipleSelect('enable');
		}


		if(typeof SD['currency'] !== 'undefined'){
			$(".radio-switcher label").removeClass('active');
			$(".radio-switcher input").removeAttr('checked');
			if(SD['currency'] == "USD"){
				$('#switcher-1').attr('checked', 'checked');
				$("label[for='switcher-1']").addClass('active');
			}  
			else if (SD['currency'] == "GEL") {
				$("label[for='switcher-2']").addClass('active');
				$('#switcher-2').attr('checked', 'checked');
			}
		}
		else{
			$('#switcher-1').attr('checked', 'checked');
			$("label[for='switcher-1']").addClass('active');
		}
		


		$('.radio-switcher').on('click', '.switcher', function(event) {
			$('.switcher').removeClass('active');
			$(this).addClass('active');
			tug = $(this).attr('data-tug');
			$('.from-to-input i').removeClass('usd-sign gel-sign')
			$('.from-to-input i').addClass(tug+'-sign')

		});

$('.leftLabel').on('click', '.switcher', function(event) {

			$('.switcher').removeClass('active ');

	 $( ".gel-sign").show();
			// $( ".usd-sign:nth-of-type(2)" ).css( "display", "none" );
			// $( ".gel-sign:nth-of-type(2)" ).css( "display", "none" );
			
			tug = $(this).attr('data-tug');
			$('.from-to-input i').removeClass('usd-sign gel-sign')
			$('.from-to-input i').addClass(tug+'-sign')

		});

$('.rightLabel').on('click', '.switcher', function(event) {

			$('.switcher').removeClass('active ');

	 $( ".gel-sign").show();
			// $( ".usd-sign:nth-of-type(2)" ).css( "display", "none" );
			// $( ".gel-sign:nth-of-type(2)" ).css( "display", "none" );
			
			tug = $(this).attr('data-tug');
			$('.from-to-input i').removeClass('usd-sign gel-sign')
			$('.from-to-input i').addClass(tug+'-sign')

		});

		

		
		
		

	});
	var city_region = <?php echo $districts; ?>

</script>