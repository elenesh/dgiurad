<?php $__env->startSection('content'); ?>
<div class="col-md-10 main-content-admin">
	<ol class="breadcrumb">
		<?php if(Request::segment(2) == "users"): ?>	
      		<li><a href="<?php echo e(route('admin.users.show')); ?>">მომხმარებლები</a></li>
      	<?php else: ?>
      		<li><a href="<?php echo e(route('admin.inc-users.show')); ?>">იურიდიული პირები</a></li>	
      	<?php endif; ?>	
      	<li class="active"><?php echo e(Request::segment(4)); ?></li>
    </ol>
	<div class="row">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">მომხმარებლის მონაცემები</div>
				<ul class="list-group">
					<li class="list-group-item"><strong>ID:</strong> <?php echo e($user->id); ?></li>
					<li class="list-group-item"><strong>უნიკალური ID:</strong> <?php echo e($user->unique_id); ?></li>
					<?php if($user->type == 1): ?>
					<li class="list-group-item"><strong>კომპანიის სახელი:</strong> <?php echo e($user->incorporated_name); ?></li>
					<?php endif; ?>
					<li class="list-group-item"><strong>სახელი:</strong> <?php echo e($user->first_name); ?></li>
					<li class="list-group-item"><strong>გვარი:</strong> <?php echo e($user->last_name); ?></li>
					<?php if($user->type == 0): ?>
					<li class="list-group-item"><strong>დაბადების თარიღი:</strong> <?php echo e($user->birth_date); ?></li>
					<?php endif; ?>
					<li class="list-group-item"><strong>ელ-ფოსტა:</strong> <?php echo e($user->email); ?></li>
					<li class="list-group-item"><strong>ტელეფონი:</strong> <?php echo e($user->phone); ?></li>
					<li class="list-group-item"><strong>ბალანსი:</strong><span style="color:green;"> <?php echo e($user->balance); ?><span></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8">
			<h2>განცხადებები</h2>
			<table class="table table-striped">
				<tr>
					<th>#</th>
					<th>უნიკალური ID</th>
					<th>სათაური</th>
					<th>განცხადების ტიპი</th>
				</tr>
				<?php foreach($user->applications as $p): ?>
				<tr>
					<td><?php echo e($p->id); ?></td>
					<td><a href="#"><?php echo e($p->unique_id); ?></a></td>
					<td><?php echo e($p->title); ?></td>
					<td>
						<?php if($p->rank == 0): ?>
							ჩვეულებრივი
						<?php elseif($p->rank == 1): ?>
							VIP
						<?php elseif($p->rank == 2): ?>
							SUPER VIP
						<?php endif; ?>			
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8">
			<h2>ანგარიშსწორება</h2>
			<table class="table table-striped">
				<tr>
					<th>#</th>
					<th>თარიღი</th>
					<th>გადახდის ტიპი</th>
					<th>ოდენობა</th>
				</tr>
				<?php foreach($user->payments as $p): ?>
				<tr>
					<td><?php echo e($p->id); ?></td>
					<td><?php echo e($p->created_at); ?></td>
					<td><?php echo e($p->type); ?></td>
					<td>
						<?php if($p->amount > 0): ?>
							<span style="color:green;">+<?php echo e($p->amount); ?></span>
						<?php else: ?>
							<span style="color:red;"><?php echo e($p->amount); ?></span>
						<?php endif; ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>