<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="/assets/fileinput/css/fileinput.min.css">
		<link rel="stylesheet" href="/assets/css/admin.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="/assets/css/bootstrap-theme.min.css">
		<!-- Latest compiled and minified JavaScript -->
		<script src="/assets/js/jquery-1.11.3.min.js"></script>
		<script src="/assets/fileinput/js/plugins/canvas-to-blob.min.js"></script>
		<script src="/assets/fileinput/js/fileinput.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/bootstrap-datepicker.min.js"></script>

	</head>
	<body>
		<?php echo $__env->make('admin.components.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<?php echo $__env->make('admin.components.alerts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="row" style="margin-right:0px">
			<?php echo $__env->make('admin.components.side-bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			<?php echo $__env->yieldContent('content'); ?>
		</div>
		
		<?php echo $__env->make('helpers.delete-popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</body>
</html>