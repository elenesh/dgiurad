<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
		<meta charset="utf-8">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/admin.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="/assets/css/bootstrap-theme.min.css">
	</head>
	<body>
		<div class="container" style="width: 300px">
			<form class="form-signin" action="" method="post">
				<h2 class="form-signin-heading" style="text-align: center">შესვლა</h2>
				<label for="inputEmail" class="sr-only">მომხმარებლის სახელი</label>
				<input type="text" name="username" id="inputEmail" class="form-control" placeholder="მომხმარებლის სახელი" required autofocus style="margin-bottom: 10px; margin-top: 20px;">
				<label for="inputPassword" class="sr-only">პაროლი</label>
				<input type="password" name="password" id="inputPassword" class="form-control" placeholder="პაროლი" required>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="remember"> დამახსოვრება
					</label>
				</div>
				<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				
				<button class="btn btn-lg btn-primary btn-block" type="submit">შესვლა</button>
			</form>
			</div> <!-- /container -->
			
			<!-- Latest compiled and minified JavaScript -->
			<script src="/assets/js/jquery-1.11.3.min.js"></script>
			<script src="/assets/js/bootstrap.min.js"></script>
		</body>
	</html>