<section  class="search-area" >
	<form action="<?php echo e(route('search')); ?>" method="GET" class="search-form">
		<div class="container">
			<div class="head">
				<div class="row">

					<div class="col-md-2">
						<div class="option-group free">
							<button onclick=" $('#filter-forpary').click()" class="btn btn-party">
								<span>წვეულებისთვის</span>
							</button>
						</div>
					</div>

					<div class="col-md-3">
						<div class="option-group free">
							<span class="caption">ვალუტა</span>
							<div class="radio-switcher">
								<input type="radio" checked id="switcher-1" name="currency">
								<label class="switcher active" for="switcher-1">USD</label>

								<input type="radio" id="switcher-2" name="currency">
								<label class="switcher" for="switcher-2">GEL</label>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<h2>ბინების ძებნა</h2>
					</div>

					<div class="col-md-3">
						<a class="detail-search-link">დეტალური ძიება</a>
					</div>

				
					<input style="display:none" id="filter-forpary" name="forpary" type="checkbox">


				</div>
				
				<span class="separator"></span>
			</div>
			
			<div class="row">
				<div class="col-md-2">
					<div class="option-group">
						<span class="caption">ქალაქი</span>
						<select id="city-select" name="city[]" disabled="disabled" multiple="multiple" class="sumoSelectt">
							<?php $i=0; ?>
							<?php foreach($cities as $c): ?>
								<option value="<?php echo e($c->id); ?>_<?php echo e($i); ?>"><?php echo e($c->title_geo); ?></option>
								<?php $i++; ?>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="option-group region-select">
						<span class="caption">უბანი</span>
						
						<select id="region-select" disabled="disabled" name="region[]" multiple="multiple" class="sumoSelectt">
						</select>
					</div>
				</div>
				<div class="col-md-3 from-to-input">
					<div class="option-group">
						<span class="caption">ფასი</span>

						<div class="leftLabel"> <input name="priceFrom" type="text"/> <i>$</i></div>
							<div class="nstSlider" data-range_min="0" data-range_max="1000" data-cur_min="100" data-cur_max="500">
						    	<div class="bar"></div>
						    	<div class="leftGrip"></div>
						    	<div class="rightGrip"></div>
							</div>
						<div class="rightLabel"> <input name="priceTo" type="text"/><i>$</i></div>
						
					</div>
				</div>
				<div class="col-md-2">
					<div class="option-group">
						<div class="room-select">
							<span class="caption">ფართი</span>
							<p class="room"><% roomFrom %>-<% roomTo %> ოთახი<label><i></i></label></p>
							<div class="wrapper">
								<input name="roomFrom" ng-model="roomFrom" ng-init="roomFrom=4" maxlength="2"  value="4" type="number">დან<br>
								<input name="roomTo" ng-model="roomTo" ng-init="roomTo=6" maxlength="2"  value="6" type="number">ოთახამდე
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<button type="submit" id="search-btn" class="btn">
						ძებნა
					</button>
				</div>
			</div>


			<div class="detail-search">
				<div class="col-md-12 separator2"></div>

				<div class="row">
					<div class="col-md-2">
						<div class="option-group">
							<h5>კეთილმოწყობა</h5>
						</div>
					</div>
					<div class="col-md-3">
						<div class="option-group">
							<span class="caption">უძრავი ქონების ტიპი</span>
							<select name="type[]" multiple="multiple" class="sumoSelectt">
								<?php foreach($types as $t): ?>
									<option value="<?php echo e($t->id); ?>"><?php echo e($t->title_geo); ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<div class="option-group">
							<span class="caption">მდგომარეობა</span>
							<select name="type[]" multiple="multiple" class="sumoSelectt">
								<?php foreach($conditions as $c): ?>
									<option value="<?php echo e($c->id); ?>"><?php echo e($c->title_geo); ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<?php 
						(int)$count = count($attributes);
						if((int)($count%3) > 1){
							$firstC = (int)($count/3) + 1;
							$secondC = (int)($count/3)*2 + 2;
							$thirdC = (int)($count/3)*3 + (int)($count%3);
						}else{
							$firstC = (int)($count/3) + (int)($count%3);
							$secondC = (int)($count/3)*2 + (int)($count%3);
							$thirdC = (int)($count/3)*3 + (int)($count%3);
						}
						
					?>
					<div class="col-md-2">
						<?php for($i = 0; $i < $firstC; $i++ ): ?>
							<div class="checkbox">
							    <input type="checkbox" name='attributes[<?php echo e($attributes[$i]->id); ?>]' id="labelId_<?php echo e($attributes[$i]->id); ?>">
							    <label for="labelId_<?php echo e($attributes[$i]->id); ?>"> <?php echo e($attributes[$i]->title_geo); ?></label>
							</div>
						<?php endfor; ?>
					</div>
					<div class="col-md-2">
						<?php for($i = $firstC; $i<$secondC; $i++ ): ?>
							<div class="checkbox">
							    <input type="checkbox" name='attributes[<?php echo e($attributes[$i]->id); ?>]' id="labelId_<?php echo e($attributes[$i]->id); ?>">
							    <label for="labelId_<?php echo e($attributes[$i]->id); ?>"> <?php echo e($attributes[$i]->title_geo); ?></label>
							</div>
						<?php endfor; ?>
					</div>
					<div class="col-md-2">
						<?php for($i = $secondC; $i < $thirdC; $i++ ): ?>
							<div class="checkbox">
							    <input type="checkbox" name='attributes[<?php echo e($attributes[$i]->id); ?>]' id="labelId_<?php echo e($attributes[$i]->id); ?>">
							    <label for="labelId_<?php echo e($attributes[$i]->id); ?>"> <?php echo e($attributes[$i]->title_geo); ?></label>
							</div>
						<?php endfor; ?>
					</div>
				</div>
				<div class="row btns">
					<button class="btn style-1">გაუქმება</button>
					<button class="btn style-1">გაფილტვრა</button>
				</div>

				
			</div>
			
		</div>
	</form>
	
</section>

<script type="text/javascript">
	var city_region = <?php echo $districts; ?>

</script>