<?php 
	$auth = auth()->guard('siteUsers');
    $user = $auth->user(); 
?>
<section class="heading profile-info">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img width="100" class="user-avatar" src="/assets/site/images/avatars/default-avatar.svg">
				<ul>
					<li style="font-size: 20px;"><?php echo e($user->first_name); ?> <?php echo e($user->last_name); ?></li>
					<li><?php echo e($user->email); ?></li>
					<li><?php echo e($user->phone); ?></li>
					<li>ID: <?php echo e($user->unique_id); ?></li>
				</ul>
			</div>

			<div class="col-md-4">
				<a href="<?php echo e(route('user.profile.edit')); ?>" class="btn btn-success profile-edit-btn"><i class="fa fa-pencil"></i> <?php echo e(trans('main.profile-edit')); ?></a>
				<a href="<?php echo e(route('user.area')); ?>" class="btn btn-success balance-edit-btn"><i class="fa fa-newspaper-o"></i> <?php echo e(trans('main.my-apps')); ?></a>
			</div>

			<div class="col-md-4">
				<a class="btn btn-success balance-edit-btn" style="margin-top: 15px;" href="<?php echo e(route('user.pay')); ?>"><i class="fa fa-credit-card"></i> <?php echo e(trans('main.charge-balance')); ?></a>
				<div class="balance">
					<?php echo e(trans('main.balance')); ?>

					<i><?php echo e($user->balance); ?></i>
					<?php echo e(trans('main.gel')); ?>

				</div>
			</div>
		</div>

	</div>
</section>