<header>
	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<a class="logo" href="<?php echo e(url(Request::segment(1))); ?>"></a>
			</div>
			<div class="col-md-2">
				<div class="language-swither">
					<a class="active" href="">GEO</a>
					<a href="">ENG</a>
					<a href="">RUS</a>
				</div>
			</div>
			<div class="col-md-4">
				<nav>
					<a>კონტაქტი</a>
					<a href="<?php echo e(route('app.add')); ?>" class="btn btn-danger">ბინის დამატება</a>
				</nav>
			</div>
			<div class="col-md-3">
				<?php $auth = auth()->guard('siteUsers'); ?>
				<?php if($auth->check()): ?>
					<div class="user-area loged">
						
						<div class="text">
							<span style="float: right;"><?php echo e($auth->user()->first_name); ?> <?php echo e($auth->user()->last_name); ?></span><br>

							<a href="<?php echo e(route('user.area')); ?>">ანგარიში</a> /
							<a href="<?php echo e(route('logout')); ?>">გამოსვლა</a>
						</div>
						<a class="avatar"><img width="45" alt="avatar" src="<?php echo e(asset('assets/site/images/avatars/default-avatar.svg')); ?>"></a></a>
					</div>
				<?php else: ?>
					<div class="user-area">
						
						<div class="text">
							გამარჯობა, შედით<br>
							<a href="<?php echo e(route('show.login')); ?>">ანგარიშზე</a>
						</div>
						<a class="avatar"><img width="45" alt="avatar" src="<?php echo e(asset('assets/site/images/avatars/default-avatar.svg')); ?>"></a></a>
					</div>
				<?php endif; ?>
			</div>
		</div>

	</div>
</header>


