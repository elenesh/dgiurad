<header>
	<div class="container" style="position: relative;">

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P337KJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P337KJ');</script>
<!-- End Google Tag Manager -->


		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-6">
				<a class="logo desc" href="<?php echo e(url(Request::segment(1))); ?>"></a>
			</div>
			<div class="col-lg-2 col-md-7 col-sm-7 desc">
				<div class="language-swither">
					<a class="<?php echo e(\App::getLocale() == 'ka' ? 'active' : ''); ?>" href="<?php echo e(Helpers::getLocaleChangerURL('ka')); ?>">ქარ</a>
					<a class="<?php echo e(\App::getLocale() == 'en' ? 'active' : ''); ?>" href="<?php echo e(Helpers::getLocaleChangerURL('en')); ?>">ENG</a>
					<a class="<?php echo e(\App::getLocale() == 'ru' ? 'active' : ''); ?>" href="<?php echo e(Helpers::getLocaleChangerURL('ru')); ?>">РУС</a>
				</div>
			</div>
			<div class="col-lg-7 desc lap1">
				<nav>
					<a href="<?php echo e(route('contact')); ?>"><?php echo e(trans('main.contact')); ?></a>
					<a href="<?php echo e(route('app.add')); ?>" class="btn btn-danger"><?php echo e(trans('main.application-add')); ?></a>

					<?php $auth = auth()->guard('siteUsers'); ?>
					<?php if($auth->check()): ?>
						<div class="user-area loged">
							
							<div class="text">
								<span style="float: right;"><?php echo e($auth->user()->first_name); ?> <?php echo e($auth->user()->last_name); ?></span><br>

								<a href="<?php echo e(route('user.area')); ?>"><?php echo e(trans('main.account')); ?></a> /
								<a href="<?php echo e(route('logout')); ?>"><?php echo e(trans('main.logout')); ?></a>
							</div>
							<a href="<?php echo e(route('login')); ?>" class="avatar"><img width="45" alt="avatar" src="<?php echo e(asset('assets/site/images/avatars/default-avatar.svg')); ?>"></a></a>
						</div>
					<?php else: ?>
						<div class="user-area">
							
							<div class="text">
								<?php echo e(trans('main.hello-login')); ?><br>
								<a href="<?php echo e(route('show.login')); ?>"><?php echo e(trans('main.on-account')); ?></a>
							</div>
							<a href="<?php echo e(route('user.area')); ?>" class="avatar"><img width="45" alt="avatar" src="<?php echo e(asset('assets/site/images/avatars/default-avatar.svg')); ?>"/></a>
						</div>
					<?php endif; ?>
				</nav>
			</div>

			<div class="col-md-12">

				<nav class="navbar navbar-default mob">
					<div class="navbar-header">
			          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			            <span class="sr-only">Toggle navigation</span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			            <span class="icon-bar"></span>
			          </button>
			          <a class="logo navbar-brand" href="<?php echo e(url(Request::segment(1))); ?>"></a>
			        </div>
			        <div id="navbar" class="navbar-collapse collapse">
			          <ul class="nav navbar-nav">
			          	<li><a href="<?php echo e(route('app.add')); ?>"><?php echo e(trans('main.application-add')); ?></a></li>
			            <li><a href="<?php echo e(route('contact')); ?>"><?php echo e(trans('main.contact')); ?></a></li>
			            <li class="dropdown">
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ენა <span class="caret"></span></a>
			              <ul class="dropdown-menu">
			                <a class="<?php echo e(\App::getLocale() == 'ka' ? 'active' : ''); ?>" href="<?php echo e(Helpers::getLocaleChangerURL('ka')); ?>">ქარ</a>
			                <a class="<?php echo e(\App::getLocale() == 'en' ? 'active' : ''); ?>" href="<?php echo e(Helpers::getLocaleChangerURL('en')); ?>">ENG</a>
			                <a class="<?php echo e(\App::getLocale() == 'ru' ? 'active' : ''); ?>" href="<?php echo e(Helpers::getLocaleChangerURL('ru')); ?>">РУС</a>
			              </ul>
			            </li>
			            <li class="dropdown">
			              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">პროფილი <span class="caret"></span></a>
			              <ul class="dropdown-menu">
			                <?php $auth = auth()->guard('siteUsers'); ?>
			                <?php if($auth->check()): ?>
			                	<li class="dropdown-header"><?php echo e($auth->user()->first_name); ?> <?php echo e($auth->user()->last_name); ?></li>
			                	<li><a href="<?php echo e(route('user.area')); ?>"><?php echo e(trans('main.account')); ?></a></li>
			                	<li><a href="<?php echo e(route('logout')); ?>"><?php echo e(trans('main.logout')); ?></a></li>
			                <?php else: ?>
			                	<li class="dropdown-header"><?php echo e(trans('main.hello-login')); ?></li>
			                	<li><a href="<?php echo e(route('show.login')); ?>"><?php echo e(trans('main.on-account')); ?></a></li>
			                <?php endif; ?>
			              </ul>
			            </li>
			          </ul>
			        </div><!--/.nav-collapse -->
			    </nav>
			</div>
			
				
		</div>

	</div>
</header>


