<div id="popup1" class="modal fade bs-1-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">VIP-ში გადაყვანა</h4>
            </div>
            <div class="modal-body">
                გაცნობებთ, რომ VIP-ში 7 დღით განცხადების გადაყვანის საფასური არის 1 ლარი.
                ნამდვილად გსურთ VIP-ში გადაყვანა?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
                <button type="button" class="btn btn-success make_vip_ok">დიახ</button>
            </div>
        </div>
    </div>
</div>
<div id="popup2" class="modal fade bs-2-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Super VIP-ში გადაყვანა</h4>
            </div>
            <div class="modal-body">
                გაცნობებთ, რომ Super VIP-ში 7 დღით განცხადების გადაყვანის საფასური არის 2 ლარი.
                ნამდვილად გსურთ Super VIP-ში გადაყვანა?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
                <button type="button" class="btn btn-success make_vip_ok">დიახ</button>
            </div>
        </div>

    </div>
</div>
<div id="popup3" class="modal fade bs-3-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">შეცდომა</h4>
            </div>
            <div class="modal-body">
                თქვენ არ გაქვთ საკმარისი თნხა
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
            </div>
        </div>
    </div>
</div>
<div id="popup4" class="modal fade bs-4-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">გააქტიურება</h4>
            </div>
            <div class="modal-body">
                ნამდვილად გსურთ განცხადების აქტივაცია?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">დახურვა</button>
                <a href="#" id="make-active-ok" type="button" class="btn btn-success">დიახ</a>
                
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        var act = '';
        $('body').on('click', '.actions .make-vip', function(event) {
            url = $(this).attr('data-url');
            //$('#make_vip_ok').attr('href', url);
            act = url;
        });
        $('body').on('click', '.actions .make-svip', function(event) {
            url = $(this).attr('data-url');
            //$('#make_svip_ok').attr('href', url);
            act = url;
        });
        $('.make_vip_ok').on('click', function(){
            $.ajax({
                url: act,
                type: 'GET',
                data: [],
                success: function(data){
                    switch(data['status']) {
                        case 2:
                            $('#popup1').modal('hide');
                            $('#popup2').modal('hide');
                            $('#popup3').modal('show');
                            break;
                        case 1:
                            window.location.href = "<?php echo e(route('user.area')); ?>";
                            break;
                    }
                }
            });
        });

        $('body').on('click', '.actions .make-active', function(event) {
            url = $(this).attr('data-url');
            $('#make-active-ok').attr('href', url);
        });
    });

</script>