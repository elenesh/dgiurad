<?php $__env->startSection('content'); ?>
<?php echo $__env->make('site.components.user-area-heading', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php 
	$auth = auth()->guard('siteUsers');
    $user = $auth->user(); 
?>
<style>
	article
{
    width: 80%;
    margin:auto;
    margin-top:10px;
}


.thumbnail{

    height: 100px;
    margin: 10px;    
}
.delete {
	margin-top:-35px;
}






</style>
<section class="main">
	<div class="container application-page">
		<div class="rows" style="margin-left: 20px; margin-right: 20px;">
			<?php if(!empty($obj)): ?>
		        <?php echo Form::model($obj,['method' => 'post', 'files' => true, 'id' => 'post-form']); ?>

		    <?php else: ?>
		        <?php echo Form::open(['method' => 'post', 'files' => true, 'id' => 'post-form']); ?>

		    <?php endif; ?>

		    

		    <div class="first-part">
		      <div class="row">
				  <div class="col-md-12">

			  		    	<ul class="clear nav nav-tabs language">
			                	<li class="active"><a data-toggle="tab" href="#ge_1"><?php echo e(trans('main.title')); ?> (ka) <img src="/assets/site/images/ka.png"></a></li>
			                	<li class=""><a data-toggle="tab" href="#en_1"><?php echo e(trans('main.title')); ?> (en) <img src="/assets/site/images/en.png"></a></li>
			                	<li><a data-toggle="tab" href="#ru_1"> <?php echo e(trans('main.title')); ?> (ru) <img src="/assets/site/images/ru.png"></a></li>
			            	</ul>

			            	<div class="tab-content">
			  	            <div class="form-group tab-pane active" id="ge_1">
			  	              <?php echo Form::text('title_geo', null,
                                      ['required',
                                      'class'       => 'form-control',
                                      'placeholder' => trans("main.title").' '.trans('main.in-georgian'),
                                      'data-lan' => 'ka',
                                      'maxlength' =>'50']); ?>

			  	            </div>
			  	            <div class="form-group tab-pane" id="en_1">
			  	              <?php echo Form::text('title_eng', null,
                                      [
                                      'class'       => 'form-control',
                                      'placeholder' => trans("main.title").' '.trans('main.in-english'),
                                      'data-lan' => 'en',
                                      'maxlength' =>'50']); ?>

			  	            </div>
			  	            <div class="form-group tab-pane" id="ru_1">
			  	              <?php echo Form::text('title_rus', null,
                                      [
                                      'class'       => 'form-control',
                                      'placeholder' => trans("main.title").' '.trans('main.in-russian'),
                                      'data-lan' => 'ru',
                                      'maxlength' =>'50']); ?>

			  	            </div>
			            	</div>
				  </div>
		      </div>
		      <div class="row">
				  <div class="col-md-6">
					  <div class="form-group">
						  <?php echo Form::label('city-id', trans('main.city')); ?>

						  <?php echo Form::select('city_id', $cities, !empty($obj) ? $obj->city_id: null, ['class' => 'form-control', 'id' => 'city-id']); ?>

					  </div>
				  </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('district-id', trans('main.district')); ?>

		            <?php echo Form::select('district_id', $districts, !empty($obj) ? $obj->district_id: null, ['class' => 'form-control', 'id' => 'district-id']); ?>

		          </div>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-6">
	    		    <ul class="clear nav nav-tabs language">
	                  	<li class="active"><a data-toggle="tab" href="#ge_2"><?php echo e(trans('main.description')); ?> (ka) <img src="/assets/site/images/ka.png"></a></li>
	                  	<li class=""><a data-toggle="tab" href="#en_2"><?php echo e(trans('main.description')); ?> (en) <img src="/assets/site/images/en.png"></a></li>
	                  	<li><a data-toggle="tab" href="#ru_2"><?php echo e(trans('main.description')); ?> (ru) <img src="/assets/site/images/ru.png"></a></li>
	              	</ul>

	              	<div class="tab-content">
			  	        <div class="form-group tab-pane active" id="ge_2">
		  	              <?php echo Form::textarea('description_geo', null,
		                        ['required',
		                        'class'       => 'form-control',
		                        'id'          => 'description',
		                        'data-lan' => 'ka',
		                        'placeholder' => trans('main.description').' '.trans('main.in-georgian'),
		                        'style'       => 'height:185px;',
	                            'maxlength' =>'3000']); ?>  
			  	        </div>

			  	        <div class="form-group tab-pane" id="en_2">
		  	              <?php echo Form::textarea('description_eng', null,
		                        [
		                        'class'       => 'form-control',
		                        'id'          => 'description',
		                        'data-lan' => 'en',
		                        'placeholder' => trans('main.description').' '.trans('main.in-english'),
		                        'style'       => 'height:185px;',
	                            'maxlength' =>'3000']); ?>  
			  	        </div>

    		  	        <div class="form-group tab-pane" id="ru_2">
    	  	              <?php echo Form::textarea('description_rus', null,
    	                        [
    	                        'class'       => 'form-control',
    	                        'id'          => 'description',
    	                        'data-lan' => 'ru',
    	                        'placeholder' => trans('main.description').' '.trans('main.in-russian'),
    	                        'style'       => 'height:185px;',
                                'maxlength' =>'3000']); ?>  
    		  	        </div> 
	              	</div>
		          	
		        </div>
		        <div class="col-md-6">
    			    <ul class="clear nav nav-tabs language">
    	              	<li class="active"><a data-toggle="tab" href="#ge_3"><?php echo e(trans('main.street-addr')); ?> (ka) <img src="/assets/site/images/ka.png"></a></li>
    	              	<li class=""><a data-toggle="tab" href="#en_3"><?php echo e(trans('main.street-addr')); ?> (en) <img src="/assets/site/images/en.png"></a></li>
    	              	<li><a data-toggle="tab" href="#ru_3"><?php echo e(trans('main.street-addr')); ?> (ru) <img src="/assets/site/images/ru.png"></a></li>
    	          	</ul>
    	          	<div class="tab-content">
			  	        <div class="form-group tab-pane active" id="ge_3">
				            <?php echo Form::text('street_addr_geo', null,
				                        ['required',
				                        'class'       => 'form-control',
				                         'id'         => 'street-addr',
				              
				                         'data-lan' => 'ka',
				                        'placeholder' => trans('main.street-addr').' '.trans('main.in-georgian'),
		                                      'maxlength' =>'100']); ?>

		          		</div>
  			  	        <div class="form-group tab-pane " id="en_3">
  				            <?php echo Form::text('street_addr_rus', null,
  				                        [
  				                        'class'       => 'form-control',
  				                         'id'         => 'street-addr',
  				                         'data-lan' => 'en',
  				                        'placeholder' => trans('main.street-addr').' '.trans('main.in-english'),
  		                                      'maxlength' =>'100']); ?>

  		          		</div>
  			  	        <div class="form-group tab-pane " id="ru_3">
  				            <?php echo Form::text('street_addr_eng', null,
  				                        [
  				                        'class'       => 'form-control',
  				                         'id'         => 'street-addr',
  				                         'data-lan' => 'ru',
  				                        'placeholder' => trans('main.street-addr').' '.trans('main.in-russian'),
  		                                      'maxlength' =>'100']); ?>

  		          		</div>
		          	</div>
		          <div class="form-group">
		            <?php echo Form::label('m2', trans('main.m2').' (m2)'); ?>

		            <?php echo Form::input('number','m2', null,
		                        ['required',
		                        'class'       => 'form-control',
		                         'id'         => 'm2',
		                        'placeholder' => trans('main.m2').' (m2)',
		                        'min' => '0']); ?>

		          </div>
		          <div class="form-group">
		            <?php echo Form::label('room-quantity', trans('main.room-quantity')); ?>

		            <?php 
		            $rooms = [
		              '1' => '1', 
		              '2' => '2', 
		              '3' => '3', 
		              '4' => '4', 
		              '5' => '5', 
		              '6' => '6', 
		              '7' => '7', 
		              '8' => '8', 
		              '9' => '9', 
		              '10' => '10', 
		              '11' => '11', 
		              '12' => '12'
		            ]; ?>
		            <?php echo Form::select('room_quantity', $rooms, !empty($obj) ? $obj->room_quantity: null, ['class' => 'form-control', 'id' => 'room-quantity']); ?>

		          </div>
		        </div>
		      </div>

		      <div class="row">

			      <div class="col-md-6">
			        <div class="form-group">
			          <?php echo Form::label('bathroom_quantity', trans('main.bathroom')); ?>

			          <?php echo Form::input('number','bathroom_quantity', null,
			                      [
			                      'id' => 'bathroom_quantity',
			                      'class'       => 'form-control',
			                      'placeholder' => trans('main.bathroom'),
			                      'min' => '0']); ?>

			        </div>
			      </div>

			      <div class="col-md-6">
			        <div class="form-group">
			          <?php echo Form::label('bedroom_quantity', trans('main.bedroom')); ?>

			          <?php echo Form::input('number','bedroom_quantity', null,
			                      [
			                      'id' => 'badroom_quantity',
			                      'class'       => 'form-control',
			                      'placeholder' => trans('main.bedroom'),
			                      'min' => '0']); ?>

			        </div>
			      </div>

		      
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('num-of-days', trans('main.validity-day')); ?>

		            <?php if(isset($obj)): ?>
		              <select name="num_of_days" id="num-of-days" class="form-control" disabled>
		            <?php else: ?>
		              <select name="num_of_days" id="num-of-days" class="form-control">
		            <?php endif; ?>
		              <option value="7">7</option>
		              <option value="15">15</option>
		              <option value="30">30</option>
		              <option value="60">60</option>
		              <option value="90">90</option>
		            </select>
		          </div>
		        </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('floor', trans('main.floor')); ?>

		            <?php echo Form::input('number','floor', null,
		                        [
		                        'class'       => 'form-control',
		                         'id'         => 'floor',
		                        'placeholder' => trans('main.floor'),
		                        'min' => '-2']); ?>

		          </div>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('type-id', trans('main.type')); ?>

		            <?php echo Form::select('type_id', $types, !empty($obj) ? $obj->type_id: null, ['class' => 'form-control', 'id' => 'type-id']); ?>

		          </div>
		        </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            <?php echo Form::label('condition-id', trans('main.condition')); ?>

		            <?php echo Form::select('condition_id', $conditions, !empty($obj) ? $obj->condition_id: null, ['class' => 'form-control', 'id' => 'condition-id']); ?>

		          </div>
		        </div>
		      </div>
		      <div class="row" style="margin-bottom: 20px;">
		        <div class="col-md-5">
		          <?php echo Form::label('daily-price', trans('main.daily-price')); ?>

		          <?php echo Form::text('daily_price', null,
		                        ['class'       => 'form-control',
		                         'id'         => 'daily-price',
		                        'placeholder' => trans('main.daily-price')]); ?>

		        </div>
		        <div class="col-md-5">
		          <?php echo Form::label('hourly-price', trans('main.hourly-price')); ?>

		          <?php echo Form::text('hourly_price', null,
		                        ['class'       => 'form-control',
		                         'id'         => 'hourly-price',
		                        'placeholder' => trans('main.hourly-price')]); ?>

		        </div>
		        <div class="col-md-2">
		          <?php echo Form::label('currency', trans('main.currency')); ?>

		          <?php 
		            $currencies = [
		              'USD' => 'USD',
					  'GEL' => 'GEL'
		            ];
		          ?>
		          <?php echo Form::select('currency', $currencies, !empty($obj) ? $obj->currency: null, ['class' => 'form-control', 'id' => 'currency']); ?>

		        </div>
		      </div>
		      <div class="form-group">
		        <div class="row">
		            <div class="col-md-8">
		                <label for="attributes"><?php echo e(trans('main.attributes')); ?></label>
		                <div id="attributes" style="border: 1px solid #D4D4D4; border-radius: 5px; padding: 0 0 0 15px ;">
		                <div class="row">
		                <?php foreach($attributes as $a): ?>
		                    <?php $i=0; ?>
		                        <div class="col-md-4">
		                        <?php if(!empty($obj)): ?>
		                            <?php foreach($obj->attributes as $oa): ?>
		                                <?php if($oa->id == $a->id): ?>
		                                        <?php $i=1; ?>
		                                <?php endif; ?>
		                            <?php endforeach; ?>
		                            <?php if($i == 1): ?>
		                                <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>" checked>
		                                <label for="attribute-<?php echo e($a->id); ?>"><?php echo e(L10nHelper::get($a)); ?></label>
		                            <?php else: ?>
		                                <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>">
		                                <label for="attribute-<?php echo e($a->id); ?>"><?php echo e(L10nHelper::get($a)); ?></label>
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <input type="checkbox" value="<?php echo e($a->id); ?>" name="attributes[]" id="attribute-<?php echo e($a->id); ?>">
		                            <label for="attribute-<?php echo e($a->id); ?>"><?php echo e(L10nHelper::get($a)); ?></label>
		                        <?php endif; ?>
		                        </div>
		                <?php endforeach; ?>
		                </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                <?php echo Form::label('phone', trans('main.phone')); ?>

		                <?php echo Form::text('phone', isset($obj)? $obj->phone : $user->phone,
		                              ['required',
		                               'class'       => 'form-control',
		                               'id'         => 'phone',
		                               'placeholder' => trans('main.phone').' (XXXXXXXXX)']); ?>


		            </div>
		        </div>
		      </div>

		      <div class="row">
		        <div class="col-md-12">
		          <div class="form-group" id="map" style="height:300px;"></div>
		        </div>
		      </div>
		    </div>


		    <div class="row image-upload">
		      <div class="col-md-12 upload-file-area">
		      <div class="alert alert-warning" role="alert"> <strong><?php echo e(trans('main.attention')); ?></strong>
		       <?php echo e(trans('main.image-size')); ?>, <?php echo e(trans('main.image-quantity')); ?></div>
		      <div class="upload-file">
		      <?php if(isset($obj)): ?>
		      	  <?php foreach($obj->images as $i): ?>
					<?php if($obj->default_image_id == $i->id): ?>
				      <article class="img-thumbnail main-image">
				     	  <a title="წაშლა" data-id="<?php echo e($i->id); ?>" data-obj-id="<?php echo e($obj->id); ?>" class="ajax-delete"><i class="fa fa-trash"></i></a>
				          <img class="thumbnail" src="/uploads/applications/<?php echo e($obj->id); ?>/2_<?php echo e($i->src); ?>">
				
				      </article>
				    <?php endif; ?>  
		      	  <?php endforeach; ?>
			      <?php foreach($obj->images as $i): ?>
			      	<?php if($obj->default_image_id != $i->id): ?>
				      <article class="img-thumbnail">
				     	  <a title="<?php echo e(trans('main.delete')); ?>" data-id="<?php echo e($i->id); ?>" data-obj-id="<?php echo e($obj->id); ?>" class="ajax-delete"><i class="fa fa-trash"></i></a>
				     	  <a class="add-as-main-image" title="<?php echo e(trans('main.image-size')); ?>"><?php echo e(trans('main.main-image-add')); ?></a>
				          <img class="add-image prewimg" src="/uploads/applications/<?php echo e($obj->id); ?>/2_<?php echo e($i->src); ?>">
				
				      </article>
				    <?php endif; ?>  
				  <?php endforeach; ?>
			  <?php endif; ?>	
		      	<article class="img-thumbnail upload-file-class" id="uploadd">
		      	    <input name="photo[]"  type="file" id="files" multiple/>
		      	    <img class="add-image prewimg" alt="+" src="/assets/site/images/plus-sign.jpg">

		      	</article>

		      </div>

		      

		    </div>


		    <button  class="btn btn-success submit-form" type="submit">
		    <?php if(Request::segment(4) == 'edit'): ?>
				<?php echo e(trans('main.edit')); ?>

		    <?php else: ?>
				<?php echo e(trans('main.add')); ?>

		    <?php endif; ?>
		    </button>
			
		</div>
	</div>

</div>
</select>

<?php if(isset($obj)): ?>
  <input type="hidden" name="latitude" id="latitude" value="<?php echo e($obj->latitude); ?>">
  <input type="hidden" name="longitude" id="longitude" value="<?php echo e($obj->longitude); ?>">
  <input type="hidden" name="default_image_id" id="default-image-id" value="<?php echo e($obj->default_image_id); ?>">
<?php else: ?>
  <input type="hidden" name="latitude" id="latitude" value="41.704510">
  <input type="hidden" name="longitude" id="longitude" value="44.809120">
  <input type="hidden" name="default_image_id" id="default-image-id" value="">
<?php endif; ?>
<input type="hidden" id="appl-id" value="">

<input type="hidden" id="token" name="_token" value="<?php echo csrf_token(); ?>">

	<?php echo Form::close(); ?>


<script type="text/javascript" src="/assets/js/admin-map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgP8HLE8PuUTobmMq25uRGetbbTqPgW6A&callback=initMap" async defer></script>
<script src="http://dgiurad.ge/assets/site/js/validation.min.js" type="text/javascript"></script>

<script type="text/javascript">


$(document).ready(function() {


	
	$('#city-id').change(function() {
		id = $(this).val();
		token = $('#token').val();
		$.ajax({
          	url: "<?php echo e(route('get.districts.for.city.id')); ?>",
          	type: 'POST',
          	data: {id: id, _token: token},
          	success: function(data){
          		$('#district-id').html(data);
          		//$('#district-id').show();
          		if (data[0] == null){
          			data = '<option selected value=""></option>';
          			$('#district-id').html(data);
          			//$('#district-id').hide();
          		} 
            	
        	}
        });
	});
	$('.upload-file').on('click', '.add-as-main-image', function(event) {
		article = $(this).closest('article');
		this_data = $(this).siblings('.ajax-delete');
		app_id = this_data.attr('data-obj-id');
		img_id = this_data.attr('data-id');

		token = $('#token').val();
		$.ajax({
          	url: "<?php echo e(route('edit.default.image')); ?>",
          	type: 'POST',
          	data: {app_id: app_id, img_id: img_id, _token: token},
          	success: function(data){
            	if(data == 1){
            		$('.img-thumbnail').removeClass('main-image');
            		article.addClass('main-image');
            		$('#default-image-id').val(img_id);
            	}
            	else console.log(data);
        	}
        })
	});

	
	$('#post-form').validator().on('submit', function (e) {
		
		if (e.isDefaultPrevented()) {
		    $("html, body").animate({ scrollTop: $('.first-part').offset().top-100 }, 200);
		} else {
			if($('#latitude').val() == 41.704510){
			    alert("<?php echo e(trans('main.please-add-location')); ?>");
			    $("html, body").animate({ scrollTop: $('#map').offset().top-100 }, 200);
			    return false;
			}
		}
	})
	$('.upload-file').on('click', '.add-image', function(event) {
		event.preventDefault();
		$(this).closest('article').find('input[type=file]').click();
	});

	$('.upload-file').on('click', '.delete', function(event) {
		$(this).closest('article').remove();
	});

	$('.upload-file').on('click', '.ajax-delete', function(event) {
		id = $(this).attr('data-id');
		obj_id = $(this).attr('data-obj-id');
		this_ = $(this);

		$.ajax({
			url: "<?php echo e(route('site.delete.image')); ?>",
			type: 'post',
			data: {id: id, _token: $('#token').val(),obj_id: obj_id },
			success: function(data){
				if(data == 1){
					this_.closest('article').remove();
				}
				else if(data == 2){
					alert("<?php echo e(trans('main.please-change-main-image')); ?>");
				}
				else{
					alert(':(')
				}
			}
		})
	

	
});

// 	window.onload = function(){
        

// if(window.File && window.FileList && window.FileReader)
//     {
//         $('#files').on("change", function(event) {
//             var files = event.target.files; //FileList object
//             var output = document.getElementById("result");

//             for (var i = 0, f; f = files[i]; i++)
//             {
//                 var file = files[i];
//                 //Only pics
//                 // if(!file.type.match('image'))
//                 if(file.type.match('image.*')){
//                     if(this.files[0].size < 2097152){    
//                   // continue;
//                     var picReader = new FileReader();
//                     picReader.addEventListener("load",function(event){
//                         var picFile = event.target;
//                         var div = document.createElement("div");
//                         div.innerHTML = '<article class="img-thumbnail">'+'<input name="photo[]" onchange="previewFile($(this))" id="files" type="file" />'+"<img class='thumbnail' src='" + picFile.result + "'" +
//                                 "title='preview image'/>"+'<a title="<?php echo e(trans('main.delete')); ?>" class="delete"><i class="fa fa-trash"></i></a>'+'</article>';
//                         output.insertBefore(div,null);            
//                     });
//                     //Read the image
//                     $('#clear, #result').show();
//                     picReader.readAsDataURL(file);
//                     }else{
//                         alert("Image Size is too big. Minimum size is 2MB.");
//                         $(this).val("");
//                     }
//                 }else{
//                 alert("You can only upload image file.");
//                 $(this).val("");
//             }
//             }                               
           
//         });
//     }
//     else
//     {
//         console.log("Your browser does not support File API");
//     }
	
// }



    //Check File API support
    window.onload = function(){

    if(window.File && window.FileList && window.FileReader)
    {


        $('.upload-file').on('change', '#files', function(event) {

            var files = event.target.files; //FileList object\
            var html= "";

            $(".upload-file-class").hide();
            
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];
                
                //Only pics
                if(!file.type.match('image'))
                  continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener("load",function(event){
                    
                    var picFile = event.target;
                    
                    
                    $('.upload-file').append( '<article class="img-thumbnail" id="upload" >'+'<input name="photo[]" type="file" id="files" />'+"<img class='thumbnail' src='" + picFile.result + "'" +
                                "title='preview image'/>"+'<a title="<?php echo e(trans('main.delete')); ?>" class="delete"><i class="fa fa-trash"></i></a>'+'</article>');
                        
                    
                                
                
                });
                
                 //Read the image
                picReader.readAsDataURL(file);
            }


            last = '<article class="img-thumbnail upload-file-class" id="uploadd">'+
                                    '<input name="photo[]"  type="file" id="files" multiple/>'+
                                    '<img class="add-image prewimg" id="upimg" alt="+" src="/assets/site/images/plus-sign.jpg">'+
                                '</article>';

            $(last).appendTo('.upload-file');

            console.log(last);

           
           
        });
    }
    else
    { 
        console.log("Your browser does not support File API");
    }

}



    });

 
   
    






 

</script>



 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.site', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>