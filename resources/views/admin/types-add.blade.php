@extends('layouts.admin')
@section('content')
    <div class="col-md-10 main-content-admin">
        <ol class="breadcrumb">
          <li><a href="{{route('admin.type.show')}}">ტიპები</a></li>
          <li class="active">
	          @if(Request::segment(3) == 'add')
	          	დამატება
	          @else
	          	რედაქტირება
	          @endif
          </li>
        </ol>
        <div class="row">
            <div class="col-md-6">
                @if(!empty($obj))
                    {!! Form::model($obj,['method' => 'post']) !!}
                @else
                    {!! Form::open(['method' => 'post']) !!}
                @endif 
                <div class="form-group">
                    <label for="title-geo">სათაური [GEO]</label>
                    {!! Form::text('title_geo', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-geo',
                              'placeholder'=>'ქართული სათაური')) !!}
                </div>
                <div class="form-group">
                    <label for="title-eng">სათაური [ENG]</label>
                    {!! Form::text('title_eng', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-eng',
                              'placeholder'=>'ინგლისური სათაური')) !!}
                </div>
                <div class="form-group">
                    <label for="title-rus">სათაური [RUS]</label>
                    {!! Form::text('title_rus', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-rus',
                              'placeholder'=>'რუსული სათაური')) !!}
                </div>
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <button type="submit" class="btn btn-primary">შენახვა</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop