@extends('layouts.admin')
@section('content')
    <div class="col-md-10 main-content-admin">
        <ol class="breadcrumb">
          <li><a href="{{route('admin.users.show')}}">მომხმარებლები</a></li>
          <li class="active">
	          @if(Request::segment(3) == 'add')
	          	დამატება
	          @else
	          	რედაქტირება
	          @endif
          </li>
        </ol>
        <div class="row">
            <div class="col-md-6">
                @if(!empty($obj))
                    {!! Form::model($obj,['method' => 'post']) !!}
                @else
                    {!! Form::open(['method' => 'post']) !!}
                @endif
                <div class="row"> 
                  <div class="col-md-6 form-group">
                      <label for="first-name">სახელი</label>
                      {!! Form::text('first_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'first-name',
                                'placeholder'=>'სახელი')) !!}
                  </div>
                  <div class="col-md-6 form-group">
                      <label for="last-name">გვარი</label>
                      {!! Form::text('last_name', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'last-name',
                                'placeholder'=>'გვარი')) !!}
                  </div>
                </div>
                <div class="row user-email-parent">  
                  <div class="col-md-6 form-group user-email">
                      <label for="email">ელ. ფოსტა</label>
                      {!! Form::text('email', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'email',
                                'placeholder'=>'ელ. ფოსტა')) !!}
                  </div>
                  @if(empty($obj))
                    <div class="col-md-6 form-group">
                        <label for="password">პაროლი</label>
                        {!! Form::password('password',
                            array('required',
                                  'class'=>'form-control',
                                   'id' => 'password',
                                  'placeholder'=>'მინ. 8 სიმბოლო')) !!}
                    </div>
                  @else
                    <div class="col-md-6 form-group">
                      <label for="password">პაროლი</label>
                      {!! Form::password('password',
                          array('disabled',
                                'class'=>'form-control',
                                 'id' => 'password',
                                'placeholder'=>'მინ. 8 სიმბოლო')) !!}
                    </div>
                  @endif
                </div>
                <div class="row">
                  <div class="col-md-6 form-group">
                      <label for="phone">ტელეფონი</label>
                      {!! Form::text('phone', null,
                          array('required',
                                'class'=>'form-control',
                                 'id' => 'phone',
                                'placeholder'=>'xxx xx xx xx')) !!}
                  </div>
                  <div class="col-md-6 form-group">
                      <label for="timepicker1">დაბადების თარიღი</label>
                      <div class="input-group bootstrap-timepicker timepicker">
                          {!! Form::text('birth_date',null,[
                              'required',
                              'class'=>'form-control input-small',
                              'id' => 'timepicker1',
                              'placeholder'=>'წელი - თვე - რიცხვი (1993-08-05)'
                          ]) !!}
                          <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                  </div>
                </div>  
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <button type="submit" class="btn btn-primary">შენახვა</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $('#timepicker1').datepicker({
          format: "yyyy-mm-dd"
        });
    </script>
@stop