@extends('layouts.admin')
@section('content')
<div class="col-md-10">
	<h2>განცხადებები</h2>
	<a href="{{route('admin.applications.add')}}"><button class="btn btn-primary add-button">დამატება</button></a>
	<table class="table table-striped">
		<tr>
			<th>სურათი</th>
			<th>უნიკალური ID</th>
			<th>სათაური</th>
			<th>მომხმარებელი</th>
			<th>მოქმედება</th>
		</tr>
		@foreach ($applications as $m)
			<tr>
				<td style="padding-top: 13px;">
					@if($m->defaultImage)
						<img src='/uploads/applications/{{$m->id}}/2_{{$m->defaultImage->src}}' style="height:40px;"/>
					@else
						<img src='/uploads/applications/default.jpg' style="height:40px;"/>
					@endif
				</td>
				<td style="padding-top: 13px;">{{$m->unique_id}}</td>
				<td style="padding-top: 13px;">{{$m->title}}</td>
				<td style="padding-top: 13px;">
					@if($m->user->type == 0)
						<a href="{{route('admin.users.detailed.show', $m->user->unique_id)}}">
					@else
						<a href="{{route('admin.inc-users.detailed.show', $m->user->unique_id)}}">	
					@endif	
					{{$m->user->email}}</a>
				</td>
				<td class="actions">
					<a href="{{route('admin.applications.edit', $m->id)}}"><button style="width: 49%" class="btn btn-warning edit-button">ჩასწორება</button></a>
					<button style="width: 49%" data-url="{{route('admin.applications.delete', $m->id)}}" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
				</td>
			</tr>
		@endforeach
	</table>
</div>
@stop