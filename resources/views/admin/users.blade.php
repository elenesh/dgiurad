@extends('layouts.admin')
@section('content')
<div class="col-md-10">
	<h2>მომხმარებლები</h2>
	<a href="{{route('admin.users.add')}}"><button class="btn btn-primary add-button">დამატება</button></a>
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>უნიკალური ID</th>
			<th>სახელი</th>
			<th>გვარი</th>
			<th>ელ. ფოსტა</th>
			<th>ტელეფონი</th>
			<th>მოქმედება</th>
		</tr>
		@foreach ($users as $m)
			<tr>
				<td style="padding-top: 13px;">{{$m->id}}</td>
				<td style="padding-top: 13px;"><a href="{{route('admin.users.detailed.show', $m->unique_id)}}">{{$m->unique_id}}</a></td>
				<td style="padding-top: 13px;">{{$m->first_name}}</td>
				<td style="padding-top: 13px;">{{$m->last_name}}</td>
				<td style="padding-top: 13px;">{{$m->email}}</td>
				<td style="padding-top: 13px;">{{$m->phone}}</td>
				<td class="actions">
					<a href="{{route('admin.users.edit', $m->id)}}"><button style="width: 49%" class="btn btn-warning edit-button">ჩასწორება</button></a>
					{{--@if($m->blocked == 0)--}}
						{{--<a href="{{route('admin.users.block', $m->id)}}"><button style="width: 32%" class="btn btn-danger block-button">დაბლოკვა</button></a>--}}
					{{--@else--}}
						{{--<a href="{{route('admin.users.unblock', $m->id)}}"><button style="width: 32%" class="btn btn-primary block-button">განბლოკვა</button></a>--}}
					{{--@endif--}}
					<button style="width: 49%" data-url="{{route('admin.users.delete', $m->id)}}" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
				</td>
			</tr>
		@endforeach
	</table>
</div>
@stop