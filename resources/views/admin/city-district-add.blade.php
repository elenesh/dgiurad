@extends('layouts.admin')
@section('content')
    <div class="col-md-10 main-content-admin">
        <ol class="breadcrumb">
          <li><a href="{{route('admin.city-district.show')}}">ქალაქები / უბნები</a></li>
          <li class="active">
	          @if(Request::segment(3) == 'add')
	          	დამატება
	          @else
	          	რედაქტირება
	          @endif
          </li>
        </ol>
        <div class="row">
            <div class="col-md-6">
            	<?php 
        		if(Request::input('isDistrict')){
        			if(empty($obj)){
        				$action = route('admin.city-district.storeDistrict');
        			}else{
        				$action = route('admin.city-district.district.update', $obj->id);
        			}
        		}else{
        			if(empty($obj)){
        				$action = route('admin.city-district.store');
        			}else{
        				$action = route('admin.city-district.city.update', $obj->id);
        			}
        		}
            	?>
                @if(!empty($obj))
                    {!! Form::model($obj,['method' => 'post', 'url' => $action]) !!}
                @else
                    {!! Form::open(['method' => 'post', 'url' => $action]) !!}
                @endif
                @if(Request::input('isDistrict'))
	                <div class="form-group">
	                    <label for="title-geo">ქალაქი</label>
	                    {!! Form::select('city_id', $cities, null, [
	                    			'class' => 'form-control'
	                    ])!!}
	                </div> 
                @endif   
                <div class="form-group">
                    <label for="title-geo">სათაური [GEO]</label>
                    {!! Form::text('title_geo', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-geo',
                              'placeholder'=>'ქართული სათაური')) !!}
                </div>
                <div class="form-group">
                    <label for="title-eng">სათაური [ENG]</label>
                    {!! Form::text('title_eng', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-eng',
                              'placeholder'=>'ინგლისური სათაური')) !!}
                </div>
                <div class="form-group">
                    <label for="title-rus">სათაური [RUS]</label>
                    {!! Form::text('title_rus', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-rus',
                              'placeholder'=>'რუსული სათაური')) !!}
                </div>
                @if(!Request::input('isDistrict'))
	                <div class="form-group">
	                    <label for="latitude">გრძედი</label>
	                    {!! Form::text('latitude', null,
	                        array('class'=>'form-control',
	                               'id' => 'latitude',
	                              'placeholder'=>'კოორდინატი: გრძედი')) !!}
	                </div>
	                <div class="form-group">
	                    <label for="longitude">განედი</label>
	                    {!! Form::text('longitude', null,
	                        array('class'=>'form-control',
	                               'id' => 'longitude',
	                              'placeholder'=>'კოორდინატი: განედი')) !!}
               		</div>
               	@endif

                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <button type="submit" class="btn btn-primary">შენახვა</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop