@extends('layouts.admin')
@section('content')
    <div class="col-md-10 main-content-admin">
     @if(Request::segment(3) == 'add')
      <h2>განცხადების დამატება</h2>
    @else
      <h2>განცხადების რედაქტირება ID - {{$obj->unique_id}}</h2>
    @endif
        <ol class="breadcrumb">
          <li><a href="{{route('admin.applications.show')}}">განცხადებები</a></li>
          <li class="active">
	          @if(Request::segment(3) == 'add')
	          	დამატება
	          @else
	          	რედაქტირება
	          @endif
          </li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                @if(!empty($obj))
                    {!! Form::model($obj,['method' => 'post', 'files' => true, 'id' => 'post-form']) !!}
                @else
                    {!! Form::open(['method' => 'post', 'files' => true, 'id' => 'post-form']) !!}
                @endif
                <div class="first-part">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('user-id', 'მომხმარებელი') !!}
                        {!! Form::select('user_id', $users, !empty($obj) ? $obj->user_id: null, ['class' => 'form-control', 'id' => 'user-id']) !!}
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('city-id', 'ქალაქი') !!}
                        {!! Form::select('city_id', $cities, !empty($obj) ? $obj->city_id: null, ['class' => 'form-control', 'id' => 'city-id']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('title', 'სათაური') !!}
                        {!! Form::text('title', null,
                                    ['required',
                                    'class'       => 'form-control',
                                     'id'         => 'title',
                                    'placeholder' => 'სათაური']) !!}
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('district-id', 'უბანი') !!}
                        {!! Form::select('district_id', $districts, !empty($obj) ? $obj->district_id: null, ['class' => 'form-control', 'id' => 'district-id']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('description', 'აღწერა') !!}
                        {!! Form::textarea('description', null,
                                    ['required',
                                    'class'       => 'form-control',
                                    'id'          => 'description',
                                    'placeholder' => 'აღწერა',
                                    'style'       => 'height:185px;']) !!}                      
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('street-addr', 'მისამართი') !!}
                        {!! Form::text('street_addr', null,
                                    ['required',
                                    'class'       => 'form-control',
                                     'id'         => 'street-addr',
                                    'placeholder' => 'მისამართი']) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::label('m2', 'კვადრატულობა (m2)') !!}
                        {!! Form::input('number','m2', null,
                                    ['required',
                                    'class'       => 'form-control',
                                     'id'         => 'm2',
                                    'placeholder' => 'კვადრატულობა (m2)']) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::label('room-quantity', 'ოთახების რაოდენობა') !!}
                        <?php 
                        $rooms = [
                          '1' => '1', 
                          '2' => '2', 
                          '3' => '3', 
                          '4' => '4', 
                          '5' => '5', 
                          '6' => '6', 
                          '7' => '7', 
                          '8' => '8', 
                          '9' => '9', 
                          '10' => '10', 
                          '11' => '11', 
                          '12' => '12'
                        ]; ?>
                        {!! Form::select('room_quantity', $rooms, !empty($obj) ? $obj->room_quantity: null, ['class' => 'form-control', 'id' => 'room-quantity']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('num-of-days', 'ვადა (დღე)') !!}
                        @if(isset($obj))
                          <select name="num_of_days" id="num-of-days" class="form-control" disabled>
                        @else
                          <select name="num_of_days" id="num-of-days" class="form-control">
                        @endif
                          <option value="7">7</option>
                          <option value="15">15</option>
                          <option value="30">30</option>
                          <option value="60">60</option>
                          <option value="90">90</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('floor', 'სართული') !!}
                        {!! Form::input('number','floor', null,
                                    ['required',
                                    'class'       => 'form-control',
                                     'id'         => 'floor',
                                    'placeholder' => 'სართული']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('type-id', 'ტიპი') !!}
                        {!! Form::select('type_id', $types, !empty($obj) ? $obj->type_id: null, ['class' => 'form-control', 'id' => 'type-id']) !!}
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('condition-id', 'მდგომარეობა') !!}
                        {!! Form::select('condition_id', $conditions, !empty($obj) ? $obj->condition_id: null, ['class' => 'form-control', 'id' => 'condition-id']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row" style="margin-bottom: 20px;">
                    <div class="col-md-5">
                      {!! Form::label('daily-price', 'დღიური თანხა') !!}
                      {!! Form::text('daily_price', null,
                                    ['class'       => 'form-control',
                                     'id'         => 'daily-price',
                                    'placeholder' => 'დღიური თანხა']) !!}
                    </div>
                    <div class="col-md-5">
                      {!! Form::label('hourly-price', 'საათობრივი თანხა') !!}
                      {!! Form::text('hourly_price', null,
                                    ['class'       => 'form-control',
                                     'id'         => 'hourly-price',
                                    'placeholder' => 'საათობრივი თანხა']) !!}
                    </div>
                    <div class="col-md-2">
                      {!! Form::label('currency', 'ვალუტა') !!}
                      <?php 
                        $currencies = [
                          'GEL' => 'GEL',
                          'USD' => 'USD'
                        ];
                      ?>
                      {!! Form::select('currency', $currencies, !empty($obj) ? $obj->currency: null, ['class' => 'form-control', 'id' => 'currency']) !!}
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="attributes">ატრიბუტები</label>
                            <div id="attributes" style="border: 1px solid #D4D4D4; border-radius: 5px; padding: 0 0 0 15px ;">
                            @foreach($attributes as $a)
                                <?php $i=0; ?>
                                <div class="row">
                                    <div class="col-md-12">
                                    @if(!empty($obj))
                                        @foreach($obj->attributes as $oa)
                                            @if($oa->id == $a->id)
                                                    <?php $i=1; ?>
                                            @endif
                                        @endforeach
                                        @if($i == 1)
                                            <input type="checkbox" value="{{$a->id}}" name="attributes[]" id="attribute-{{$a->id}}" checked>
                                            <label for="attribute-{{$a->id}}">{{$a->title_geo}}</label>
                                        @else
                                            <input type="checkbox" value="{{$a->id}}" name="attributes[]" id="attribute-{{$a->id}}">
                                            <label for="attribute-{{$a->id}}">{{$a->title_geo}}</label>
                                        @endif
                                    @else
                                        <input type="checkbox" value="{{$a->id}}" name="attributes[]" id="attribute-{{$a->id}}">
                                        <label for="attribute-{{$a->id}}">{{$a->title_geo}}</label>
                                    @endif
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('phone', 'ტელეფონი') !!}
                            {!! Form::text('phone', null,
                                          ['class'       => 'form-control',
                                           'id'         => 'phone',
                                          'placeholder' => 'ტელეფონის ნომერი (XXXXXXXXX)']) !!}

                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group" id="map" style="height:300px;"></div>
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-bottom: 20px;">
                  @if(!empty($obj))
                    @foreach($obj->images as $o)
                      <div class="col-xs-6 col-md-3">
                        <a href="/uploads/applications/{{$obj->id}}/2_{{$o->src}}" class="thumbnail appl-img">
                          <img src="/uploads/applications/{{$obj->id}}/2_{{$o->src}}" style="height:100px;">
                        </a>
                        @if($o->id == $obj->default_image_id)
                          <button type="button" data-id="{{$o->id}}" class="btn btn-success imgs" style="width: 100%">მთავარი</button>
                        @else
                          <button type="button" data-id="{{$o->id}}" class="btn btn-primary imgs" style="width: 100%">მთავარზე დაყენება</button>
                        @endif
                          <a href="{{route('admin.applications.image.from.edit.delete', $o->id)}}"><button class="btn btn-danger" style="width: 100%">წაშლა</button> </a>
                      </div>
                    @endforeach
                  @endif
                </div>
                <div class="row image-upload">
                  <div class="col-md-12">
                    <input id="input-1" name="image" multiple="true" type="file"  data-preview-file-type="text">
                  </div>
                </div>
                <div class="first-part">
                  @if(isset($obj))
                    <input type="hidden" name="latitude" id="latitude" value="{{$obj->latitude}}">
                    <input type="hidden" name="longitude" id="longitude" value="{{$obj->longitude}}">
                  @else
                    <input type="hidden" name="latitude" id="latitude" value="41.704510">
                    <input type="hidden" name="longitude" id="longitude" value="44.809120">
                  @endif
                  <input type="hidden" name="default_image_id" id="default-image-id" value="">
                  <input type="hidden" id="appl-id" value="">
                  <input type="hidden" id="token" name="_token" value="{!! csrf_token() !!}">
                  <div class="form-group"  style="margin-top: 15px;">
                    <button type="submit" class="btn btn-primary">
                      @if(isset($obj))
                        შენახვა
                      @else
                        შემდეგი &#8594; 
                      @endif
                    </button>
                  </div>
                </div>
                {!! Form::close() !!}
                <div class="row complete-button">
                  <div class="form-group" style="margin:15px;">
                    <a href="{{route('admin.applications.show')}}"><button class="btn btn-primary">დასრულება</button></a>
                  </div>
                </div>
                <img src="/assets/loading.gif" alt="loading.gif" style="display:none;">
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/assets/js/admin-map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgP8HLE8PuUTobmMq25uRGetbbTqPgW6A&callback=initMap" async defer></script>
    <script>

          var applID = "{{!empty($obj)? $obj->id : 'null'}}";;
          function makeUploader(id){
            $("#input-1").addClass('file')
            var url = "{{route('admin.applications.image.upload')}}";
            $("#input-1").fileinput({
              dataType: 'json',
              previewFileType: 'any',
              showUpload: false,
              showRemove: false,
              uploadAsync: true,
              minFileCount: 1,
              maxFileCount: 8,
              acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
              uploadUrl: url,
              uploadExtraData: {
                '_token': $('#token').val(),
                'id': id
              },
              deleteExtraData: {
                '_token': $('#token').val()
              },
              previewMaxWidth: 100,
              previewMaxHeight: 100
            }).on("filebatchselected", function(event, files) {
                $("#input-1").fileinput("upload");
            });

            $('.file-preview-thumbnails').on('click', '.set-profile', function(event) {
              addDefaultImage($(this).attr('data-id'));
              $('.set-profile').removeClass('btn-success');
              $('.set-profile').html('მთავარზე დაყენება');
              $(this).html('მთავარი');
              $(this).addClass('btn-success');
              if(id == 0){
                $('.complete-button').show();
              }
            });
          }

          $('.imgs').on('click', function(){
            addDefaultImage($(this).attr('data-id'));
            $('.imgs').removeClass('btn-success');
            $('.imgs').addClass('btn-primary');
            $('.imgs').html('მთავარზე დაყენება');
            $(this).html('მთავარი');
            $(this).addClass('btn-success');
          });

          function addDefaultImage(id){
            $.ajax({
              url: '{{route("admin.applications.default.image.add")}}',
              type: 'POST',
              data: {id: id, '_token': $('#token').val(), 'appl_id': applID},
            })
            .done(function() {
              console.log("success");
            })
            .fail(function() {
              console.log("error");
            });
          }


          $('#post-form').submit(function(event){
            event.preventDefault();
            $.ajax({
              url: $('#post-form').attr('action'),
              type: 'POST',
              data: $('#post-form').serialize(),
            })
            .done(function(data) {
              if(data.success){
                if(edit == 0){
                  applID = data.applicationId;
                  makeUploader(data.applicationId);
                  $('.first-part').hide();
                  $('.image-upload').show(1000);
                }else{
                  window.location.href = "{{route('admin.applications.show')}}";
                }
              }
            })
            .fail(function() {
              console.log("error");
            });
            
          });
          $('.complete-button').hide();
          var edit = "{{!empty($obj)? $obj->id : '0'}}";
          if(edit == 0){
            $('.image-upload').hide();
          }else{
            makeUploader(edit);
          }
    </script>
@stop