@extends('layouts.admin')
@section('content')
<div class="col-md-10">
	<h2>ტიპები</h2>
	<a href="{{route('admin.type.add')}}"><button class="btn btn-primary add-button">დამატება</button></a>
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>სახელი [GEO]</th>
			<th>სახელი [ENG]</th>
			<th>სახელი [RUS]</th>
			<th>მოქმედება</th>
		</tr>
		@foreach ($types as $m)
			<tr>
				<td style="padding-top: 13px;">{{$m->id}}</td>
				<td style="padding-top: 13px;">{{$m->title_geo}}</td>
				<td style="padding-top: 13px;">{{$m->title_eng}}</td>
				<td style="padding-top: 13px;">{{$m->title_rus}}</td>
				<td class="actions">
					<a href="{{route('admin.type.edit', $m->id)}}"><button class="btn btn-warning edit-button">ჩასწორება</button></a>
					<button  data-url="{{route('admin.type.delete', $m->id)}}" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
				</td>
			</tr>
		@endforeach
	</table>
</div>
@stop