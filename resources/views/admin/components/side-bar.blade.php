<div class="col-md-2 side-menu-col">
	<ul class="nav nav-pills nav-stacked side-menu">
		<li role="presentation" class="{{ Request::segment(2) == 'index' ? 'active' : ''}}"><a href="{{route('admin.index')}}">მთავარი</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'types' ? 'active' : ''}}"><a href="{{route('admin.type.show')}}">ტიპები</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'city-district' ? 'active' : ''}}"><a href="{{route('admin.city-district.show')}}">ქალაქები / უბნები</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'attributes' ? 'active' : ''}}"><a href="{{route('admin.attributes.show')}}">მახასიათებლები</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'conditions' ? 'active' : ''}}"><a href="{{route('admin.condition.show')}}">მდგომარეობა</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'users' ? 'active' : ''}}"><a href="{{route('admin.users.show')}}">მომხმარებლები</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'inc-users' ? 'active' : ''}}"><a href="{{route('admin.inc-users.show')}}">იურიდიული პირები</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'applications' ? 'active' : ''}}"><a href="{{route('admin.applications.show')}}">განცხადებები</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'footer-links' ? 'active' : ''}}"><a href="{{route('admin.footer-links.show')}}">Footer Links</a></li>
		<li role="presentation" class="{{ Request::segment(2) == 'payments' ? 'active' : ''}}"><a href="{{route('admin.payments.show')}}">ანგარიშსწორება</a></li>
	</ul>
</div>