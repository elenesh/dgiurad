@extends('layouts.admin')
@section('content')
    <div class="col-md-10 main-content-admin">
        <ol class="breadcrumb">
            <li><a href="{{route('admin.footer-links.show')}}">Footer Links</a></li>
            <li class="active">
                @if(Request::segment(3) == 'add')
                    დამატება
                @else
                    რედაქტირება
                @endif
            </li>
        </ol>
        <div class="row">
            @if(!empty($obj))
                {!! Form::model($obj,['method' => 'post']) !!}
            @else
                {!! Form::open(['method' => 'post']) !!}
            @endif
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('city-id', 'ქალაქი') !!}
                    {!! Form::select('city_id', $cities, !empty($obj) ? $obj->city_id: null, ['class' => 'form-control', 'id' => 'city-id']) !!}
                </div>
                <div class="form-group">
                    <label for="title-geo">სათაური [GEO]</label>
                    {!! Form::text('title_geo', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-geo',
                              'placeholder'=>'ქართული სათაური')) !!}
                </div>
                <div class="form-group">
                    <label for="title-eng">სათაური [ENG]</label>
                    {!! Form::text('title_eng', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-eng',
                              'placeholder'=>'ინგლისური სათაური')) !!}
                </div>
                <div class="form-group">
                    <label for="title-rus">სათაური [RUS]</label>
                    {!! Form::text('title_rus', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'title-rus',
                              'placeholder'=>'რუსული სათაური')) !!}
                </div>
                <div class="form-group">
                    <label for="slug">მისამართი</label>
                    {!! Form::text('slug', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'slug',
                              'placeholder'=>'binebi-tbilisshi')) !!}
                </div>
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <button type="submit" class="btn btn-primary">შენახვა</button>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="slug">აღწერა [GEO]</label>
                    {!! Form::textarea('description_geo', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'description_geo',
                              'style'=>'height:83px;')) !!}
                </div>
                <div class="form-group">
                    <label for="description_eng">აღწერა [ENG]</label>
                    {!! Form::textarea('description_eng', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'description_eng',
                              'style'=>'height:83px;')) !!}
                </div>
                <div class="form-group">
                    <label for="description_rus">აღწერა [RUS]</label>
                    {!! Form::textarea('description_rus', null,
                        array('required',
                              'class'=>'form-control',
                               'id' => 'description_rus',
                              'style'=>'height:83px;')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop