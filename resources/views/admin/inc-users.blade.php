@extends('layouts.admin')
@section('content')
<div class="col-md-10">
	<h2>იურიდიული პირები</h2>
	<a href="{{route('admin.inc-users.add')}}"><button class="btn btn-primary add-button">დამატება</button></a>
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>უნიკალური ID</th>
			<th>კომპანიის სახელი</th>
			<th>ელ. ფოსტა</th>
			<th>ტელეფონი</th>
			<th>საიდენტიფიკაციო #</th>
			<th>მოქმედება</th>
		</tr>
		@foreach ($incUsers as $m)
			<tr>
				<td style="padding-top: 13px;">{{$m->id}}</td>
				<td style="padding-top: 13px;"><a href="{{route('admin.inc-users.detailed.show', $m->unique_id)}}">{{$m->unique_id}}</a></td>
				<td style="padding-top: 13px;">{{$m->incorporated_name}}</td>
				<td style="padding-top: 13px;">{{$m->email}}</td>
				<td style="padding-top: 13px;">{{$m->phone}}</td>
				<td style="padding-top: 13px;">{{$m->incorporated_id}}</td>
				<td class="actions">
					<a href="{{route('admin.inc-users.edit', $m->id)}}"><button style="width: 49%" class="btn btn-warning edit-button">ჩასწორება</button></a>
					{{--@if($m->blocked == 0)--}}
						{{--<a href="{{route('admin.inc-users.block', $m->id)}}"><button style="width: 32%" class="btn btn-danger block-button">დაბლოკვა</button></a>--}}
					{{--@else--}}
						{{--<a href="{{route('admin.inc-users.unblock', $m->id)}}"><button style="width: 32%" class="btn btn-primary block-button">განბლოკვა</button></a>--}}
					{{--@endif--}}
					<button style="width: 49%" data-url="{{route('admin.inc-users.delete', $m->id)}}" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-danger delete-button">წაშლა</button>
				</td>
			</tr>
		@endforeach
	</table>
</div>
@stop