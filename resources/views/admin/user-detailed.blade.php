@extends('layouts.admin')
@section('content')
<div class="col-md-10 main-content-admin">
	<ol class="breadcrumb">
		@if(Request::segment(2) == "users")	
      		<li><a href="{{route('admin.users.show')}}">მომხმარებლები</a></li>
      	@else
      		<li><a href="{{route('admin.inc-users.show')}}">იურიდიული პირები</a></li>	
      	@endif	
      	<li class="active">{{Request::segment(4)}}</li>
    </ol>
	<div class="row">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">მომხმარებლის მონაცემები</div>
				<ul class="list-group">
					<li class="list-group-item"><strong>ID:</strong> {{$user->id}}</li>
					<li class="list-group-item"><strong>უნიკალური ID:</strong> {{$user->unique_id}}</li>
					@if($user->type == 1)
					<li class="list-group-item"><strong>კომპანიის სახელი:</strong> {{$user->incorporated_name}}</li>
					@endif
					<li class="list-group-item"><strong>სახელი:</strong> {{$user->first_name}}</li>
					<li class="list-group-item"><strong>გვარი:</strong> {{$user->last_name}}</li>
					@if($user->type == 0)
					<li class="list-group-item"><strong>დაბადების თარიღი:</strong> {{$user->birth_date}}</li>
					@endif
					<li class="list-group-item"><strong>ელ-ფოსტა:</strong> {{$user->email}}</li>
					<li class="list-group-item"><strong>ტელეფონი:</strong> {{$user->phone}}</li>
					<li class="list-group-item"><strong>ბალანსი:</strong><span style="color:green;"> {{$user->balance}}<span></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8">
			<h2>განცხადებები</h2>
			<table class="table table-striped">
				<tr>
					<th>#</th>
					<th>უნიკალური ID</th>
					<th>სათაური</th>
					<th>განცხადების ტიპი</th>
				</tr>
				@foreach($user->applications as $p)
				<tr>
					<td>{{$p->id}}</td>
					<td><a href="#">{{$p->unique_id}}</a></td>
					<td>{{$p->title}}</td>
					<td>
						@if($p->rank == 0)
							ჩვეულებრივი
						@elseif($p->rank == 1)
							VIP
						@elseif($p->rank == 2)
							SUPER VIP
						@endif			
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8">
			<h2>ანგარიშსწორება</h2>
			<table class="table table-striped">
				<tr>
					<th>#</th>
					<th>თარიღი</th>
					<th>გადახდის ტიპი</th>
					<th>ოდენობა</th>
				</tr>
				@foreach($user->payments as $p)
				<tr>
					<td>{{$p->id}}</td>
					<td>{{$p->created_at}}</td>
					<td>{{$p->type}}</td>
					<td>
						@if($p->amount > 0)
							<span style="color:green;">+{{$p->amount}}</span>
						@else
							<span style="color:red;">{{$p->amount}}</span>
						@endif
					</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@stop