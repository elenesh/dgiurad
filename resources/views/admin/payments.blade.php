@extends('layouts.admin')
@section('content')
<div class="col-md-10">
	<h2>ანგარიშსწორება</h2>
	<table class="table table-striped">
		<tr>
			<th>#</th>
			<th>მომხმარებელი</th>
			<th>ოდენობა</th>
			<th>გადახდის საშუალება</th>
			<th>თარიღი</th>
		</tr>
		@foreach ($payments as $m)
			<tr>
				<td style="padding-top: 13px;">{{$m->id}}</td>
				<td style="padding-top: 13px;">
					@if($m->user->type == 0)
						<a href="{{route('admin.users.detailed.show', $m->user->unique_id)}}">
					@else
						<a href="{{route('admin.inc-users.detailed.show', $m->user->unique_id)}}">	
					@endif	
					{{$m->user->email}}</a>
				</td>
				<td style="padding-top: 13px;">
					@if($m->amount > 0)
						<span style="color:green;">+{{$m->amount}}</span>
					@else
						<span style="color:red;">{{$m->amount}}</span>
					@endif	
				</td>
				<td style="padding-top: 13px;">{{$m->type}}</td>
				<td style="padding-top: 13px;">{{$m->created_at}}</td>
			</tr>
		@endforeach
	</table>
</div>
@stop