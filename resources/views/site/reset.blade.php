@extends ('layouts.site')

@section ('content')
	@include('site.components.heading')

	<div class="container reg-login">
		<div class="col-md-12">
			<div class="bs-callout bs-callout-info" id="callout-navs-tabs-plugin">
				@if(!isset($_i))
				<p>{{trans('main.enter-your-email')}}</p>
				@else
				<p>{{trans('main.enter-your-new-pass')}}</p>
				@endif
			</div>
		</div>
		
		<div class="col-md-6">
			@if(!isset($_i))

			{!! Form::open(['method' => 'post', 'id' => 'pass-reset']) !!}

	    	<div class="form-group tab-2">
	    		<label for="email-inp">{{trans('main.email')}}</label>
	    		<input id="email-inp" type="email" name="email" class="form-control" placeholder="{{trans('main.email')}}" required>
	    	</div>

	    	<div class="form-group pull-right">
	    		<button class="btn btn-success pull-right" type="submit">{{trans('main.send')}}</button>
	    	</div>


	    	</form>

	    	@else


	    	{!! Form::open(['method' => 'post', 'id' => 'pass-reset-cont']) !!}

    		<div class="form-group">
    			<label for="">{{trans('main.new-pass')}}</label>
    			<input type="password" name="password" class="form-control" placeholder="{{trans('main.new-pass')}}" required="">
    		</div>

    		<div class="form-group">
    			<label for="">{{trans('main.conf-new-pass')}}</label>
    			<input type="password" name="password_confirmation" class="form-control" placeholder="{{trans('main.conf-new-pass')}}" required="">
    		</div>

	    	<div class="form-group  pull-right">
	    		<button class="btn btn-success pull-right" type="submit">{{trans('main.send')}}</button>
	    	</div>


	    	</form>

	    	@endif

		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="modal1Label"></h4>
	      </div>
	      <div class="modal-body"></div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('main.close')}}</button>
	      </div>
	    </div>
	  </div>
	</div>

	<script src="http://dgiurad.ge/assets/site/js/validation.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(function() {
			
			$('#pass-reset-cont').validator().on('submit', function (e) { 
				if (e.isDefaultPrevented()) {
				  // handle the invalid form...
				} else {
				  // everything looks good!
				  url = $('#pass-reset-cont').attr('action');
				  formData = $(this).serialize();
				  e.preventDefault();
				  $.ajax({
				  	url: url,
				  	type: 'POST',
				  	data: formData,
				  	success: function(data){
				  		switch(data['status']) {
				  			case 0:
								$('input[name=password],input[name=password_confirmation]').val('');
								$('#callout-navs-tabs-plugin').html(data['msg'][0]);
				  				break;
				  			case 1:
				  				
				  				$('#pass-reset-cont').slideUp("fast", function() {
				  					$('#form-reset').remove();
				  					$('#callout-navs-tabs-plugin').html(data['msg'][0])
				  				});
				  				break;
				  		}

				  		//console.log(data);
				  	}
				  })
				  
				}
			});


			$('#pass-reset').validator().on('submit', function (e) { 
				if (e.isDefaultPrevented()) {
				  // handle the invalid form...
				} else {
				  // everything looks good!
				  formData = $(this).serialize();
				  e.preventDefault();
				  $.ajax({
				  	url: "{{route('reset.post')}}",
				  	type: 'POST',
				  	data: formData,
				  	success: function(data){
				  		switch(data['status']) {
				  			case 0:
								$('#email-inp').val('');
								$('#callout-navs-tabs-plugin').html("{{trans('main.user-not-exist')}}");
				  				break;
				  			case 1:
				  				
				  				$('#pass-reset').slideUp("fast", function() {
				  					$('#form-reset').remove();
				  					$('#callout-navs-tabs-plugin').html("{{trans('main.check-email')}}")
				  				});
				  				break;
				  		}

				  		//console.log(data);
				  	}
				  })
				  
				}
			});

		});
	</script>

	@stop