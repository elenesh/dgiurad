
@extends ('layouts.site')

@section ('content')

@include('site.components.search')

<body>

@if(\App::getLocale() == 'ka')	  
	<div class="container reg-login" style="background: #fff;color: #565a5c;padding: 25px 5px; padding-bottom: 40px;margin: 30px auto;min-height: 410px;">
      <div class="col-md-12 both">
      <ol>
		<li>	როგორ დავამატო განცხადება?
		- განცხადების დასამატებლად, პირველ რიგში აუცილებელია გაიაროთ რეგისტრაცია.

		</li>
		<li>	ფასიანია თუ არა განცხადების დამატება?
		- რეგისტრაციაც და განცხადების დამატებაც არის სრულიად უფასო.
		- ფასიანია მხოლოდ VIP და Super VIP კატეგორიის განცხადებები.

		</li>
		<li>	რამდენი განცხადების დამატება შემიძლია?
		- ერთ მომხმარებელს (მნიშვნელობა არ აქვს ფიზიკურს, თუ იურიდიულ პირს) შეუძლია დაამატოს შეუზღუდავი რაოდენობის განცხადებები სრულიად უფასოდ.
		</li>
		<li>	როგორ დავრეგისტრირდე?
		- რეგისტრაციისათვის შეგიძლიათ დააჭიროთ შემდეგ ღილაკებს: 1) „ბინის დამატება“; 2) „შედით ანგარიშზე“; ან გადადით <a href="http://dgiurad.ge/ka/login">ბმულზე</a>

		</li>
		<li>	რეგისტრაციისას რომელი ვარიანტი ავირჩიო „ფიზიკური პირი“ თუ „იურიდიული პირი“?
		- თუ ხართ სააგენტო, ან რომელიმე კომპანიის წარმომადგენელი და კომპანიის სახელით აპირებთ განცხადების განთავსებას, მაშინ რეგისტრაციისას უნდა აირჩიოთ „იურიდიული პირი“ და ასევე შესაბამის ველში მიუთითოთ კომპანიის 9 ნიშნა საიდენთიფიკაციო კოდი.
		- ყველა სხვა შემთხვევაში შეგიძლიათ დარეგისტრირდეთ როგორც „ფიზიკური პირი“.
		</li>

		<li>როგორ დავამატო განცხადება VIP_ში და Super VIP-ში?
		- იმისათვის, რომ თქვენი განცხადება იყოს VIP ან Super VIP კატეგორიაში, ამისათვის პირველ რიგში საჭიროა გქონდეთ ერთი განცხადება მაინც და ბალანსზე გქონდეთ საკმარისი თანხა და მხოლოდ შემდგომ შეძლებთ განცხადების VIP და Super VIP კატეგორიაში გადაყვანას.

		</li>

		<li>	როგორ შევავსო ბალანსი?
		- ბალანსის შევსებისთვის დააჭირეთ ღილაკს „ბალანსის შევსება“, მიუთითეთ სასურველი რაოდენობის თანხა და გადაიხადეთ პლასტიკური ბარათის საშუალებით.
		- ბალანსის შევსება კიდევ შეგიძლიათ “Express Pay” და “TBC Pay”-ის სწრაფი გადახდის აპარატებიდან. მოძებნეთ DGIURAD.GE მიუთითეთ თქვენი უნიკალური კოდი (ID), რომელიც გენიჭებათ საიტზე რეგისტრაციისას და შეგიძლიათ იხილოთ თქვენ ანგარიშში პირად მონაცემებთან ერთად, და გადაიხადეთ სასურველი ოდენობის თანხა.

		</li>
		</ol>
	   </div>
	</div>


	   @elseif (\App::getLocale() == 'en')

	   <div class="container reg-login" style="background: #fff;color: #565a5c;padding: 25px 5px; padding-bottom: 40px;margin: 30px auto;min-height: 410px;">
      <div class="col-md-12 both">
      <ol>	

	        <li>	How to add an Ad?
- You must register first in order to be able to add ads.

</li>
			<li>		Is it chargeable to add ads?
- It is totally free to register and to add ads also.
- You only must pay to make your ads VIP or Super VIP.


</li>
			<li>	How many ads can I add?
- One user (does not matter “Individual Person” or “Legal Entity”) can add unlimited ads for free.

</li>
			<li>	How to Register?
- For registration you can click the following buttons: 1) “Add New”; 2) “Login in Account”; or just click the following<a href="http://dgiurad.ge/en/login"> link: </a>

</li>
			 <li>	Which option should I choose while registration: “Individual Person” or “Legal Entity”?
- If you are an Agency or represent a company then you must choose “Legal Entity” and also fill in the TAX Code of your company.
- In other cases you can register as “Individual Person”.

</li>

			 <li>	How to make Ads VIP or Super VIP?
- First of all you have to have minimum one Ad and enough money on your virtual account in order to make your Ads VIP or Super VIP.


</li>

			 <li>	How to put money on my virtual account?
- You can charge your account by clicking on the following button: “Charge Account”, indicate the amount of money you would like to transfer on your account and pay with your Credit/Debit Card.
- Second option to charge your account are “Express Pay” and “TBC Pay” machines. Find DGIURAD.GE fill in your ID (which generates automatically when you register, and you can find it in your profile) and pay the amount of money you would like to transfer on your account.


</li>
</ol>
	        	
	   </div>
	   </div>

	   @elseif(\App::getLocale() == 'ru')

	   <div class="container reg-login" style="background: #fff;color: #565a5c;padding: 25px 5px; padding-bottom: 40px;margin: 30px auto;min-height: 410px;">
      <div class="col-md-12 both">
      <ol>	

	        <li>	Как добавить объявление?
Чтобы добавить объявление, вы должны обязательно пройти регистрацию на сайте


</li>
			<li>	Должен ли я заплатить, чтобы разместить объявления?
- Регистрация и добавление объявлений совершенно бесплатно.
 -Вы должны заплатить только в том случаи, если хотите сделать VIP объявления или супер VIP объявлени


</li>
			<li>	Сколько объявлений могу я добавить?
- Один пользователь (не имеет значения "Физическое лицо" или "юридическое лицо") может бесплатно добавить неограниченное количество объявления.

</li>
			<li>	Как  пройти регистрацию на сайте? 
- Для регистрации вы можете нажать следующие кнопки: 1) "Добавить объявление"; 2) "Войти в аккаунт”; или просто нажмите следующую <a href="http://dgiurad.ge/ru/user-area">ссылку</a>


</li>
			 <li>	Какой вариант я должен выбрать при регистрации: "Физическое лицо" или "Юридическое лицо"?
- Если вы агентство или представляете компанию, то вы должны выбрать "Юридическое лицо", а также заполнить "Идентификационный код" вашей компании.
- В других случаях вы можете зарегистрироваться как "Физическое лицо".


</li>

			 <li>	Как сделать VIP или Супер VIP объявлении?
- Прежде всего нужно иметь минимум одно объявления  и достаточно денег на вашем виртуальном счете для того, чтобы ваше объявления/объявлении стали VIP или Супер VIP



</li>

			 <li>Как зачислить деньги на моем виртуальном счете?
- Вы можете заполнить свой счет, нажав на кнопку: " Заполнить Баланс ", указав сумму денег, которую вы хотели бы перевести на свой счет и произвести оплату с помощью кредитной / дебетовой карты.
- Второй вариант, чтобы  заполнить свой счет,  в аппаратах "Express Pay" и "TBC Pay" нужно найти DGIURAD.GE  указать  свой уникалний код (который генерируется автоматически при регистрации, также  вы можете найти его в вашем профиле) и оплатить сумму, которую вы хотели бы перевести на свой счет.



</li>
</ol>
	        	
	   </div>
	   </div>


	   @endif


	   
	  
</body>
@stop