@extends ('layouts.site')

@section ('content')
    @include('site.components.user-area-heading')

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 status">
            <h1 style="color:green; text-align: center;">თანხა წარმატებით ჩაირიცხა</h1>
        </div>
        <div class="col-md-4"></div>
    </div>
@stop