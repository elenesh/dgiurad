@extends ('layouts.site')

@section ('content')
@include('site.components.user-area-heading')
<?php 
	$auth = auth()->guard('siteUsers');
    $user = $auth->user(); 
?>
<style>
	article
{
    width: 80%;
    margin:auto;
    margin-top:10px;
}


.thumbnail{

    height: 100px;
    margin: 10px;    
}
.delete {
	margin-top:-35px;
}






</style>
<section class="main">
	<div class="container application-page">
		<div class="rows" style="margin-left: 20px; margin-right: 20px;">
			@if(!empty($obj))
		        {!! Form::model($obj,['method' => 'post', 'files' => true, 'id' => 'post-form']) !!}
		    @else
		        {!! Form::open(['method' => 'post', 'files' => true, 'id' => 'post-form']) !!}
		    @endif

		    

		    <div class="first-part">
		      <div class="row">
				  <div class="col-md-12">

			  		    	<ul class="clear nav nav-tabs language">
			                	<li class="active"><a data-toggle="tab" href="#ge_1">{{trans('main.title')}} (ka) <img src="/assets/site/images/ka.png"></a></li>
			                	<li class=""><a data-toggle="tab" href="#en_1">{{trans('main.title')}} (en) <img src="/assets/site/images/en.png"></a></li>
			                	<li><a data-toggle="tab" href="#ru_1"> {{trans('main.title')}} (ru) <img src="/assets/site/images/ru.png"></a></li>
			            	</ul>

			            	<div class="tab-content">
			  	            <div class="form-group tab-pane active" id="ge_1">
			  	              {!! Form::text('title_geo', null,
                                      ['required',
                                      'class'       => 'form-control',
                                      'placeholder' => trans("main.title").' '.trans('main.in-georgian'),
                                      'data-lan' => 'ka',
                                      'maxlength' =>'50']) !!}
			  	            </div>
			  	            <div class="form-group tab-pane" id="en_1">
			  	              {!! Form::text('title_eng', null,
                                      [
                                      'class'       => 'form-control',
                                      'placeholder' => trans("main.title").' '.trans('main.in-english'),
                                      'data-lan' => 'en',
                                      'maxlength' =>'50']) !!}
			  	            </div>
			  	            <div class="form-group tab-pane" id="ru_1">
			  	              {!! Form::text('title_rus', null,
                                      [
                                      'class'       => 'form-control',
                                      'placeholder' => trans("main.title").' '.trans('main.in-russian'),
                                      'data-lan' => 'ru',
                                      'maxlength' =>'50']) !!}
			  	            </div>
			            	</div>
				  </div>
		      </div>
		      <div class="row">
				  <div class="col-md-6">
					  <div class="form-group">
						  {!! Form::label('city-id', trans('main.city')) !!}
						  {!! Form::select('city_id', $cities, !empty($obj) ? $obj->city_id: null, ['class' => 'form-control', 'id' => 'city-id']) !!}
					  </div>
				  </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            {!! Form::label('district-id', trans('main.district')) !!}
		            {!! Form::select('district_id', $districts, !empty($obj) ? $obj->district_id: null, ['class' => 'form-control', 'id' => 'district-id']) !!}
		          </div>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-6">
	    		    <ul class="clear nav nav-tabs language">
	                  	<li class="active"><a data-toggle="tab" href="#ge_2">{{trans('main.description')}} (ka) <img src="/assets/site/images/ka.png"></a></li>
	                  	<li class=""><a data-toggle="tab" href="#en_2">{{trans('main.description')}} (en) <img src="/assets/site/images/en.png"></a></li>
	                  	<li><a data-toggle="tab" href="#ru_2">{{trans('main.description')}} (ru) <img src="/assets/site/images/ru.png"></a></li>
	              	</ul>

	              	<div class="tab-content">
			  	        <div class="form-group tab-pane active" id="ge_2">
		  	              {!! Form::textarea('description_geo', null,
		                        ['required',
		                        'class'       => 'form-control',
		                        'id'          => 'description',
		                        'data-lan' => 'ka',
		                        'placeholder' => trans('main.description').' '.trans('main.in-georgian'),
		                        'style'       => 'height:185px;',
	                            'maxlength' =>'3000']) !!}  
			  	        </div>

			  	        <div class="form-group tab-pane" id="en_2">
		  	              {!! Form::textarea('description_eng', null,
		                        [
		                        'class'       => 'form-control',
		                        'id'          => 'description',
		                        'data-lan' => 'en',
		                        'placeholder' => trans('main.description').' '.trans('main.in-english'),
		                        'style'       => 'height:185px;',
	                            'maxlength' =>'3000']) !!}  
			  	        </div>

    		  	        <div class="form-group tab-pane" id="ru_2">
    	  	              {!! Form::textarea('description_rus', null,
    	                        [
    	                        'class'       => 'form-control',
    	                        'id'          => 'description',
    	                        'data-lan' => 'ru',
    	                        'placeholder' => trans('main.description').' '.trans('main.in-russian'),
    	                        'style'       => 'height:185px;',
                                'maxlength' =>'3000']) !!}  
    		  	        </div> 
	              	</div>
		          	
		        </div>
		        <div class="col-md-6">
    			    <ul class="clear nav nav-tabs language">
    	              	<li class="active"><a data-toggle="tab" href="#ge_3">{{trans('main.street-addr')}} (ka) <img src="/assets/site/images/ka.png"></a></li>
    	              	<li class=""><a data-toggle="tab" href="#en_3">{{trans('main.street-addr')}} (en) <img src="/assets/site/images/en.png"></a></li>
    	              	<li><a data-toggle="tab" href="#ru_3">{{trans('main.street-addr')}} (ru) <img src="/assets/site/images/ru.png"></a></li>
    	          	</ul>
    	          	<div class="tab-content">
			  	        <div class="form-group tab-pane active" id="ge_3">
				            {!! Form::text('street_addr_geo', null,
				                        ['required',
				                        'class'       => 'form-control',
				                         'id'         => 'street-addr',
				              
				                         'data-lan' => 'ka',
				                        'placeholder' => trans('main.street-addr').' '.trans('main.in-georgian'),
		                                      'maxlength' =>'100']) !!}
		          		</div>
  			  	        <div class="form-group tab-pane " id="en_3">
  				            {!! Form::text('street_addr_rus', null,
  				                        [
  				                        'class'       => 'form-control',
  				                         'id'         => 'street-addr',
  				                         'data-lan' => 'en',
  				                        'placeholder' => trans('main.street-addr').' '.trans('main.in-english'),
  		                                      'maxlength' =>'100']) !!}
  		          		</div>
  			  	        <div class="form-group tab-pane " id="ru_3">
  				            {!! Form::text('street_addr_eng', null,
  				                        [
  				                        'class'       => 'form-control',
  				                         'id'         => 'street-addr',
  				                         'data-lan' => 'ru',
  				                        'placeholder' => trans('main.street-addr').' '.trans('main.in-russian'),
  		                                      'maxlength' =>'100']) !!}
  		          		</div>
		          	</div>
		          <div class="form-group">
		            {!! Form::label('m2', trans('main.m2').' (m2)') !!}
		            {!! Form::input('number','m2', null,
		                        ['required',
		                        'class'       => 'form-control',
		                         'id'         => 'm2',
		                        'placeholder' => trans('main.m2').' (m2)',
		                        'min' => '0']) !!}
		          </div>
		          <div class="form-group">
		            {!! Form::label('room-quantity', trans('main.room-quantity')) !!}
		            <?php 
		            $rooms = [
		              '1' => '1', 
		              '2' => '2', 
		              '3' => '3', 
		              '4' => '4', 
		              '5' => '5', 
		              '6' => '6', 
		              '7' => '7', 
		              '8' => '8', 
		              '9' => '9', 
		              '10' => '10', 
		              '11' => '11', 
		              '12' => '12'
		            ]; ?>
		            {!! Form::select('room_quantity', $rooms, !empty($obj) ? $obj->room_quantity: null, ['class' => 'form-control', 'id' => 'room-quantity']) !!}
		          </div>
		        </div>
		      </div>

		      <div class="row">

			      <div class="col-md-6">
			        <div class="form-group">
			          {!! Form::label('bathroom_quantity', trans('main.bathroom')) !!}
			          {!! Form::input('number','bathroom_quantity', null,
			                      [
			                      'id' => 'bathroom_quantity',
			                      'class'       => 'form-control',
			                      'placeholder' => trans('main.bathroom'),
			                      'min' => '0']) !!}
			        </div>
			      </div>

			      <div class="col-md-6">
			        <div class="form-group">
			          {!! Form::label('bedroom_quantity', trans('main.bedroom')) !!}
			          {!! Form::input('number','bedroom_quantity', null,
			                      [
			                      'id' => 'badroom_quantity',
			                      'class'       => 'form-control',
			                      'placeholder' => trans('main.bedroom'),
			                      'min' => '0']) !!}
			        </div>
			      </div>

		      
		        <div class="col-md-6">
		          <div class="form-group">
		            {!! Form::label('num-of-days', trans('main.validity-day')) !!}
		            @if(isset($obj))
		              <select name="num_of_days" id="num-of-days" class="form-control" disabled>
		            @else
		              <select name="num_of_days" id="num-of-days" class="form-control">
		            @endif
		              <option value="7">7</option>
		              <option value="15">15</option>
		              <option value="30">30</option>
		              <option value="60">60</option>
		              <option value="90">90</option>
		            </select>
		          </div>
		        </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            {!! Form::label('floor', trans('main.floor')) !!}
		            {!! Form::input('number','floor', null,
		                        [
		                        'class'       => 'form-control',
		                         'id'         => 'floor',
		                        'placeholder' => trans('main.floor'),
		                        'min' => '-2']) !!}
		          </div>
		        </div>
		      </div>
		      <div class="row">
		        <div class="col-md-6">
		          <div class="form-group">
		            {!! Form::label('type-id', trans('main.type')) !!}
		            {!! Form::select('type_id', $types, !empty($obj) ? $obj->type_id: null, ['class' => 'form-control', 'id' => 'type-id']) !!}
		          </div>
		        </div>
		        <div class="col-md-6">
		          <div class="form-group">
		            {!! Form::label('condition-id', trans('main.condition')) !!}
		            {!! Form::select('condition_id', $conditions, !empty($obj) ? $obj->condition_id: null, ['class' => 'form-control', 'id' => 'condition-id']) !!}
		          </div>
		        </div>
		      </div>
		      <div class="row" style="margin-bottom: 20px;">
		        <div class="col-md-5">
		          {!! Form::label('daily-price', trans('main.daily-price')) !!}
		          {!! Form::text('daily_price', null,
		                        ['class'       => 'form-control',
		                         'id'         => 'daily-price',
		                        'placeholder' => trans('main.daily-price')]) !!}
		        </div>
		        <div class="col-md-5">
		          {!! Form::label('hourly-price', trans('main.hourly-price')) !!}
		          {!! Form::text('hourly_price', null,
		                        ['class'       => 'form-control',
		                         'id'         => 'hourly-price',
		                        'placeholder' => trans('main.hourly-price')]) !!}
		        </div>
		        <div class="col-md-2">
		          {!! Form::label('currency', trans('main.currency')) !!}
		          <?php 
		            $currencies = [
		              'USD' => 'USD',
					  'GEL' => 'GEL'
		            ];
		          ?>
		          {!! Form::select('currency', $currencies, !empty($obj) ? $obj->currency: null, ['class' => 'form-control', 'id' => 'currency']) !!}
		        </div>
		      </div>
		      <div class="form-group">
		        <div class="row">
		            <div class="col-md-8">
		                <label for="attributes">{{trans('main.attributes')}}</label>
		                <div id="attributes" style="border: 1px solid #D4D4D4; border-radius: 5px; padding: 0 0 0 15px ;">
		                <div class="row">
		                @foreach($attributes as $a)
		                    <?php $i=0; ?>
		                        <div class="col-md-4">
		                        @if(!empty($obj))
		                            @foreach($obj->attributes as $oa)
		                                @if($oa->id == $a->id)
		                                        <?php $i=1; ?>
		                                @endif
		                            @endforeach
		                            @if($i == 1)
		                                <input type="checkbox" value="{{$a->id}}" name="attributes[]" id="attribute-{{$a->id}}" checked>
		                                <label for="attribute-{{$a->id}}">{{L10nHelper::get($a)}}</label>
		                            @else
		                                <input type="checkbox" value="{{$a->id}}" name="attributes[]" id="attribute-{{$a->id}}">
		                                <label for="attribute-{{$a->id}}">{{L10nHelper::get($a)}}</label>
		                            @endif
		                        @else
		                            <input type="checkbox" value="{{$a->id}}" name="attributes[]" id="attribute-{{$a->id}}">
		                            <label for="attribute-{{$a->id}}">{{L10nHelper::get($a)}}</label>
		                        @endif
		                        </div>
		                @endforeach
		                </div>
		                </div>
		            </div>
		            <div class="col-md-4">
		                {!! Form::label('phone', trans('main.phone')) !!}
		                {!! Form::text('phone', isset($obj)? $obj->phone : $user->phone,
		                              ['required',
		                               'class'       => 'form-control',
		                               'id'         => 'phone',
		                               'placeholder' => trans('main.phone').' (XXXXXXXXX)']) !!}

		            </div>
		        </div>
		      </div>

		      <div class="row">
		        <div class="col-md-12">
		          <div class="form-group" id="map" style="height:300px;"></div>
		        </div>
		      </div>
		    </div>


		    <div class="row image-upload">
		      <div class="col-md-12 upload-file-area">
		      <div class="alert alert-warning" role="alert"> <strong>{{trans('main.attention')}}</strong>
		       {{trans('main.image-size')}}, {{trans('main.image-quantity')}}</div>
		      <div class="upload-file">
		      @if(isset($obj))
		      	  @foreach($obj->images as $i)
					@if($obj->default_image_id == $i->id)
				      <article class="img-thumbnail main-image">
				     	  <a title="წაშლა" data-id="{{ $i->id }}" data-obj-id="{{$obj->id}}" class="ajax-delete"><i class="fa fa-trash"></i></a>
				          <img class="thumbnail" src="/uploads/applications/{{$obj->id}}/2_{{ $i->src }}">
				
				      </article>
				    @endif  
		      	  @endforeach
			      @foreach ($obj->images as $i)
			      	@if($obj->default_image_id != $i->id)
				      <article class="img-thumbnail">
				     	  <a title="{{trans('main.delete')}}" data-id="{{ $i->id }}" data-obj-id="{{$obj->id}}" class="ajax-delete"><i class="fa fa-trash"></i></a>
				     	  <a class="add-as-main-image" title="{{trans('main.image-size')}}">{{trans('main.main-image-add')}}</a>
				          <img class="add-image prewimg" src="/uploads/applications/{{$obj->id}}/2_{{ $i->src }}">
				
				      </article>
				    @endif  
				  @endforeach
			  @endif	
		      	<article class="img-thumbnail upload-file-class" id="uploadd">
		      	    <input name="photo[]"  type="file" id="files" multiple/>
		      	    <img class="add-image prewimg" alt="+" src="/assets/site/images/plus-sign.jpg">

		      	</article>

		      </div>

		      

		    </div>


		    <button  class="btn btn-success submit-form" type="submit">
		    @if(Request::segment(4) == 'edit')
				{{trans('main.edit')}}
		    @else
				{{trans('main.add')}}
		    @endif
		    </button>
			
		</div>
	</div>

</div>
</select>

@if(isset($obj))
  <input type="hidden" name="latitude" id="latitude" value="{{$obj->latitude}}">
  <input type="hidden" name="longitude" id="longitude" value="{{$obj->longitude}}">
  <input type="hidden" name="default_image_id" id="default-image-id" value="{{$obj->default_image_id}}">
@else
  <input type="hidden" name="latitude" id="latitude" value="41.704510">
  <input type="hidden" name="longitude" id="longitude" value="44.809120">
  <input type="hidden" name="default_image_id" id="default-image-id" value="">
@endif
<input type="hidden" id="appl-id" value="">

<input type="hidden" id="token" name="_token" value="{!! csrf_token() !!}">

	{!! Form::close() !!}

<script type="text/javascript" src="/assets/js/admin-map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgP8HLE8PuUTobmMq25uRGetbbTqPgW6A&callback=initMap" async defer></script>
<script src="http://dgiurad.ge/assets/site/js/validation.min.js" type="text/javascript"></script>

<script type="text/javascript">


$(document).ready(function() {


	
	$('#city-id').change(function() {
		id = $(this).val();
		token = $('#token').val();
		$.ajax({
          	url: "{{route('get.districts.for.city.id')}}",
          	type: 'POST',
          	data: {id: id, _token: token},
          	success: function(data){
          		$('#district-id').html(data);
          		//$('#district-id').show();
          		if (data[0] == null){
          			data = '<option selected value=""></option>';
          			$('#district-id').html(data);
          			//$('#district-id').hide();
          		} 
            	
        	}
        });
	});
	$('.upload-file').on('click', '.add-as-main-image', function(event) {
		article = $(this).closest('article');
		this_data = $(this).siblings('.ajax-delete');
		app_id = this_data.attr('data-obj-id');
		img_id = this_data.attr('data-id');

		token = $('#token').val();
		$.ajax({
          	url: "{{route('edit.default.image')}}",
          	type: 'POST',
          	data: {app_id: app_id, img_id: img_id, _token: token},
          	success: function(data){
            	if(data == 1){
            		$('.img-thumbnail').removeClass('main-image');
            		article.addClass('main-image');
            		$('#default-image-id').val(img_id);
            	}
            	else console.log(data);
        	}
        })
	});

	
	$('#post-form').validator().on('submit', function (e) {
		
		if (e.isDefaultPrevented()) {
		    $("html, body").animate({ scrollTop: $('.first-part').offset().top-100 }, 200);
		} else {
			if($('#latitude').val() == 41.704510){
			    alert("{{trans('main.please-add-location')}}");
			    $("html, body").animate({ scrollTop: $('#map').offset().top-100 }, 200);
			    return false;
			}
		}
	})
	$('.upload-file').on('click', '.add-image', function(event) {
		event.preventDefault();
		$(this).closest('article').find('input[type=file]').click();
	});

	$('.upload-file').on('click', '.delete', function(event) {
		$(this).closest('article').remove();
	});

	$('.upload-file').on('click', '.ajax-delete', function(event) {
		id = $(this).attr('data-id');
		obj_id = $(this).attr('data-obj-id');
		this_ = $(this);

		$.ajax({
			url: "{{ route('site.delete.image') }}",
			type: 'post',
			data: {id: id, _token: $('#token').val(),obj_id: obj_id },
			success: function(data){
				if(data == 1){
					this_.closest('article').remove();
				}
				else if(data == 2){
					alert("{{trans('main.please-change-main-image')}}");
				}
				else{
					alert(':(')
				}
			}
		})
	

	
});

// 	window.onload = function(){
        

// if(window.File && window.FileList && window.FileReader)
//     {
//         $('#files').on("change", function(event) {
//             var files = event.target.files; //FileList object
//             var output = document.getElementById("result");

//             for (var i = 0, f; f = files[i]; i++)
//             {
//                 var file = files[i];
//                 //Only pics
//                 // if(!file.type.match('image'))
//                 if(file.type.match('image.*')){
//                     if(this.files[0].size < 2097152){    
//                   // continue;
//                     var picReader = new FileReader();
//                     picReader.addEventListener("load",function(event){
//                         var picFile = event.target;
//                         var div = document.createElement("div");
//                         div.innerHTML = '<article class="img-thumbnail">'+'<input name="photo[]" onchange="previewFile($(this))" id="files" type="file" />'+"<img class='thumbnail' src='" + picFile.result + "'" +
//                                 "title='preview image'/>"+'<a title="{{trans('main.delete')}}" class="delete"><i class="fa fa-trash"></i></a>'+'</article>';
//                         output.insertBefore(div,null);            
//                     });
//                     //Read the image
//                     $('#clear, #result').show();
//                     picReader.readAsDataURL(file);
//                     }else{
//                         alert("Image Size is too big. Minimum size is 2MB.");
//                         $(this).val("");
//                     }
//                 }else{
//                 alert("You can only upload image file.");
//                 $(this).val("");
//             }
//             }                               
           
//         });
//     }
//     else
//     {
//         console.log("Your browser does not support File API");
//     }
	
// }



    //Check File API support
    window.onload = function(){

    if(window.File && window.FileList && window.FileReader)
    {


        $('.upload-file').on('change', '#files', function(event) {

            var files = event.target.files; //FileList object\
            var html= "";

            $(".upload-file-class").hide();
            
            for(var i = 0; i< files.length; i++)
            {
                var file = files[i];
                
                //Only pics
                if(!file.type.match('image'))
                  continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener("load",function(event){
                    
                    var picFile = event.target;
                    
                    
                    $('.upload-file').append( '<article class="img-thumbnail" id="upload" >'+'<input name="photo[]" type="file" id="files" />'+"<img class='thumbnail' src='" + picFile.result + "'" +
                                "title='preview image'/>"+'<a title="{{trans('main.delete')}}" class="delete"><i class="fa fa-trash"></i></a>'+'</article>');
                        
                    
                                
                
                });
                
                 //Read the image
                picReader.readAsDataURL(file);
            }


            last = '<article class="img-thumbnail upload-file-class" id="uploadd">'+
                                    '<input name="photo[]"  type="file" id="files" multiple/>'+
                                    '<img class="add-image prewimg" id="upimg" alt="+" src="/assets/site/images/plus-sign.jpg">'+
                                '</article>';

            $(last).appendTo('.upload-file');

            console.log(last);

           
           
        });
    }
    else
    { 
        console.log("Your browser does not support File API");
    }

}



    });

 
   
    






 

</script>



 
@stop