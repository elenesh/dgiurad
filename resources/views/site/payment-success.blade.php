@extends ('layouts.site')

@section ('content')
    @include('site.components.user-area-heading')

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 status">
            <h1 style="text-align: center;">მუშავდება</h1>
            <h3 style="color:red; text-align:center;">გთხოვთ დაელოდოთ</h3>
            <img src="{{url('assets/site/loading.gif')}}" style="height:125px;margin:auto;display:block;"/>
        </div>
        <div class="col-md-4"></div>
    </div>
    <script type="application/javascript">
        gettransStatus();
        function gettransStatus() {
            var token = '{{ csrf_token() }}';
            $.ajax({
                url: "{{route('payment.check')}}",
                type:"POST",
                data: { '_token': token, 'tid': '{{$_COOKIE['trans-id']}}' },
                success:function(data){
                    if(data == 'Rejected'){
                        $('.status').html('<h1 style="text-align:center;">შეცდომა!</h1><h3 style="color:red; text-align: center;">გადახდა ვერ განხორციელდა</h3>');
                    }else if(data == 'OK'){
                        document.cookie = "trans-id=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
                        $('.status').html('<h3 style="color:green; text-align: center;">თანხა წარმატებით ჩაირიცხა</h3>');
                        setTimeout(function(){
                            window.location.href = '{{route('payment.success')}}';
                        }, 3000);
                    }else if(data == 'Waiting'){
                        gettransStatus();
                    }
                },error:function(){
                    gettransStatus();
                }
            });
    }
    </script>
@stop