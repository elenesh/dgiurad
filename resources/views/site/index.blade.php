@extends ('layouts.site')

@section ('content')
	@include('site.components.search')

	<style>
	  .lSPager.lSpg {
	  	    margin-top: -17px!important;
	  }	
	  .caption {
	  	bottom: 18px!important;
	  }

	  @media only screen and (max-width: 985px) {
  
	  .block-items a { float:none!important;}
 
  
}
.lSpg {
	display:none;
}
	</style>

	

	<div id="getAplications" ng-controller="AplicationsCtrl"  data-ng-init="getAplications('1')">

		<section class="main">
			<div class="container">
				<div class="row" >
					<div class="col-md-12">
						<h5 class="app-rank-header">{{trans('main.super-vip-apps')}}</h5>
					</div>
					
					<div class="block-items style-1">

                     
						@foreach($superVIP as $a)
                       
                       
                 {{--    <ul id ="adaptive">
				            @foreach($a->images as $i) 
				            <li>  
				             <img src= "/uploads/applications/{{$a->id}}/2_{{$i->src}}">
				             </li>
							@endforeach	
							</ul> --}}
							
							
							
                            

                            
                         <a href="{{ route('single', [$a->slug, $a->unique_id]) }}" class="col-md-4 item {{$a->app_type}}">
								<div class="preview-img rank">

								<ul class ="responsive">

							 @foreach($a->images as $i)
                            <li>
							
									<img alt="house-1" src="/uploads/applications/{{$a->id}}/2_{{$i->src}}">
								
							</li>
							@endforeach	
							</ul>
						
							
									<div class="caption">
										{!!Helpers::getPrice($a)!!}
									</div>
								</div>


								

                                
								


								<div class="info-block">
									<ul>
										<li>{{$a->m2}} {{trans('main.m')}}<sup>2</sup></li>
										<li>{{$a->room_quantity}} {{trans('main.room')}}</li>
										<li>{{L10nHelper::get($a->type)}}</li>
									</ul>
									<div class="clearfix"></div>

									<h4>{{L10nHelper::get($a->city)}}{{isset($a->district->title_geo) ? ", ".L10nHelper::get($a->district) : ''}}</h4>
								</div>
							</a>


						
@endforeach	

						

						<div class="clearfix"></div>
					</div>
					<!--// block-items style-1 -->
				</div>



				<div class="banner-style-1">
					<a href="http://sesxebi.ge/" rel="nofollow" target="_blank">
						<img alt="sesxebi.ge" src="/assets/site/images/banners/sesxebi-ge.png">
					</a>
					<a class="ban-caption">{{trans('main.ad')}}</a>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h5 class="app-rank-header">{{trans('main.vip-apps')}}</h5>
					</div>
					<div class="block-items style-1">

					
						@foreach($VIP as $v)
						

						
							<a href="{{ route('single', [$v->slug, $v->unique_id]) }}" class="col-md-3 item {{$v->app_type}}">
								<div class="preview-img">
									<img alt="house-4" src="/uploads/applications/{{$v->id}}/2_{{$v->defaultImage->src}}">
									<div class="caption">
										{!!Helpers::getPrice($v)!!}
									</div>
								</div>
								<div class="info-block">
									<h4>
										{{L10nHelper::get($v->city)}}{{isset($v->district->title_geo) ? ", ".L10nHelper::get($v->district) : ''}}
									</h4>
								</div>
							</a>
							
						@endforeach
						
					</div>
				</div>

				<div class="banner-style-1">
					<a href="http://sesxebi.com/" rel="nofollow" target="_blank">
						<img alt="sesxebi.com" src="/assets/site/images/banners/sesxebi-com.jpg">
					</a>
					<a class="ban-caption">{{trans('main.ad')}}</a>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h5 class="app-rank-header">{{trans('main.last-apps')}}</h5>
					</div>
					<div class="block-items style-2 latest">
						@foreach($NEW as $n)
							<a href="{{ route('single', [$n->slug, $n->unique_id]) }}" class="col-lg-4 col-md-6 col-sm-6 col-xs-12 item {{$n->app_type}}" style="float:left!important;">
								<div style="background:url('/uploads/applications/{{$n->id}}/2_{{$n->defaultImage->src}}'); background-size: 100% 100%;" class="preview-img">
									
								</div>
								<div class="info-block">
									<h4>{{L10nHelper::get($n->city)}}{{isset($n->district->title_geo) ? ", ".L10nHelper::get($n->district) : ''}}</h4>
									<div class="caption">
										{!!Helpers::getPrice($n)!!}
									</div>
								</div>
							</a>
						@endforeach
						<div class="clearfix"></div>
					</div>
				</div>

			</div>
		</section>
			


	</div>
<script src="http://dgiurad.ge/assets/site/js/lightslider.min.js" type="text/javascript"></script>
	
<script type="text/javascript">
$(document).ready(function() {
    $('.responsive').lightSlider({
        item:1,
        loop:false,
        slideMove:1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:1,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ]
    });  
  });
 
	</script>
    
   

@stop