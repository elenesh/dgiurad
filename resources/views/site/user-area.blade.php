@extends ('layouts.site')

@section ('content')
	@include('site.components.user-area-heading')

	<section class="main">
		<div class="container application-list">
			<h3 class="page-header">{{trans('main.apps')}}</h3>
			<div class="row" >
				<div class="block-items style-1">
				@if(isset($apps[0]))
					@foreach($apps as $a)
						
						<div  class="col-md-4 item {{$a->app_type}} rank_{{$a->rank}}">
							<div class="app-date"><span class="start">{{trans('main.added')}}: {!!$a->start!!}</span> <span class="end"> {{trans('main.validity')}}: {!!$a->end!!}</span></div>
							<div class="preview-img">
								<a style="float:none" href="{{ route('single', [$a->slug, $a->unique_id]) }}">
									<img src="/uploads/applications/{{$a->id}}/2_{{$a->defaultImage->src}}">
								</a>
								<div class="caption">
									{!!Helpers::getPrice($a)!!}
								</div>
								<div class="overlay actions" style="left:0; right:0; z-index:999">
									<a>
										@if($a->rank == 0)
											<i class="fa fa-battery-quarter" style="color:#FF9100; margin-right:60px;"></i>
										@elseif($a->rank == 1)
											<i class="fa fa-battery-half" style="color:#AFAF00; margin-right:60px;"></i>
										@elseif($a->rank == 2)
											<i class="fa fa-battery-full" style="color:#5CB85C; margin-right:60px;"></i>
										@endif
									</a>
									<a href="{{ route('application.edit', $a->id) }}" class="edit" title="{{trans('main.edit')}}">{{trans('main.edit')}} <i class="fa fa-pencil"></i></a>
									<a data-url="{{route('application.delete', $a->id)}}" data-toggle="modal" data-target=".bs-example-modal-sm" title="{{trans('main.delete')}}" class="delete delete-button">{{trans('main.delete')}} <i class="fa fa-trash"></i></a>
								</div>
								@if($a->rank != 2)
									@if($a->rank != 1)
										<div class="overlay actions" style="height:45px;bottom:0px;left:0px;top:140px;">
									@else
										<div class="overlay actions" style="height:30px;bottom:0px;left:0px;top:155px;">
									@endif
										@if($a->rank != 1)
											<a data-url="{{route('make.vip', [$a->id, 1])}}" data-toggle="modal" data-target=".bs-1-modal-sm" title="{{trans('main.make-VIP')}}" class="make-vip"><i class="fa fa-battery-half" style="color:#AFAF00;"></i> {{trans('main.make-VIP')}}</a>
										@endif
										<a data-url="{{route('make.vip', [$a->id, 0])}}" data-toggle="modal" data-target=".bs-2-modal-sm" title="{{trans('main.make-SVIP')}}" class="make-svip"><i class="fa fa-battery-full" style="color:#5CB85C;"></i> {{trans('main.make-SVIP')}}</a>
								</div>
								@endif
							</div>
							<div class="info-block">
								<ul>
									<li>{{$a->m2}} {{trans('main.m')}}<sup>2</sup></li>
									<li>{{$a->room_quantity}} {{trans('main.room')}}</li>
									<li>{{L10nHelper::get($a->type)}}</li>
								</ul>
								<div class="clearfix"></div>

								<h4>{{L10nHelper::get($a->city)}}
									@if(isset($n->district->title_geo))
										, {{L10nHelper::get($a->district)}}
									@endif
								</h4>
							</div>
						</div>

					@endforeach
				@else


					<div class="bs-callout bs-callout-info" id="callout-navs-tabs-plugin">
						<h4>{{trans('main.you-have-no-apps')}}</h4>
						<h5><a class="btn btn-danger" href="{{ route('app.add') }}">{{trans('main.add-app')}}</a></h5>
					</div>
			
				@endif
				</div>
			</div>
		</div>

		@if(isset($inactiveApps[0]))
		<div class="container application-list">
			<h3 class="page-header">{{trans('main.inactive-apps')}}</h3>
			<div class="row" >
				<div class="block-items style-1">
					@foreach($inactiveApps as $a)
						
						<div  class="col-md-4 item {{$a->app_type}} rank_{{$a->rank}}">
							<div class="app-date"><span class="start">{{trans('main.added')}}: {!!$a->start!!}</span> <span class="end"> {{trans('main.validity')}}: {!!$a->end!!}</span></div>
							<div class="preview-img">
								<a style="float:none" href="{{ route('single', [$a->slug, $a->unique_id]) }}">
									<img src="/uploads/applications/{{$a->id}}/2_{{$a->defaultImage->src}}">
								</a>
								<div class="caption">
									{!!Helpers::getPrice($a)!!}
								</div>
								<div class="overlay actions" style="left:0; right:0; z-index:999">
									<a>
										<i class="fa fa-battery-empty" style="color:red; margin-right:60px;"></i>
									</a>
									<a href="{{ route('application.edit', $a->id) }}" class="edit" title="{{trans('main.edit')}}">{{trans('main.edit')}} <i class="fa fa-pencil"></i></a>
									<a data-url="{{route('application.delete', $a->id)}}" data-toggle="modal" data-target=".bs-example-modal-sm" title="{{trans('main.delete')}}" class="delete delete-button">{{trans('main.delete')}} <i class="fa fa-trash"></i></a>
								</div>
								<div class="overlay actions" style="height:30px;bottom:0px;left:0px;top:155px;">
									<a data-url="{{route('make.active', $a->id)}}" data-toggle="modal" data-target=".bs-4-modal-sm" title="{{trans('main.activate')}}" class="make-active">
										<i class="fa fa-battery-quarter" style="color:#FF9100;"></i>
										{{trans('main.activate')}}
									</a>
								</div>
							</div>
							<div class="info-block">
								<ul>
									<li>{{$a->m2}} {{trans('main.m')}}<sup>2</sup></li>
									<li>{{$a->room_quantity}} {{trans('main.room')}}</li>
									<li>{{L10nHelper::get($a->type)}}</li>
								</ul>
								<div class="clearfix"></div>

								<h4>{{L10nHelper::get($a->city)}}
									@if(isset($n->district->title_geo))
										, {{L10nHelper::get($a->district)}}
									@endif
								</h4>
							</div>
						</div>

					@endforeach
				</div>
			</div>
		</div>
		@endif
	</section>
	@include('helpers.delete-popup')
	@include('helpers.vip-popup')
@stop