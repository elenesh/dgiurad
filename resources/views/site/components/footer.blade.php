<footer>
	<div class="bc"></div>
	<div class="footer-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					@for($i = 0; $i < 6; $i++)
						<a href="{{Request::root()}}/{{\App::getLocale()}}/{{$footerLinks[$i]->slug}}">{{L10nHelper::get($footerLinks[$i])}}</a>
					@endfor
				</div>
				<div class="col-md-3">
					@for($i = 6; $i < 12; $i++)
						<a href="{{Request::root()}}/{{\App::getLocale()}}/{{$footerLinks[$i]->slug}}">{{L10nHelper::get($footerLinks[$i])}}</a>
					@endfor
				</div>
				<div class="col-md-3">
					@for($i = 12; $i < 18; $i++)
						<a href="{{Request::root()}}/{{\App::getLocale()}}/{{$footerLinks[$i]->slug}}">{{L10nHelper::get($footerLinks[$i])}}</a>
					@endfor
				</div>
				<div class="col-md-3">
					@for($i = 18; $i < 24; $i++)
						<a href="{{Request::root()}}/{{\App::getLocale()}}/{{$footerLinks[$i]->slug}}">{{L10nHelper::get($footerLinks[$i])}}</a>
					@endfor
				</div>
				@if(\App::getLocale() == 'ka')	
				<div class="col-md-12">
				  <div class="footer-single-text">
					<h1>კერძო სახლები, აპარტამენტები, ბინები დღიურად მთელი საქართველოს მასშტაბით. </h1>
					<hr>
					<p>ვებგვერდზე განთავსებულია ბინები, რომელთა ქირაობა შესაძლებელია დღიურად და საათობრივად. ჩვენი მიზანია მომხმარებელს შევუქმნათ ყველა პირობა, რათა სწრაფად და მარტივად მოძებნონ მათთვის სასურველი ბინა.  </p>
					<p>უძრავი ქონების სააგენტოებს ვთავაზობთ ჩვენი მომსახურებით სარგებლობას უფასოდ. სტანდარტული სერვისის გარდა შესაძლებელია VIP და სუპერ VIP განცხადებების განთავსება. განცხადებები იდება სრული აღწერით, შესაძლებელია ადგილის მონიშვნა რუკაზე. </p>
					<p>ვებგვერდის დახმარებით მარტივად იპოვნით ბინას დღიურად ან საათობრივად საქირაოდ, თქვენთვის მისაღებ ადგილას მთელი საქართველოს მასშტაბით! მოძებნეთ სასურველი ბინა დღიურად და დაზოგეთ თანხა!</p>
				</div>
				</div>
				@elseif (\App::getLocale() == 'en')
				<div class="col-md-12">
				  <div class="footer-single-text">
					<h1>Private Houses, Apartments, Flats for daily rentals throughout Georgia.</h1>
					<hr>
					<p>All ads on the website are for daily and vacation rentals. Our mission is to create the fastest and easiest way to find a place to stay for a day or more.</p>
					<p>Our service for Real Estate Agencies is totally free. Additionally, beside the standard ads, there is a possibility to publish VIP or Super VIP ads also. All ads have a full description and locations are marked on the map. </p>
					<p>Using our website you can find a place to stay from an hour to several days in all over Georgia! Do not overpay, rent a flat exactly for the duration you need and save money!</p>
				</div>
				</div>

	            @elseif(\App::getLocale() == 'ru')
	            	<div class="col-md-12">
				  <div class="footer-single-text">
					<h1>Private Houses, Apartments, Flats for daily rentals throughout Georgia.</h1>
					<hr>
					<p>All ads on the website are for daily and vacation rentals. Our mission is to create the fastest and easiest way to find a place to stay for a day or more.</p>
					<p>Our service for Real Estate Agencies is totally free. Additionally, beside the standard ads, there is a possibility to publish VIP or Super VIP ads also. All ads have a full description and locations are marked on the map. </p>
					<p>Using our website you can find a place to stay from an hour to several days in all over Georgia! Do not overpay, rent a flat exactly for the duration you need and save money!</p>
				</div>
				</div>

				 @endif

			</div>
		</div>
	</div>

	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=152100168475769";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	@if(\App::getLocale() == 'ka')

	<div class="footer-ex-menu">
		<div class="container">
			<div class="col-md-8 footer-links" style="position: relative; top:22px; ">
				<a href="{{route('contact')}}" style="color: white; font-size: 15px; float: left; padding-left: 21px;">კონტაქტი</a>
				<a href="{{route('faq')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">ხშირად დასმული კითხვები</a>
				<a href="{{route('privacy')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">კონფიდენციალურობა</a>
				<a href="{{route('instructions')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">ინსტრუქციები</a>
				<div class="social">
		
					<div class="fb-like" data-href="https://www.facebook.com/dgiurad.ge/" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
				
					<a href="//plus.google.com/+DgiuradGebinebi/posts"
					   rel="publisher" target="_top" style="text-decoration:none;">
					<img src="//ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;"/>
					</a>
				
		        </div>		
			</div>
			<div class="col-md-3">
				<a class="created" target="_blank" href="http://advertwise.ge/"></a>
			</div>
		</div>
		
	</div>
</footer>

@elseif(\App::getLocale() == 'en')

<div class="footer-ex-menu">
		<div class="container">
			<div class="col-md-8 footer-links" style="position: relative; top:22px; ">
				<a href="{{route('contact')}}" style="color: white; font-size: 15px; float: left; padding-left: 21px;">Contact</a>
				<a href="{{route('faq')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">FAQ</a>
				<a href="{{route('privacy')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">Privacy Policy</a>
				<a href="{{route('instructions')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">Instructions</a>
			</div>
			<div class="col-md-4">
				<a class="created" target="_blank" href="http://advertwise.ge/"></a>
			</div>
		</div>
	</div>

	</footer>

	@elseif(\App::getLocale() == 'ru')


	<div class="footer-ex-menu">
		<div class="container">
			<div class="col-md-8 footer-links" style="position: relative; top:22px; ">
				<a href="{{route('contact')}}" style="color: white; font-size: 15px; float: left; padding-left: 21px;">Контакт</a>
				<a href="{{route('faq')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">Часто задаваемые вопросы</a>
				<a href="{{route('privacy')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">Конфиденциальность</a>
				<a href="{{route('instructions')}}" style="color: white; font-size: 15px; float: left; padding-left:21px;">Инструкции</a>
			</div>
			<div class="col-md-4">
				<a class="created" target="_blank" href="http://advertwise.ge/"></a>
			</div>
		</div>
	</div>

	</footer>


	@endif
