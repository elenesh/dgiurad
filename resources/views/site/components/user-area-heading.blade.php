<?php 
	$auth = auth()->guard('siteUsers');
    $user = $auth->user(); 
?>
<section class="heading profile-info">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img width="100" class="user-avatar" src="/assets/site/images/avatars/default-avatar.svg">
				<ul>
					<li style="font-size: 20px;">{{ $user->first_name }} {{ $user->last_name }}</li>
					<li>{{ $user->email }}</li>
					<li>{{ $user->phone }}</li>
					<li>ID: {{ $user->unique_id }}</li>
				</ul>
			</div>

			<div class="col-md-4">
				<a href="{{ route('user.profile.edit') }}" class="btn btn-success profile-edit-btn"><i class="fa fa-pencil"></i> {{trans('main.profile-edit')}}</a>
				<a href="{{ route('user.area') }}" class="btn btn-success balance-edit-btn"><i class="fa fa-newspaper-o"></i> {{trans('main.my-apps')}}</a>
			</div>

			<div class="col-md-4">
				<a class="btn btn-success balance-edit-btn" style="margin-top: 15px;" href="{{route('user.pay')}}"><i class="fa fa-credit-card"></i> {{trans('main.charge-balance')}}</a>
				<div class="balance">
					{{trans('main.balance')}}
					<i>{{ $user->balance }}</i>
					{{trans('main.gel')}}
				</div>
			</div>
		</div>

	</div>
</section>