<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">

    {{--Static Pages--}}
    <url>
        <loc>{{ Request::root() }}</loc>
        <priority>1.0</priority>
    </url>

    {{--Dynamic Pages--}}

    @foreach($footerLinks as $s)
        <url>
            <loc>{{ Request::root() . '/ka/'.$s->slug}}</loc>
            <priority>0.8</priority>
        </url>
    @endforeach

    @foreach($apps as $s)
        <url>
            <loc>{{ Request::root() . '/ka/application/'.$s->slug.'/'.$s->unique_id}}</loc>
            <priority>0.4</priority>
        </url>
    @endforeach

</urlset>