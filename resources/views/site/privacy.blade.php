@extends ('layouts.site')

@section ('content')

@include('site.components.search')

<body>
	@if(\App::getLocale() == 'ka')	  
		<div class="container reg-login" style="background: #fff;color: #565a5c;padding: 25px 5px; padding-bottom: 40px;margin: 30px auto;min-height: 410px;">
      <div class="col-md-12 both">	

      <h3>კონფიდენციალურობის პოლიტიკა</h3>

	        <h5>1. ზოგადი პირობები</h5>
			<p>1.1. წინამდებარე ვადები და პირობები არეგულირებს ამ ვებგვერდის გამოყენების ფარგლებს. ამ ვებგვერდის გამოყენებით, თქვენ სავსებით ეთანხმებით და იღებთ მოცემულ ვადებსა და პირობებს;
			1.2. რეკომენდებულია, არ იქნეს ამ ვებგვერდის გამოყენება იმ პირის მიერ, რომელიც არ იღებს ხსენებულ ვადებსა და პირობებს;
			1.3. ამ ვებგვერდის გამოყენების მიზნებისათვის, პირის მინიმალური ასაკი უნდა წარმოადგენდეს არა ნაკლებ 18 წელს;
			1.4. ჩვენ ვიტოვებთ უფლებას, განვახორციელოთ და შევიტანოთ ცვლილებები ამ ვადებსა და პირობებში აღნიშნული შესწორების თაობაზე გაფრთხილების გარეშე. შესწორებული ვადები და პირობები ამოქმედდება დეიდან მათი ვებგვერდზე განთავსებისა. იმის გათვალისწინებით, რომ ჩვენი ვადები და პირობები ექვემდებარება შესწორებებს, რეკომენდებულია, შეამოწმოთ აღნიშნული ვებგვერდზე ყოველი სტუმრობისას. თქვენ მიერ ამ ვებგვერდის სისტემატური გამოყენება წარმოშობს ამ ვადებისა და პირობების მიღებას თქვენის მხრიდან.
</p>
			<h5>2. ჩვენი სერვისი</h5>
			<p>
			
	        2.1. ჩვენ გთავაზობთ დამოუკიდებელ ონლაინ სერვისს, რომლის საშუალებითაც ხდება თქვენი და მესამე მხარის ერთმანეთთან დაკავშირება, რათა მარტივად მოხდეს ორი მხარის ინტერესთა თანხვედრა უძრავი ქონების დღიურად ან/და საათობრივად გაქირავებასთან დაკავშირებით.
			2.2. ჩვენს მიზანს წარმოადგენს უზრუნველგყოთ ვებგვერდზე უწყვეტი ხელმისაწვდომობით. ამასთან, არ ვიძლევით აღნიშნული უწყვეტი ხელმისაწვდომობის გარანტიას იმგვარი დაზიანების ან ნებისმიერის ისეთი ვითარების შემთხვევაში, რაც ჩვენთან არ არის დაკავშირებული. ჩვენ ვიტოვებთ უფლებას, ნებისმიერ დროს შევაჩეროთ, შევზღუდოთ ან შევწყვიტოთ ვებგვერდზე თქვენი დაშვება;
			2.3. ჩვენ ვიტოვებთ უფლებას, წინასწარი შეტყობინების გარეშე განვახორციელოთ ვებგვერდზე განთავსებულ ინფორმაციაში, მასალაში, შინაარსსა თუ მონაცემებში ცვლილება, წაშლა, რედაქტირება, შესწორება ან დამატება.
			</p>

	   		<h5>3. ნებადართული გამოყენება</h5>
	   		<p>3.1. თქვენ უფლება გაქვთ გქონდეთ წვდომა და გამოიყენოთ ეს საიტი მხოლოდ თქვენი პირადი, არაკომერციული მიზნებისათვის, ამ ვებგვერდზე წვდომა და გამოყენება დასაშვებია მხოლოდ უშუალოდ თქვენს, როგორც კერძო პირის, მიერ ან თქვენს, როგორც ბიზნეს–საქმიანობის ფაგრლებში უშუალოდ ან თქვენი სახელით ჩვენი სერვისის მაძიებლის მიერ. მკაცრად იკრძალება ამ ვებგვერდზე წვდომა და მისი გამოყენება რაიმე სხვა მინზებისათვის, თუ ეს არ წარმოადგენს თქვენს პირად და არაკომერციულ მიზანს;
			3.2. აკრძალულია ამ ვებგვერდის გამოყენება:
			3.2.1. ნებისმიერი უკანონო, თაღლითური ან კომერციული გზით;
			3.2.2. ისეთი მიზნით, რომელშიც მოაიაზრება სხვა პირის მიმართ ზიანი, შეურაცხყოფა, მუქარა, ცილისწამება, დაშინება ან შევიწროვება ან ვებგვერდზე წვდომა და მისი გამოყენება ისეთი გზით, რაც არღვევს სხვა პირის პირად ცხოვრებას, ან თუ მასში მოაზრებულია უხამსი, შეურაცხმყოფელი, არაზუსტი, ცილისმწამებლური, არაადექვატური, არასასურველი, მიუღებელი, დისკრიმინაციული მოცემულობა, რაც ამგვარად არის განსაზღვრული ჩვენ მიერ;
			3.2.3. საკუთარი ან სხვისი მონაცემთა ბაზის, ჩანაწერების ან ცნობარის შექმნა, შემოწმება, დამტკიცება, განახლება, შესწორებების შეტანა ან შეცვლა;
			3.2.4. ამ ვებგვერდის ან მისი ნაწილის შეცვლა, შესწორებების შეტანა, დაზიანება ან მასზე რევერსული ინჟინერიის წარმოება;
			3.2.5. რაიმე ქმედების განხორციელება, რაც არღვევს, წარმოშობს ან ახდენს მიზანშეუწონელი და არაპროპორციულად დიდი მოცულობით ჩარევას ჩვენს ტექნიკურ სისტემებსა და კომუნიკაციებში;
			3.2.6. ავტომატიზირებული პროგრამული უზრუნველყოფის, პროცესის, პროგრამის, რობოტის, ვებ crawler–ის, spider–ის, მონაცემების მოპოვება, trawling–ის ან სხვა ავტომატური ეკრანზე მოსმენით პროგრამული უზრუნველყოფა, პროცესის, პროგრამა ან ავტომატური ეკრანული წაკითხვის პროგრამული უზრუნველყოფის, პროცესის, პროგრამისა თუ სისტემის გამოყენება.
			</p>
	   		<h5>4. ინტელექტუალური საკუთრების უფლებები</h5>
	   		<p>4.1. ამ ვებგვერდზე არსებული ინფორმაცია, შინაარსი, მასალა ან მონაცემები წარმოადგენს ჩვენს, ჩვენი ლიცენზიარისა ან პროდუქტის მესაკუთრის საკუთრებას, რომელიც მითითებულია ვებგვერდზე. თქვენი პირადი მოხმარებისათვის, შეგიძლიათ განახორციელოთ დროებით ამობეჭდვა, ასლის მიღება, ჩამოტვირთვა ან ამონაწერის მიღება ამ ვებგვერდზე შენახული ინფორმაციის, შინაარსის, მასალისა ან მონაცემებისა, რაც, თავის მხრივ, უნდა განხორციელდეს შემდეგი პირობების შესაბამისად და გათვალისწინებით:
			4.1.1. ამ ვებგვერდზე არსებული ინფორმაცია, შინაარსი, მასალა ან მონაცემები არ შეიძლება გამოყენებულ იქნას რაიმე სახის კომერციული მიზნებისთვის და არ შეიძლება მათი კომერციულის სახით ექსპლოატაცია ჩვენი წერილობითი თანხმობის გარეშე;
			4.1.2. დაუშვებელია თქვენ მიერ ავტომატიზირებული პროგრამული უზრუნველყოფის, პროცესის, პროგრამის, რობოტის, ვებ crawler–ის, spider–ის, მონაცემების მოპოვება, trawling–ის ან სხვა ავტომატური ეკრანზე მოსმენით პროგრამული უზრუნველყოფა, პროცესის, პროგრამა ან ავტომატური ეკრანული წაკითხვის პროგრამული უზრუნველყოფის, პროცესის, პროგრამისა თუ სისტემის გამოყენება;
			4.1.3. დაუშვებელია ინფორმაციის გაყიდვა ან გადაცემა მესამე მხარის მიმართ;
			4.1.4. ასლი უნდა შეიცავდეს ამ ვებგვერდზე არსებულ შეტყობინებას საავტორო ან სხვა ინტელექტუალური საკუთრების შესახებ. არ დაიშვება თქვენ მიერ საბუთების ან ამგვარი ინფორმაციის ციფრული ასლების, შინაარსის, მასალის ან მონაცემების მოდიფიცირება;
			4.1.5. ჩვენი, ჩვენი ლიცენზიარის ან მომსახურების პროვაიდერების სტატუსი წარმოჩენილ უნდა იყოს იმგვარად, რომ ადასტურებდეს სათანადო ინფორმაციის, შინაარსის, მასალისა თუ მონაცემების ავტორობას;
			4.2. DGIURAD.GE–ზე ყველა უფლებები გვეკუთვნის ჩვენ.
			</p>
	   		<h5>5. ჩვენი პასუხისმგებლობის ფარგლები</h5>
	   		<p>5.1. ჩვენს მიზანსა და ძალისხმევის საგანს წარმოადგენს, უზრუნველყოთ, რათა ამ გვერდზე მოცემული ინფორმაცია იყოს ზუსტი და განახლებადი – მასში რაიმე ცდომილების ან უზუსტობის გასწორება მოხდეს აღნიშნულის თაობაზე შეტყობიდნებიდან შესაძლო მოკლე ვადაში;
			5.2. ჩვენ არ ვართ პასუხისმგებელნი ან ანგარიშვალდებულნი ნებისმიერი იმ დანაკარგის ან მიყენებული ზარალის გამო, რაც მოგადგებათ მესამე მხარის მიერ შეგროვებული და ჩვენს ვებგვერდზე განთავსებული ინფორმაციის, შინაარსის, მასალისა თუ მონაცემების გამო. ჩვენ არ ვამოწმებთ ამგვარ ინფორმაციას, შინაარსს, მასალას ან მონაცემებს, არ ვახორციელებთ მათ მონიტორინგს, არ მიმოხილავთ, არ ვახდენთ გადამოწმებასა და ინდოსირებას მათი უზუსტობის, არასრულყოფილების ან მოძველებულობის კუთხით: ინფორმაციის, შინაარსის, მასალისა თუ მონაცემების არის სიზუსტის, სისრულის, სისწორისა და განახლებადობის შემოწმება არის თქვენი პასუხისმგებლობა;
			5.3. ნებისმიერი შეხედულება, მოსაზრება, რჩევა, მიმოხილვა, რეიტინგები და კომენტარები ამ ვებგვერდსა თუ მესამე მხარის მიერ მომზადებულ ვებგვერდზე (შინაარსის ჩათვლით) არ წარმოადგენს ჩვენს შეხედულებებს, მოსაზრებებს, რჩევას, მიმოხილვებსა და რეიტინგებს. ასეთი ინფორმაციას არ უნდა დაეყრდნოთ, ვინაიდან ჩვენ მიერ არ ხდება აღნიშნული მოცემულობის გადამოწმება, მონიტორინგი, განხილვა, გადამოწმება თუ ინდოსირება. ჩვენ არ ვართ პასუხისმგებელი ან ანგარიშვალდებული რაიმე დანაკარგის ან დაზიანების გამო, რაც შეიძლება განიცადოთ ან წარმოიშვას ამგვარ მოსაზრებებთან, შეხედულებებთან, რჩევებთან, გადამოწმებასთან, რეიტინგებთან და კომენტარებთან დაკავშირებით –მათი უზუსტობის, სისწორისა თუ არასრულყოფილების მიზეზის ჩათვლით;
			5.4. ჩვენ არ ვიძლევით რაიმე გარანტიას, რომ ამ ვებგვერდზე განთავსებული ნებისმიერი მესამე მხარის ვებგვერდი ან რაიმე ინფორმაცია, შინაარსი, მასალა ან მონაცემები თავისუფალია ვირუსების, spyware–ის, სახიფათო პროგრამული უზრუნველყოფის, ტროას ვირუსის, ეგ.ქ. worm–ის, logic bomb–ის რაიმე სხვა ისეთი ტექნოლოგიისაგან, რასაც შეიძლება თან სდევდეს დაბინძურების ეფექტი, მავნე და დამღუპველი ზეგავლენა. ჩვენი რეკომენდაციით, მიზანშეწონილია მიმართოთ ვირუსის შემოწმების სათანადო პროგრამულ უზრუნველყოფას. ჩვენ არ ვართ პასუხისმგებელნი ან ანგარიშვალდებულნი რაიმე დანაკარგსა თუ დაზიანებაზე, თქვენ შეიძლება განიცადოთ ან წარმოიშვას მესამე მხარის მიზეზით;
			5.5. ჩვენ არ ვართ პასუხისმგებელნი ან ანგარიშვალდებულნი რაიმე დანაკარგსა თუ დაზიანებაზე, თქვენ შეიძლება განიცადოთ ან წარმოიშვას თქვენ მიერ ამ ვებგვერდის გამოყენების გამო რაიმე იმ მიზეზით, რაც სცილდება ჩვენი მხრიდან მიზანშეწონილი კონტროლის ფარგლებს – სხვათა მიერ ელექტრონული ინფორმაციის, შინაარსის, მასალისა და მონაცემების ინტერნეტში გადაცემის, მოხსნისა თუ დეშიფრაციის ჩათვლით;
			5.6. ჩვენ არ ვართ პასუხისმგებელნი ან ანგარიშვალდებულნი რაიმე დანაკარგსა თუ დაზიანებაზე, თქვენ შეიძლება განიცადოთ ან წარმოიშვას თქვენ მიერ ამ ვებგვერდის გამოყენების გამო (დანაზოგის, ბიზნეს–საქმიანობის, ბიზნეს–პერსპექტივების, შემოსავლის ან მოგების დაკარგვის ჩათვლით).
			</p>
	   		<h5>6. ჩვენი პასუხისმგებლობის გამორიცხვა</h5>
	   		<p>6.1. გაითვალისწინეთ, რომ ამ ვებგვერდზე ინფორმაცია და პროდუქციისა და მომსახურების აღწერილობები არ წარმოადგენს პროდუქტების და მომსახურების ყველა თვისებებისა და პირობების სრულ აღწერილობას. ყურადღებით გაეცანით პროდუქტების და მომსახურების ყველა თვისებებასა და პირობებს მათ გამოყენებამდე;
			6.2. თუ განაცხადს აკეთებთ რაიმე სახის პროდუქტსა თუ მომსახურებაზე, თქვენ იქნებით მესამე მხარესთან ხელშემკვრელი მხარე (კონტრაქტის), რომელიც (მესამე მხარე) უზრუნველგყოფთ პროდუქტით და მომსახურებით თავიანთი ვადებისა და პირობების ფარგლებში. თქვენი პირადი პასუხისმგებლობაა გაეცნოთ, გაიაზროთ და შემდეგ და დაეთანხმოთ ამ პირობებსა და ვადებს, სანამ მოცემულ კონკრეტულ პროდუქტსა თუ მომსახურებაზე დადებდეთ კონტრაქტს. ჩვენ არ ვართ პასუხისმგებელი ან ანგარიშვალდებული იმ დანაკარგის ან დაზიანების გამო, რაც შეიძლება განიცადოთ ან წარმოიშვას ხსენებულ პროდუქტსა და სერვისებთან დაკავშირებით მესამე მხარესთან კონტრაქტზე ხელმოწერის გამო;
			6.3. ნებისმიერი შეხედულება, მოსაზრება, რჩევა ან დახმარება, რომელიც გაცემულია მესამე მხარის მიერ თქვენი მხრიდან ამ ვებგვერდის გამოყენების შემდეგ, არ წარმოადგენს ჩვენს შეხედულებებს, მოსაზრებებს, რჩევას ან დახმარებას და აღნიოშნული არ არის გადამოწმებული, შემოწმებული, განხილული, დადგენილი ან გაკონტროლებული ჩვენს მიერ. ჩვენ არ ვახდენთ ინდოსირებას, არ გავცემთ რეკომენდაციას და არ ვიღებთ პასუხისმგებლობას მესამე მხარეზე, რომელიც უზრუნველგყოფთ თავისი მხარის შეხედულებებით, მოსაზრებებით, რჩევით ან დახმარებით. ჩვენ არ ვართ პასუხისმგებელი ან ანგარიშვალდებული იმ დანაკარგის ან დაზიანების გამო, რაც შეიძლება განიცადოთ ან წარმოიშვას ამგვარი შეხედულებების, მოსაზრებების, რჩევის ან დახმარების მიზეზით, მესამე მხარისაგან მიღებულის შეხედულებების, მოსაზრებების, რჩევის ან დახმარების, ასევე, აღნიშნულის ზუსტის, უტყუარობის, სისრულის ჩათვლით;
			6.4. გაითვალისწინეთ, რომ ეს საიტი იძლევა პროდუქციის ან მომსახურების შესახებ ფართო სპექტრის ინფორმაციას; შესაძლოა, ბაზარზე არსებობდეს სხვა პროდუქცია ან მომსახურება, რომელიც არ არის ნაჩვენები ამ ვებგვერდზე და რაც შეიძლება, გაცილებით მისაღები და შესაფერისი იყოს თქვენი საჭიროებებისათვის.
			</p>

			<h5>7. თქვენი პასუხისმგებლობა</h5>
			<p>7.1. თქვენ ვალდებული ხართ, მიიღოთ ყველა გონივრული ზომა (მათ შორის, შესაბამისი ვირუსის შემოწმება), რათა უზრუნველყოფილ იქნეს, რომ თქვენს მიერ მოწოდებული ნებისმიერი ინფორმაცია, შინაარსი, მასალა ან მონაცემები თავისუფალი იყოს ვირუსების, spyware–ს, საფრთხის შემცველი პროგრამული უზრუნველყოფის, ტროას ვირუსის, ეგ.წ. worm–ის, logic bomb–ის, და ყოველივე იმისაგან, რომელსაც შეიძლება ჰქონდეს დაბინძურების ეფექტი, მავნე და დამღუპველი ზეგავლენა ამ ვებგვრდის ან მისი რომელიმე ნაწილის ან მესამე მხარეების ვებგვერდების ან სხვა ტექნოლოგიის მიმართ;
			7.2. თქვენ ვალდებული ხართ, შეამოწმოთ და უზრუნველყოთ, რათა თქვენს მიერ მოწოდებული ნებისმიერი ინფორმაცია, შინაარსი, მასალა ან მონაცემები იყოს სწორი, სრული, ზუსტი და თავისუფალი რაიმე სახის დეზინფორმაციისაგან, ასევე, უზრუნველყოფილ უნდა იქნეს ის გარემოებახც, რომ თქვენის მხრიდან არ აქვს ადგილი ყველა ამ შესაბამისი ფაქტების გამჟღავნებას. ჩვენ არ ვიღებთ პასუხისმგებლობას ან ანგარიშვალდებულებას იმ დანაკარგის ან დაზიანების გამო, რომელიც შეიძლება განიცადოთ ან წარმოიშვას ამ ვებგვერდზე თქვენს მიერ მოწოდებული ინფორმაციის, შინაარსის, მასალის ან მონაცემების უზუსტობის, არასრულყოფილების ან შეუსაბამობის მიზეზით ან ამ ფქტების თქვენის მხრიდან გამჟღავნების შედეგად;
			7.3. მანამ, სანამ მიიღებდეთ რაიმე პროდუქტს ან მომსახურებას მესამე მხარისაგან, ვალდებული ხართ, შეამოწმოთ ყველა ინფორმაცია, შინაარსი, მასალა ან მონაცემები, რომელსაც ფლობს მესამე მხარე თქვენს შესახებ, რათა გადააკონტროლოთ და დარწმუნდეთ, რომ ამგვარი ინფორმაცია, შინაარსი, მასალა ან მონაცემები არის სწორი, სრული, ზუსტი და მოკლებულია დეზინფორმაციას, ასევე, რათა დაადასტუროთ ის ფაქტიც, რომ თქვენს მირ არ ხდება ყველა ამ შესაბამისი ფაქტების გამჟღავნება. თქვენი პასუხისმგებლობაა მოახდინოთ მესამე მხარის ხელთ არსებულ ინფორმაციაში, შინაარსში, მასალასა ან მონაცემებში რაიმე ცდომილების ან შეცდომის იდენტიფიცირება და შეაბამისი გასწორება/კორექტირება, წინააღმდეგ შემთხვევაში, შეიძლება მოხდეს მესამე მხარის მიერ მოწოდებული პროდუქტის ან სერვისის გაუქმება. ჩვენ არ ვიღებთ პასუხისმგებლობას ან ანგარიშვალდებულებას იმ დანაკარგის ან დაზიანების გამო, რომელიც შეიძლება განიცადოთ ან წარმოიშვას მესამე მხარის ხელთ არსებული შინაარსის, მასალის ან მონაცემების უზუსტობის, არასრულყოფილების, შეუსაბამობის ან მასში არსებული დეზინფორმაციული სახის მოცემულობის მიზეზით ან თქვენს მიერ ყველა ამ ფაქტის გაუმჟღავნელობის მიზეზით;
			7.4. ამით თქვენ ეთანხმებით, რომ პასუხისმგებელი იქნებით ყველა იმ ზიანზე, დანაკარგზე, სარჩელზე, მოთხოვნზე, ვალდებულებასა თუ ხარჯზე (იურიდიული მომსახურების გონივრული მოცულობის საფასურის ჩათვლით), რომელიც შეიძლება განიცადოთ ან წარმოიშვას ამ ვებგვერდზე თქვენი ქმედების გამო.
			</p>

	   </div>
	  
	
	
		
	</div>
	@elseif(\App::getLocale() == 'en')


	<div class="container reg-login" style="background: #fff;color: #565a5c;padding: 25px 5px; padding-bottom: 40px;margin: 30px auto;min-height: 410px;">
      <div class="col-md-12 both">	
	

     
      <h3>Privacy Police</h3>

	        
			<p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information' (PII) is being used online. PII, as used in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.
</p>
			<h5>What personal information do we collect from the people that visit our blog, website or app?</h5>
			<p>
			
	        When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, phone number  or other details to help you with your experience
			</p>

	   		<h5>When do we collect information?</h5>
	   		<p>We collect information from you when you register on our site  or enter information on our site.
			</p>
	   		<h5>How do we use your information? </h5>
	   		<p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:
	   		<ul>
	   			<li>To allow us to better service you in responding to your customer service requests.</li>
	   			<li>To quickly process your transactions.</li>
	   			<li>To administer a contest, promotion, survey or other site feature.</li>
	   	    </ul>		
			</p>
	   		<h5>How do we protect visitor information?</h5>
	   		<p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.We use regular Malware Scanning.Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.All transactions are processed through a gateway provider and are not stored or processed on our servers.
			</p>
	   		<h5>Do we use 'cookies'?</h5>
	   		<p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.

	   	    <strong>We use cookies to:</strong> Understand and save user's preferences for future visits. Keep track of advertisements. Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.<br>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser's Help menu to learn the correct way to modify your cookies.<br><br>If you disable cookies off, some features will be disabled It won't affect the user's experience that make your site experience more efficient and some of our services will not function properly.However, you can still place orders .<strong>Third-Party Disclosure</strong>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information.<strong>Third-party links</strong><br />Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.
			</p>

			<h5>Google</h5>
			<p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en <br>We use Google AdSense Advertising on our website.<br>Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.<br><strong>We have implemented the following:</strong>  Remarketing with Google AdSense Google Display Network Impression Reporting Demographics and Interests Reporting DoubleClick Platform Integration We along with third-party vendors, such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website. <br>Opting out:<Br>
					Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising initiative opt out page or permanently using the Google Analytics Opt Out Browser add on.<strong>California Online Privacy Protection Act</strong>CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy.  The law's reach stretches well beyond California to require a person or company in the United States (and conceivably the world) that operates websites collecting personally identifiable information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals with whom it is being shared, and to comply with this policy. -  See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf<strong>According to CalOPPA we agree to the following:</strong>Users can visit our site anonymously.Once this privacy policy is created, we will add a link to it on our home page or as a minimum on the first significant page after entering our website.Our Privacy Policy link includes the word 'Privacy' and can be easily be found on the page specified above.<br>Users will be notified of any privacy policy changes: On our Privacy Policy PageUsers are able to change their personal information: By logging in to their account How does our site handle do not track signals?We honor do not track signals and do not track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place. <br><strong>Does our site allow third-party behavioral tracking?</strong>It's also important to note that we do not allow third-party behavioral tracking<strong>COPPA (Children Online Privacy Protection Act)
			</p>
			<h5></h5>
			<p>When it comes to the collection of personal information from children under 13, the Children's Online Privacy Protection Act (COPPA) puts parents in control.  The Federal Trade Commission, the nation's consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.<br>We do not specifically market to children under 13.<strong>Fair Information Practices</strong><br />The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.<br><strong>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</strong>We will notify the users via email Within 7 business days</p>
			<h5></h5>
			<p>We also agree to the Individual Redress Principle, which requires that individuals have a right to pursue legally enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.<strong>CAN SPAM Act</strong>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.<strong>We collect your email address in order to:</strong>To be in accordance with CANSPAM we agree to the following:<strong><br>If at any time you would like to unsubscribe from receiving future emails, you can email us at</strong> and we will promptly remove you from <strong>ALL</strong> correspondence.<strong>Contacting Us</strong>If there are any questions regarding this privacy policy you may contact us using the information below.<br><br>DGIURAD.GE<br></p>

	   </div>
	  
	
	
		
	</div>

	@endif
</body>
@stop