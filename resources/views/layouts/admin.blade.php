<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="/assets/css/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="/assets/fileinput/css/fileinput.min.css">
		<link rel="stylesheet" href="/assets/css/admin.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="/assets/css/bootstrap-theme.min.css">
		<!-- Latest compiled and minified JavaScript -->
		<script src="/assets/js/jquery-1.11.3.min.js"></script>
		<script src="/assets/fileinput/js/plugins/canvas-to-blob.min.js"></script>
		<script src="/assets/fileinput/js/fileinput.min.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>
		<script src="/assets/js/bootstrap-datepicker.min.js"></script>

	</head>
	<body>
		@include('admin.components.header')
		@include('admin.components.alerts')
		<div class="row" style="margin-right:0px">
			@include('admin.components.side-bar')
			@yield('content')
		</div>
		
		@include('helpers.delete-popup')
	</body>
</html>