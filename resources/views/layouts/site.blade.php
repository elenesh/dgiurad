<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		@if(Request::segment(2) == '')
			{{isset($title) ?  $title : ''}}
		@else
			{{isset($title) ?  $title .' | DGIURAD.GE' : 'DGIURAD.GE'}}
		@endif
	</title>
	<meta name="description" content="{{isset($description) ?  $description : ''}}">
	<link rel="alternate" href="{{Helpers::getLocaleChangerURL('ka')}}" hreflang="ka-ge" />
	<link rel="alternate" href="{{Helpers::getLocaleChangerURL('en')}}" hreflang="en" />
	<link rel="alternate" href="{{Helpers::getLocaleChangerURL('ru')}}" hreflang="ru" />

	<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/app.css')}}">

	<link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/icons/apple-icon-57x57.png')}}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets/icons/apple-icon-60x60.png')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/icons/apple-icon-72x72.png')}}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/icons/apple-icon-76x76.png')}}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/icons/apple-icon-114x114.png')}}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/icons/apple-icon-120x120.png')}}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/icons/apple-icon-144x144.png')}}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/icons/apple-icon-152x152.png')}}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/icons/apple-icon-180x180.png')}}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{asset('assets/icons/android-icon-192x192.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/icons/favicon-32x32.png')}}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets/icons/favicon-96x96.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/icons/favicon-16x16.png')}}">
	<link rel="manifest" href="{{asset('assets/icons/manifest.json')}}">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{asset('assets/icons/ms-icon-144x144.png')}}">
	<meta name="theme-color" content="#ffffff">


	<script type="text/javascript" src="{{asset('assets/site/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/site/js/angular.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/site/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/site/js/multiple-select.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/site/js/jquery.nstSlider.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/site/js/app.js')}}"></script>


	<script type="application/javascript">
		var lang_object = {
			'choose' : '{{trans('main.choose')}}',
			'selected' : '{{trans('main.selected')}}',
			'nothing-found' : '{{trans('main.nothing-found')}}',
			'all-district' : '{{trans('main.all-district')}}',
			'daily' : '{{trans('main.in-day')}}',
			'hourly' : '{{trans('main.in-hour')}}',
			'room' : '{{trans('main.room')}}',
			'tel' : '{{trans('main.tel')}}'
		};
	</script>
	



</head>
<body ng-app="myApp" data-lang="{{Request::segment(1)}}">
	
	@include('site.components.header')
	@yield('content')
	@include('site.components.footer')

<script type="text/javascript">
	$('.nstSlider').nstSlider({
		"rounding": {
	        "20": "1000"
	    },
	   	"left_grip_selector": ".leftGrip",
	   	"right_grip_selector": ".rightGrip",
	   	"value_bar_selector": ".bar",
	   	"value_changed_callback": function(cause, leftValue, rightValue) {
	   	    $(this).parent().find('.leftLabel input').val(leftValue);
	   	    $(this).parent().find('.rightLabel input').val(rightValue);
	   	}
	});
</script>
</body>
</html>