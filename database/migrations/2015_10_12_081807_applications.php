<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Applications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->String('unique_id');
            $table->unique('unique_id');
            $table->dateTime('end_time');
            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
            $table->String('title_geo');
            $table->String('title_eng');
            $table->String('title_rus');
            $table->text('description_geo');
            $table->text('description_eng');
            $table->text('description_rus');
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
            $table->String('street_addr')->nullable();
            $table->float('m2')->nullable();
            $table->integer('room_quantity')->nullable();
            $table->integer('floor')->nullable()->nullable();
            $table->float('daily_price')->nullable();
            $table->float('hourly_price')->nullable();
            $table->String('currency')->default('GEL');
            $table->integer('condition_id')->unsigned();
            $table->foreign('condition_id')->references('id')->on('condition')->onDelete('cascade');
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('rank')->default(0);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('default_image_id')->unsigned()->nullable();
            //$table->foreign('default_image_id')->references('id')->on('images')->onDelete('cascade');
            $table->String('phone');
            $table->String('comment')->nullable();
            $table->String('slug');
            $table->integer('views')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applications');
    }
}
