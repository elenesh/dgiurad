<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->String('unique_id');
            $table->unique('unique_id');
            $table->String('email');
            $table->unique('email');
            $table->String('password');
            $table->String('phone');
            $table->unique('phone');
            $table->String('first_name');
            $table->String('last_name');
            $table->date('birth_date')->nullable();
            $table->integer('type')->default(0);
            $table->String('incorporated_id')->nullable();
            $table->String('incorporated_name')->nullable();
            $table->float('balance')->default(0);
            $table->integer('blocked')->default(0);
            $table->String('remember_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
