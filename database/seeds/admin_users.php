<?php

use Illuminate\Database\Seeder;

class admin_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_users')->delete();

        $admin_users = [
        	[
        		"username" => "admin",
        		"password" => "$2y$10$5Ay6QQ5y0ZDWuwNsO4MaZu.FlmeOddbIyE.SAF62zRMIxy9XQAlR.",
        		"role"	   => "0"	
        	]
        ];
        DB::table('admin_users')->insert($admin_users);
    }
}
