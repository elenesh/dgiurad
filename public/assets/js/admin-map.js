var map;
function initMap() {

	var latitude = parseFloat($('#latitude').val());
	var longitude = parseFloat($('#longitude').val());

  	map = new google.maps.Map(document.getElementById('map'), {
    	center: {lat: latitude, lng: longitude},
    	zoom: 11
  	});

  	var marker = new google.maps.Marker({
	    position: {lat: latitude, lng: longitude},
	    map: map
	});

  	map.addListener('click', function(event) {
   		addMarker(event.latLng, marker);
	});

}
// Adds a marker to the map and push to the array.
function addMarker(location, marker) {
  marker.setPosition(location);
  $('#latitude').val(marker.position.lat());
  $('#longitude').val(marker.position.lng());
}



