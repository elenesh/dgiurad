
var popupModal = function(status_msg){
    $('#modal1Label').html(status_msg['title']);
    html = '';

    for (var i in status_msg['msg']) {
        if (status_msg['msg'].hasOwnProperty(i)) {
            html += status_msg['msg'][i] + '<br>';
        }
    }

    $('#modal1 .modal-body').html(html);
    $('#modal1').modal('show');
}


$(function() {
    
    if($(window).width() > 992){
        $(window).scroll(function(){
            if($(this).scrollTop() > 101){
                $('.search-area').addClass('fixed-search-area');
            }
            else{
                $('.search-area').removeClass('fixed-search-area')
            }
        });
    }


    $('.multiple-select').multipleSelect({
        filter: true,
        width: '100%',
        multiple: true,
        selectAll: false,
        placeholder: lang_object["choose"],
        countSelected: lang_object["selected"],
        noMatchesFound: lang_object["nothing-found"],
    });
    $('.multiple-select2').multipleSelect({
        filter: false,
        width: '100%',
        placeholder: lang_object['choose'],
        multiple: true,
        selectAll: false,
    });

    //angular.element('#test').scope().getAplications('raginda');

    //build  city select option
   // setTimeout(function(){
        // var html = '';
        // for (var i = 0; i < city_region.length; i++) {
        //     html += '<option value="'+city_region[i]['id']+'_'+[i]+'">' +city_region[i]['title-geo']+ '</option>';
        //     $('#city-select').html(html);

        //     $('#city-select')[0].sumo.reload();
        //     $('#city-select')[0].sumo.enable();
        // };
   // }, 3000);

    // if (typeof $('#city-select')[0] != 'undefined') {
    //     $('#city-select')[0].sumo.reload();
    //     $('#city-select')[0].sumo.enable();
    // }

    
    // build  region select option
    $('#city-select').closest('.option-group').on('click', '.ms-drop li', function(event) {
        //event.preventDefault();
        
        var city_select = [];
       

        $(this).closest('.ms-drop').find('li.selected').each(function( index ) {
            city_select.push( $( this ).find('input').val().split('_')[1] );
        });


        
        
        regionHtml = '';
        $('#region-select').html('');
        cout = 0;

        $.each(city_select,function(index,value){
            
            parent = city_region[value]['districts'];
            
            if (typeof parent[index] !== 'undefined') {
                regionHtml += '<optgroup label='+ city_region[value]['title'] +' >';
                
                    for (var i = 0; i < parent.length; i++) {
                        regionHtml += '<option value="'+parent[i]['id']+'">' +parent[i]['title']+ '</option>';
                    };
                    
                regionHtml += '</optgroup>';
                cout = 1;
                $('#region-select').html(regionHtml);
            }
        });


        if(cout == 1){
            $('#region-select').multipleSelect("refresh");
            $('#region-select').multipleSelect('enable');
            $('#region-select').removeAttr('disabled');
            
        } 
        else {
            $('#region-select').multipleSelect('disable');
            $('#region-select').closest('.option-group').find('.ms-parent .placeholder').html(lang_object['all-district']);
        }

    });


    //$( ".search-form" ).on( "submit", function( event ) {
      //event.preventDefault();
      //console.log( $( this ).serialize() );
    //});

    $('.room-select .room').click(function() {
        $('.room-select .wrapper').toggleClass('open');
    });

    $('.detail-search-link').click(function() {
        $( ".detail-search" ).slideToggle( "fast", function() {
            // Animation complete.
            // $('#search-btn').toggle();
            // $('.from-to-input').toggleClass('col-md-3');
            // $('.from-to-input').toggleClass('col-md-5');
          });
    });

    $('._search-link').click(function() {
        $( "._search" ).slideToggle( "fast", function() { });
    });

    // $('.room-select').on('change keyup', 'input', function(event) {
    //  event.preventDefault();
    //  $('.room-select .room span').html()
    //  /* Act on the event */
    // });

    $('body').on('keypress', 'input[type=text], textarea', function(event) {
        lan = $('body').attr('data-lang');
        return makeLan(this,event,lan);
    });


});

    

var app = angular.module('myApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });


app.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);


app.controller('AplicationsCtrl', function ($scope, $http) {

    // $scope.getAplications = function(params){
    //     console.log(params);

    //     $http.get('/houses.json').success(function(data) {

    //         $scope.houses = data;
    //         if($scope.houses.RentType == 'daily') $scope.houses.RentTypeName = 'დღეში';
    //         else $scope.houses.RentTypeName = 'საათში';

    //         console.log($scope.houses[0].RentType);
            
    //     }); 
    // }

});


app.controller('samplecontoller', function ($scope, $http) {

  
 $scope.showData = function(){

   // $http.get('/houses.json').success(function(data) {

    // function getPrice($app){
    //     if($app->app_type == 'hourly'){
    //         return $app->hourly_price." ლარი / საათში";
    //     }
    //     else if($app->app_type == 'daily'){
    //         return $app->daily_price." ლარი / დღეში";
    //     }
    //     else if($app->app_type == 'daily-hourly'){
    //         return $app->hourly_price." ლარი / დღეში";
    //     }
    // }

     
        $scope.houses = Appdata;
        //console.log($scope.houses);
        
        if($scope.houses.applicationType == 'daily') $scope.houses.RentTypeName = lang_object['daily'];
        else $scope.houses.RentTypeName = lang_object['hourly'];


        
        switch($scope.houses['applicationType']) {
            case 'hourly':
                $scope.houses['price'] = 1;
                break;
            case 'daily':
                $scope.houses['price'] = 0;
                break;
            case 'daily-hourly':
                $scope.houses['price'] = 2;
                break;
        }



        //console.log($scope.houses);

            var pagesShown = 1;
            var pageSize = 10;
            var Mlocations = [];
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };

            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($scope.houses.length / pageSize);
            };

            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;
                 
            };

            Mlocation = [];
            
            if($scope.houses.length == 0){
                $('.page-header').show();
                $('.footer-link-header-text').hide();
            }


            for (var i = 0; i < $scope.houses.length; i++) {

                if ($scope.houses[i]['currency'] == 'USD' )  $scope.houses[i]['sign'] = 'usd';
                else $scope.houses[i]['sign'] = 'gel';

                var hourly_price_html = '&nbsp', daily_price_html = '&nbsp';
                if ($scope.houses[i]['hourly_price'] != 0  ) hourly_price_html = '<span class="price-hour">'+$scope.houses[i]['hourly_price']+' <i class="'+ $scope.houses[i]['sign'] + '-sign"></i> / '+lang_object['hourly']+'</span>';
                if ($scope.houses[i]['daily_price'] != 0  ) daily_price_html = '<span class="price-day">'+$scope.houses[i]['daily_price']+' <i class="'+ $scope.houses[i]['sign'] + '-sign"></i> / '+lang_object['daily']+'</span>';

                if($scope.houses[i]['rentType'] != 'banner'){
                    Mlocation = $scope.houses[i]['location'];
                    var pathArray = window.location.pathname.split( '/' );
                    var url = '/'+pathArray[1]+'/application/'+$scope.houses[i]['slug']+'/'+$scope.houses[i]['uniqueID']
                  
                     Map_html = '<a href="'+url+'">' +
                                '<img class="rame" alt="house-4" src="/uploads/applications/'+$scope.houses[i]['id']+'/2_'+$scope.houses[i]['mainImage']+'">'+
                                    '<div class="caption">'+$scope.houses[i]['title']+'</div>'+
                                        hourly_price_html +
                                        daily_price_html +
                                    '<div class="clearfix"></div> <ul>'+
                                        '<li>'+$scope.houses[i]['description']+'</li>'+
                                        '<li>'+$scope.houses[i]['details']['room']+' '+lang_object['room']+', '+$scope.houses[i]['details']['m2']+' კვ.მ, '+$scope.houses[i]['details']['type']+', '+$scope.houses[i]['details']['city']+', '+$scope.houses[i]['details']['district']+'</li>'+
                                        '<li>'+lang_object['tel']+':. '+$scope.houses[i]['phone']+'</li>'+
                                    '</ul>'+
                                '</div>'+'</a>';


                            

                    //if location is not set
                    if(Mlocation.length > 0){   
                        Mlocation.unshift(Map_html);
                        Mlocations.push(Mlocation);
                    } 
                }
             };
           

            if($(document).width()>992){
                build_map(Mlocations);
            }
            //console.log(Mlocations);
    //});
}

});
 



window.onload = function () { setTimeout(function(){ $('#loading-page').fadeOut('fast') }, 300); }

 //developed by Lasha Sulaberidze

en=new Array(97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,96,126,64,35,36,94,38,91,93,59,39,92,44,46,47,63,123,125,58,34,60,62);
ka=new Array(4304,4305,4330,4307,4308,4324,4306,4336,4312,4335,4313,4314,4315,4316,4317,4318,4325,4320,4321,4322,4323,4309,4332,4334,4327,4310,65,66,4329,68,69,70,71,72,73,4319,75,76,77,78,79,80,81,4326,4328,4311,85,86,4333,88,89,4331,96,126,64,35,36,94,38,91,93,59,39,92,44,46,47,63,123,125,58,34,60,62);
ru=new Array(1092,1080,1089,1074,1091,1072,1087,1088,1096,1086,1083,1076,1100,1090,1097,1079,1081,1082,1099,1077,1075,1084,1094,1095,1085,1103,1060,1048,1057,1042,1059,1040,1055,1056,1064,1054,1051,1044,1068,1058,1065,1047,1049,1050,1067,1045,1043,1052,1062,1063,1053,1071,1105,1025,34,8470,59,58,63,1093,1098,1078,1101,47,1073,1102,46,44,1061,1066,1046,1069,1041,1070);


// function opa(kode){
//     $('#logs').html( $('#logs').html()+ ',' + kode )
// }


function makeLan(ob,e,lan) { 

//opa(e.keyCode)

attr = $(ob).attr('data-lan');

if (attr) lan = attr;


//lan = typeof lan !== 'undefined' ? lan : lan = ob.getAttribute('data-lan');

    console.log(lan);

    if(lan == 'ka') lan = ka;
    else if(lan == 'ru') lan = ru;
    else if(lan == 'en') lan = en;

    code = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;  

    if (e.which==0) return true;

    //if (!document.getElementById('lanKeys').checked) return true;
    
    
    var found = false;
    for (i=0; i<=lan.length; i++) {
        if (en[i]==code) {
            c=lan[i];
            found = true;
        }
    }
    
    if ( found ) {
        if (document.selection) {
            sel = document.selection.createRange();
            sel.text = String.fromCharCode(c);
        } else {
            if (ob.selectionStart || ob.selectionStart == '0') {
                var startPos = ob.selectionStart;
                var endPos = ob.selectionEnd;
                ob.value = ob.value.substring(0, startPos) + String.fromCharCode(c) + ob.value.substring(endPos, ob.value.length);
                ob.selectionStart = startPos+1;
                ob.selectionEnd = endPos+1;
            } else {
                //ob.value = ob.value + String.fromCharCode(c);
                return true;
            }
        }
        return false;
    } else {
        return true;
    }

}
